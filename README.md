> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.

# Naturident

Naturident offers a framework for the reidentification of pallets based on natural external features. We use images of the pallet blocks to uniquely identify a pallet. This repository provides an environment in which the identification procedure can be tested. 

# Services

![Architecture building block view](documentation/images/Architecture-open-source.png)

| Service           | Address               | Description                              |
|-------------------|-----------------------|------------------------------------------|
| Angular-Frontend  | http://localhost:8080 | Main order management Webinterface       |
| Order-ID-Service  | http://localhost:5000 |                                          |
| Order-ID-MongoDB  | localhost:27017       | persistent database for Order-ID-Service |
| Signature-Service | http://localhost:5001 |                                          |
| Detection-Service | http://localhost:5002 | Terminal control webinterface            |
| Milvus            | localhost:19530       | Vector database                          |
| Milvus-Insight    | http://localhost:3000 | Vector database Web dashboard            |
| Milvus-Minio      | -                     | Persistent database for Milvus           |
| Milvus-etcd       | -                     | Meta database for Milvus                 |


# Getting Started

## Starting containers

The easiest way to get staret is to use the provided docker-compose file. Make sure you have [docker](https://docs.docker.com/desktop/) and [docker-compose](https://docs.docker.com/compose/install/) installed and set-up.

You'll have to make one config change in `config/milvus-insight-params.env` and replace `http://ENTER_HOST_COMPUTER_IP_HERE:3000` with the IP-Address of your host computer (for CORS). Afterwards, the line should look like this: `http://192.168.178.42:3000`.

Then simply build and start all services in background with these commands.

```
docker-compose build
docker-compose up -d
```

Make sure everything is up and runnig with the following command. You should see the `Up` everywhere in the `State` column. 

```
docker-compose ps -a
```

## Init services
**NOTE:** If you use the detection service (which you will by default) this initialization step is done for you automatically.

You'll have to initialize some data to get a basic demo running. Head over to the Swagger interface of the order-id service at http://localhost:5000 and execute the POST request at `/general/` to init the order-id service with two terminals (departure and arrival). 
After you received the `Created` response you should see two new terminals via the GET request at `/terminals/`. 


## Frontend

Open the frontend at [http://localhost:8080](http://localhost:8080). This should be pretty empty right now. 

## Detection Service
We provide you with one instance of the detection service at [http://localhost:5002](http://localhost:5002). The Detection service allows you to feed in Video files of pallets and simulate the procedure of them being scanned by a camera on a roller conveyor. The example Videos are inside of the docker image but you can add external videos in `/detection-service/videos`. You can switch between the departure terminal the arrival terminal using the buttons on the top. The first one is used to initially register the pallet into the system. The second instance  is used to re-identify a pallet after it has been registerd by the departure terminal. 

1. Open the main frontend ([http://localhost:8080](http://localhost:8080)) and head over to the departure terminal tab.
1. Open the departure terminal's webinterface ([http://localhost:5002](http://localhost:5002)).
1. Click the play button next to the file named `vid_1_c1` which plays the first video of pallet `1`.

1. Go back to the main frontend view and wait for the video to finish.
1. Ensure that a new shipment was created in the main frontend and note its pallet-id for later use.
1. You can now repeat the last 3 steps for some more videos (like `vid_2_c1`, `vid_3_c1`...)

1. Open the arrival terminal's webinterface ([http://localhost:5002](http://localhost:5002)).
1. Click the play button next to the file named `vid_1_c2` which starts the second video of pallet `1`.
1. wait for the video to finish (you can again watch the stream in the main frontend but now under the arrival terminal tab).
1. Check that the shipment which was created through `vid_1_c1` is now completed
1. Repeat the above steps for additional videos

#### Explanation:
With the first few steps you registed some pallets into the system. These were associated with a shipment for each one. Afterwards you used the arrival Terminal to re-identify these pallets using a video from another perspecitve of the very same pallet. The system manged to figure out that he pallet in `vid_1_c1` and `vid_1_c2` is essentially the same just from two different perspectives. 

## Reset

If you want to reset your current running setup you should do so by deleting all docker containers and volumes. This can be achieved with the following command. Afterwards, continue at [Starting containers](#Starting-containers).

```
docker-compose down -v
```

# Documentation
Please find the documentation of this project in `./documentation`.

# Development
Please have a look at the Readme in the respecitve directory of that component.

# License
Copyright 2022 Open Logistics Foundation  
Licensed under the Open Logistics License 1.0.  

For details on the licensing terms, see the LICENSE file.
