// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from "./pages/settings/settings.component";
import { ErrorComponent } from "./pages/error/error.component";


const routes: Routes = [
  { path: '', loadChildren: () => import('./pages/user/user.module').then(x => x.UserModule) },
  { path: 'user', loadChildren: () => import('./pages/user/user.module').then(x => x.UserModule) },
  { path: 'settings', component: SettingsComponent },
  { path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
