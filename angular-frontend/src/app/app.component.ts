// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {MatDrawerMode} from "@angular/material/sidenav";
import {MatDialog} from "@angular/material/dialog";
import { ThemeService } from './shared/services/state/theme.service';
import { FooterService } from './shared/services/state/footer.service';
import { SidenavService } from './shared/services/state/sidenav.service';
import { CookieDialogComponent } from './shared/dialogs/cookie-dialog/cookie-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'NatureIdent Service';
  breakpointStateLtMd = false;
  themeModeIcon: string;
  footerState: boolean;
  sidenavMode: MatDrawerMode = 'side';
  sidenavState = true;

  constructor(
    public breakpointObserver: BreakpointObserver,
    public dialog: MatDialog,
    public themeService: ThemeService,
    public footerService: FooterService,
    public sidenavService: SidenavService)
  {
    breakpointObserver.observe([
      Breakpoints.Small,
      Breakpoints.XSmall
    ]).subscribe(result => {
      if (result.matches) {
        this.activateHandsetLayout();
        this.breakpointStateLtMd = true;
      } else {
        this.deactivateHandsetLayout();
        this.breakpointStateLtMd = false;
      }
    });
  }

  /* istanbul ignore next */
  ngOnInit(): void {
    // cookie consent
    if (localStorage.getItem('cookie-consent') == 'true') {
      // load application state from local storage
      // mode and theme
      this.themeService.setMode(localStorage.getItem(this.themeService.getModeStorageKey()));
      this.themeService.setTheme(localStorage.getItem(this.themeService.getThemeStorageKey()));
      document.body.classList.add(this.themeService.getThemeStr() + '-' + this.themeService.getModeStr());
      this.themeModeIcon = this.themeService.getNextModeIcon();

      // footer
      this.footerService.initFooterState()
      this.footerService.footerState$.subscribe( x => this.footerState = x )

      // sidenav
      this.sidenavService.initSidenavState();
      if (!this.breakpointStateLtMd) {
        this.sidenavState = this.sidenavService.getSidenavState();
      }

    } else {
      // reset application state to default
      localStorage.clear();

      this.themeService.resetMode();
      this.themeService.resetTheme();
      document.body.classList.add(this.themeService.getAvailableThemes()[0].name + '-' +
        this.themeService.getAvailableModes()[0].name);
      this.themeModeIcon = this.themeService.getNextModeIcon();

      this.footerService.resetFooterState();

      this.sidenavService.resetSidenavState();

      // get consent from user and app state from local storage
      let cookieDialogRef = this.dialog.open(CookieDialogComponent, {disableClose: true});
      cookieDialogRef
        .afterClosed()
        .subscribe(() => {
          // save initial application state to local storage
          localStorage.setItem('cookie-consent', 'true');
          localStorage.setItem(this.themeService.getModeStorageKey(), this.themeService.getModeStr());
          localStorage.setItem(this.themeService.getThemeStorageKey(), this.themeService.getThemeStr());
          localStorage.setItem(this.footerService.getStorageKey(), String(this.footerService.getFooterState()))
          this.footerService.footerState$.subscribe(x => this.footerState = x)
          localStorage.setItem(this.sidenavService.getStorageKey(), String(this.sidenavService.getSidenavState()));
        });
    }
  }
   
  /* istanbul ignore next */
  private activateHandsetLayout() {
    this.sidenavMode = 'over';
    this.sidenavState = false;
  }

  /* istanbul ignore next */
  private deactivateHandsetLayout() {
    this.sidenavMode = 'side';
    this.sidenavState = this.sidenavService.getSidenavState();
  }

  /* istanbul ignore next */
  sidenavStateToggle() {
    if (!this.breakpointStateLtMd){
      this.sidenavService.toggleSidenavState();
    }
  }

  /* istanbul ignore next */
  CycleMode(): void {
    let nextClassname = this.themeService.getThemeStr() + '-' + this.themeService.getNextModeObj().name;
    let currentClassname = this.themeService.getThemeStr() + '-' + this.themeService.getModeObj().name;
    if (nextClassname !== currentClassname) {
      document.body.classList.add(nextClassname);
      document.body.classList.remove(currentClassname);
      this.themeService.cycleMode();
      this.themeModeIcon = this.themeService.getNextModeIcon();
    }
  }
}
