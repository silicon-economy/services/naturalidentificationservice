// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Injectable } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';

import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatDialogModule } from "@angular/material/dialog";
import { MatIconModule } from "@angular/material/icon";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSelectModule } from "@angular/material/select";
import { MatSliderModule } from "@angular/material/slider";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatTabsModule } from "@angular/material/tabs";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SocketIoModule, Socket } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';

import { ToastrModule } from 'ngx-toastr';

import { SettingsComponent } from './pages/settings/settings.component';
import { ErrorComponent } from "./pages/error/error.component";

import { FooterService } from "./shared/services/state/footer.service";
import { ThemeService } from "./shared/services/state/theme.service";
import { CookieDialogComponent } from './shared/dialogs/cookie-dialog/cookie-dialog.component';
import { SidenavService } from "./shared/services/state/sidenav.service";
import { ResetDemonstratorDialogComponent } from './shared/dialogs/reset-demonstrator-dialog/reset-demonstrator-dialog.component';

/*
* The Socket for communication via ScoketIO with the order-id-service
* Note: Just injecting the SocketIoConfig didn't work and caused some wired mashups of different enviroment versions
*/
@Injectable()
export class OrderIdServiceSocket extends Socket {

  constructor() {
    super({ url: `${environment.socketIOHost}:${environment.socketIOPort}`, options: {} });
  }
}

@NgModule({
  declarations: [
    AppComponent,
    SettingsComponent,
    ErrorComponent,
    CookieDialogComponent,
    ResetDemonstratorDialogComponent,
  ],
  entryComponents: [CookieDialogComponent, ResetDemonstratorDialogComponent],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,


    LoggerModule.forRoot({
      serverLoggingUrl: '/api/logs',
      level: NgxLoggerLevel.TRACE,
      serverLogLevel: NgxLoggerLevel.ERROR,
      disableConsoleLogging: false
    }),
    SocketIoModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    })
  ],
  providers: [
    HttpClientModule,
    OrderIdServiceSocket,
    FooterService,
    ThemeService,
    SidenavService
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
