// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

export interface ScanStatusUpdate {
    status: ScanStatus,
    message: string,
    scanId: string,
    palletId: string,
    shipmentId: string
}

export enum ScanStatus {
    done = 'DONE',
    error = 'ERROR'
}
