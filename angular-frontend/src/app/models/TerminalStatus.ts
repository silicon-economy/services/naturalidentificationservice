// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

export enum TerminalStatusArrival {
      scan = 'SCANNING',
      signature = 'SIGNATURE',
      sending = 'SENDING',
      done = 'DONE',
      error = 'ERROR'
}

// todo anpassen
export enum TerminalStatusDeparture {
      scan = 'SCANNING',
      signature = 'SIGNATURE',
      sending = 'SENDING',
      done = 'DONE',
      error = 'ERROR'
}
