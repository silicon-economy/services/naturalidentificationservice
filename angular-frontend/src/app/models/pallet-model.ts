// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { PalletScanDetail } from "./pallet-scan-model";

export interface PalletDetail {
  palletId: string;
  scans: Array<PalletScanDetail>;
  pallet_type: string;
  first_ever_scan: Date;
  last_scan: Date;
  last_event: string;
  status: string;
  shipment_id: string
}
