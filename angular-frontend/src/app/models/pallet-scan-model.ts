// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { PalletFeetDetail } from "./pallet-feet-model";

export interface PalletScanDetail {
    scanId: string;
    timeStamp: Date;
    certainty: number;
    terminalId: string;
    confirmed: boolean | null;
    pallet_feet: PalletFeetDetail[];
  }
  