// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

export interface ProductInformation {
  description: string;
  weight: number;
  country_of_origin: string;
  warning_labels: string[];
}
