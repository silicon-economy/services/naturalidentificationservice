// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { PalletDetail } from './pallet-model';
import {ProductInformation} from './product-model';

export interface Shipment {
  shipment_id: string;
  carrier_detail: PalletDetail;
  shipment_detail: ShipmentDetail;
  product_info: ProductInformation;
}

export interface ShipmentDetail {
  sscc_nve: string;
  sender: string;
  recipient: string;
  last_destination: string;
  next_destination: string;
}
