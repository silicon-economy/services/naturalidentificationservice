// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MatDialog, MatDialogModule } from "@angular/material/dialog";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from "@angular/material/select";

import { ToastrModule } from 'ngx-toastr';
import { of } from 'rxjs';

import { FooterService } from 'src/app/shared/services/state/footer.service';
import { ThemeService } from 'src/app/shared/services/state/theme.service';
import { SettingsComponent } from './settings.component';
import { MatSliderModule } from '@angular/material/slider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export class MdDialogMock {
  // When the component calls this.dialog.open(...) we'll return an object
  // with an afterClosed method that allows to subscribe to the dialog result observable.
  open() {
    return {
      afterClosed: () => of({})
    };
  }
}

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SettingsComponent],
      imports: [
        ToastrModule.forRoot(),
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,

        MatDialogModule,
        MatIconModule,
        MatListModule,
        MatCardModule,
        MatSelectModule,
        MatSliderModule,
        MatSlideToggleModule,
      ],
      providers: [
        ThemeService,
        FooterService,
        {
          provide: MatDialog, useClass: MdDialogMock,
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open reset dialog if action is triggered', inject([MatDialog], (matdialog: MatDialog) => {
    const dialogSpy = spyOn(matdialog, 'open').and.callThrough();
    component.openResetDemonstratorDialog();
    expect(dialogSpy).toHaveBeenCalledTimes(1);
  }));

  it('should update the theme to selected one', inject([ThemeService], (themeService: ThemeService) => {
    const themeServiceGetThemeSpy = spyOn(themeService, 'getThemeStr').and.callThrough()
    const themeServiceGetModeSpy = spyOn(themeService, 'getModeStr').and.callThrough()
    const themeServiceSetThemeSpy = spyOn(themeService, 'setTheme').and.callThrough()

    const oldTheme = component.selectedTheme;
    const newTheme = themeService.getAvailableThemes()[2].name;

    component.selectedTheme = newTheme;
    component.changeTheme();

    expect(themeServiceSetThemeSpy).toHaveBeenCalledOnceWith(newTheme);
    expect(themeServiceGetModeSpy).toHaveBeenCalledTimes(2);
    expect(themeServiceGetThemeSpy).toHaveBeenCalledTimes(2);
  }));

  it('should update footer state on toggle vallue change', inject([FooterService], (footerService: FooterService) => {
    const footerServiceSpy = spyOn(footerService, 'setFooterState').and.callThrough()

    const oldValue:boolean = component.footerToggle.value;

    component.footerToggle.setValue(!oldValue);

    expect(footerServiceSpy).toHaveBeenCalledOnceWith(!oldValue);
  }));

  it('should set footer toggle to current state of footer service', inject([FooterService], (footerService: FooterService) => {
    const footerServiceSpy = spyOn(footerService, 'getFooterState').and.callThrough()

    
    expect(component.footerToggle.value).toEqual(footerService.getFooterState())
    expect(footerServiceSpy).toHaveBeenCalledTimes(1);
  }));
});
