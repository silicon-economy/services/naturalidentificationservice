// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {Component} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {FooterService} from "../../shared/services/state/footer.service";
import {Observable} from "rxjs";
import {FormControl} from "@angular/forms";
import {ThemeService} from "../../shared/services/state/theme.service";
import { ResetDemonstratorDialogComponent } from 'src/app/shared/dialogs/reset-demonstrator-dialog/reset-demonstrator-dialog.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent {
  footerToggle = new FormControl('', []);
  footerState$ = new Observable<boolean>();

  availableThemes: Object;
  selectedTheme: string;

  constructor(public dialog: MatDialog,
              public footerService: FooterService,
              public themeService: ThemeService) {
    this.footerToggle.setValue(this.footerService.getFooterState());
    this.footerToggle.valueChanges.subscribe(x => {
      this.footerService.setFooterState(x);
    });

    this.availableThemes = this.themeService.getAvailableThemes();
    this.selectedTheme = this.themeService.getThemeStr();

  }

  openResetDemonstratorDialog() {
    this.dialog.open(ResetDemonstratorDialogComponent);
  }

  /* istanbul ignore next */
  changeTheme(): void {
    document.body.classList.remove(this.themeService.getThemeStr()+'-'+this.themeService.getModeStr());
    this.themeService.setTheme(this.selectedTheme);
    document.body.classList.add(this.themeService.getThemeStr()+'-'+this.themeService.getModeStr());
  }
}
