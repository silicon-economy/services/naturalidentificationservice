// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import {RouterTestingModule} from '@angular/router/testing';

import {CarrierDetailComponent} from './carrier-detail.component';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {Shipment} from '../../../../../models/shipment-model';
import {of} from 'rxjs';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';


export class MdDialogMock {
  // When the component calls this.dialog.open(...) we'll return an object
  // with an afterClosed method that allows to subscribe to the dialog result observable.
  open() {
    return {
      afterClosed: () => of({
        type: 'dialog_test',
        palette_id: '',
        status: '',
        first_scan: '',
        last_scan: '',
        last_event: '',
        shipment_id: 'dialog'
      })
    };
  }
}

describe('CarrierDetailComponent', () => {
  let component: CarrierDetailComponent;
  let fixture: ComponentFixture<CarrierDetailComponent>;

  const niServiceSpy = jasmine.createSpyObj('NaturidentService', ['getShipment', 'updatePallet']);

  let mockNIService;
  let mockShipment: Shipment;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatTableModule,
        RouterTestingModule,
        MatListModule,
        MatDividerModule,
        MatGridListModule,
        RouterTestingModule,
        MatDialogModule,
        ToastrModule.forRoot()
      ],
      declarations: [CarrierDetailComponent],
      providers: [
        ToastrService,
        {
          provide: NaturidentService,
          useValue: niServiceSpy
        }, 
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({objID: 1}))
          }
        },
        {
          provide: MatDialog, useClass: MdDialogMock,
        }
      ]
    });
    mockShipment = {
      shipment_id: '1',
      carrier_detail: {
        pallet_type: 'Europoolpalette (Pressspan)',
        palletId: '4221312323',
        status: 'Im Lager',
        scans: [],
        first_ever_scan: new Date('01.01.1970'),
        last_scan: new Date('15.02.2021'),
        last_event: 'Wareneingang',
        shipment_id: '1'
      },
      shipment_detail: {
        sscc_nve: '141421356237309504880',
        sender: 'Fraunhofer IML, Joseph-von-Fraunhofer Straße 2-4, 44227 Dortmund',
        last_destination: 'Umschlaglager Bayern Süd',
        next_destination: 'Umschlaglager NRW Mitte',
        recipient: 'Bundeskanzleramt, Willy-Brandt-Straße 1, 10557 Berlin'
      },
      product_info: {
        description: 'Gefahrgut',
        warning_labels: ['battery'],
        weight: 10.5,
        country_of_origin: 'Germany'
      }
    };

    mockNIService = TestBed.inject(NaturidentService);

    mockNIService.getShipment.and.returnValue(of(mockShipment));
    
    mockNIService.updatePallet.and.returnValue(of(null));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrierDetailComponent);
    component = fixture.componentInstance;
    component.carrierDetail = mockShipment.carrier_detail;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not get shipment from service', () => {
    expect(mockNIService.getShipment).toBeDefined();
    expect(mockNIService.getShipment).toHaveBeenCalledTimes(0);
    expect(component.carrierDetail.pallet_type).toEqual('Europoolpalette (Pressspan)');
  });

  it('should update carrierDetail from dialog', () => {
    component.openDialog();
    expect(mockNIService.updatePallet).toHaveBeenCalled();
  });
});
