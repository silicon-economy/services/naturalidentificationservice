// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {Component, OnInit, Inject, Input} from '@angular/core';
import {Shipment} from '../../../../../models/shipment-model';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { PalletDetail } from 'src/app/models/pallet-model';
import { ToastrService } from 'ngx-toastr';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';

@Component({
  selector: 'app-carrier-detail',
  templateUrl: './carrier-detail.component.html',
  styleUrls: ['./carrier-detail.component.scss']
})
export class CarrierDetailComponent implements OnInit {

  private shipment: Shipment;
  private objID: string;

  @Input() carrierDetail: PalletDetail;

  constructor(
    private naturIdentService: NaturidentService, 
    public dialog: MatDialog, 
    private toastr: ToastrService) {
   
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(CarrierDetailEditDialog, {
      width: '50%',
      data: {...this.carrierDetail},
      restoreFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) { // check if dialog was canceled
        this.naturIdentService.updatePallet(this.carrierDetail.palletId, result).subscribe({
          next: pallet => {
            this.carrierDetail = pallet;
            this.toastr.success("Carrier updated");
          },
          error: e => {
            this.toastr.error("Failed to update carrier information");
          }
        });
      }
    });
  }

  ngOnInit() {
  }
}

@Component({
  selector: 'app-carrier-detail-edit-dialog',
  templateUrl: 'carrier-detail-edit-dialog.html',
  styleUrls: ['./carrier-detail.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class CarrierDetailEditDialog {

  constructor(
    public dialogRef: MatDialogRef<CarrierDetailEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: PalletDetail) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
