// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {Component, Input, OnInit} from '@angular/core';
import { ImageWithDescription } from 'src/app/models/image-with-description';


@Component({
  selector: 'app-carrier-images',
  templateUrl: './carrier-images.component.html',
  styleUrls: ['./carrier-images.component.scss']
})
export class CarrierImagesComponent implements OnInit {
  @Input() palletFeetImages: ImageWithDescription[];

  hover_image_path: string;

  ngOnInit() {
    this.hover_image_path = null;
  }

  showlargeimage(imgpath: string) {
    this.hover_image_path = imgpath;
  }

  closelargeimage() {
    this.hover_image_path = null;
  }
}
