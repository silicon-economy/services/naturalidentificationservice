// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';
import { RouterTestingModule } from '@angular/router/testing';
import { CarrierDetailComponent } from './carrier-detail/carrier-detail.component';
import { CarrierImagesComponent } from './carrier-images/carrier-images.component';

import { ContentComponent } from './content.component';
import { ProductInformationComponent } from './product-information/product-information.component';
import { ShipmentDetailComponent } from './shipment-detail/shipment-detail.component';
import { OrderOverviewComponent } from '../order-overview/order-overview.component';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { Shipment } from 'src/app/models/shipment-model';
import { of, Subject } from 'rxjs';
import { PalletDetail } from 'src/app/models/pallet-model';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';


describe('ContentComponent', () => {
  let component: ContentComponent;
  let fixture: ComponentFixture<ContentComponent>;

  let niServiceSpy = jasmine.createSpyObj('NaturidentService', ['getPallet', 'getPalletFeetImageUrl', 'getShipment']);

  let mockNIService;

  let mockShipment: Shipment;
  let getShipmentMockSubject: Subject<Shipment>;
  let getPalletMockSubject: Subject<PalletDetail>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatGridListModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatDividerModule,
        MatDialogModule,
        RouterTestingModule,
        HttpClientModule,
        ToastrModule.forRoot()
      ],
      declarations: [
        ContentComponent,
        CarrierDetailComponent,
        CarrierImagesComponent,
        ProductInformationComponent,
        ShipmentDetailComponent,
        OrderOverviewComponent
      ],
      providers: [
        ToastrService,
        {
          provide: NaturidentService,
          useValue: niServiceSpy
        },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ objID: '1' }))
          }
        }
      ]
    })
      .compileComponents();

    mockShipment = {
      shipment_id: '1',
      carrier_detail: {
        pallet_type: 'Europoolpalette (Pressspan)',
        palletId: '4221312323',
        status: 'Im Lager',
        scans: [],
        first_ever_scan: new Date('01.01.1970'),
        last_scan: new Date('15.02.2021'),
        last_event: 'Wareneingang',
        shipment_id: '1'
      },
      shipment_detail: {
        sscc_nve: '141421356237309504880',
        sender: 'Fraunhofer IML, Joseph-von-Fraunhofer Straße 2-4, 44227 Dortmund',
        last_destination: 'Umschlaglager Bayern Süd',
        next_destination: 'Umschlaglager NRW Mitte',
        recipient: 'Bundeskanzleramt, Willy-Brandt-Straße 1, 10557 Berlin'
      },
      product_info: {
        description: 'Gefahrgut',
        warning_labels: ['battery'],
        weight: 10.5,
        country_of_origin: 'Germany'
      }
    };

    mockNIService = TestBed.inject(NaturidentService);

    getShipmentMockSubject = new Subject<Shipment>();
    mockNIService.getShipment.and.returnValue(getShipmentMockSubject);

    getPalletMockSubject = new Subject<PalletDetail>();
    mockNIService.getPallet.and.returnValue(getPalletMockSubject);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get shipment from service', () => {
    getShipmentMockSubject.next(mockShipment);

    expect(mockNIService.getShipment).toBeDefined();
    expect(mockNIService.getShipment).toHaveBeenCalledWith('1');
    expect(component.shipment).toBeDefined();
  });

  it('should fail and not get pallet if shipment has error', () => {
    niServiceSpy = jasmine.createSpyObj('NaturidentService', ['getPallet', 'getPalletFeetImageUrl', 'getShipment']);

    getShipmentMockSubject.error(new Error('failed to get shipment'));
    

    expect(mockNIService.getShipment).toBeDefined();
    expect(mockNIService.getShipment).toHaveBeenCalledWith('1');
    expect(niServiceSpy.getPallet).toHaveBeenCalledTimes(0);
  });

  it('should get pallet from service', () => {
    getShipmentMockSubject.next(mockShipment);

    const pallet: PalletDetail = {
      palletId: '4221312323',
      scans: [
        {
          scanId: 'scan1',
          certainty: 0.42,
          confirmed: null,
          terminalId: 'terminal1',
          timeStamp: new Date(),
          pallet_feet: []
        }
      ],
      first_ever_scan: new Date('01.01.1970'),
      last_scan: new Date('15.02.2021'),
      last_event: 'Wareneingang',
      shipment_id: '1',
      status: 'Test',
      pallet_type: 'EPAL'
    };
    getPalletMockSubject.next(pallet);

    expect(mockNIService.getPallet).toHaveBeenCalledWith('4221312323');
    expect(component.pallet).toBeDefined();
    expect(component.pallet.palletId).toEqual('4221312323');
  });

  it('should set pallet feet images to empty array if scans are missing', () => {
    getShipmentMockSubject.next(mockShipment);

    const pallet: PalletDetail = {
      palletId: '4221312323',
      scans: [],
      first_ever_scan: new Date('01.01.1970'),
      last_scan: new Date('15.02.2021'),
      last_event: 'Wareneingang',
      shipment_id: '1',
      status: 'Test',
      pallet_type: 'EPAL'
    };
    getPalletMockSubject.next(pallet);


    expect(mockNIService.getPallet).toHaveBeenCalledWith('4221312323');
    expect(component.pallet.palletId).toEqual('4221312323');
    expect(component.pallet.scans).toEqual([]);
    expect(component.palletFeetImages).toEqual([]);
  });

  it('should set pallet feet images to these of the last scan', () => {
    getShipmentMockSubject.next(mockShipment);

    const pallet: PalletDetail = {
      palletId: '4221312323',
      scans: [
        {
          scanId: 'scan1',
          certainty: 0.42,
          confirmed: null,
          terminalId: 'terminal1',
          timeStamp: new Date('2020-01-01'),
          pallet_feet: [
            { description: 'pallet feet 1-1' },
            { description: 'pallet feet 1-2' }
          ]
        },
        {
          scanId: 'scan2',
          certainty: 0.52,
          confirmed: null,
          terminalId: 'terminal1',
          timeStamp: new Date('2021-02-02'),
          pallet_feet: [
            { description: 'pallet feet 1' },
            { description: 'pallet feet 2' },
            { description: 'pallet feet 3' }
          ]
        }
      ],
      first_ever_scan: new Date('01.01.1970'),
      last_scan: new Date('15.02.2021'),
      last_event: 'Wareneingang',
      shipment_id: '1',
      status: 'Test',
      pallet_type: 'EPAL'
    };
    mockNIService.getPalletFeetImageUrl.and.returnValue('some-img-url');
    getPalletMockSubject.next(pallet);

    expect(mockNIService.getPallet).toHaveBeenCalledWith('4221312323');
    expect(component.pallet.palletId).toEqual('4221312323');

    expect(mockNIService.getPalletFeetImageUrl).toHaveBeenCalledTimes(3);
    expect(component.palletFeetImages.length).toEqual(3);
    expect(component.palletFeetImages[0].description).toEqual('pallet feet 1')
  });
});
