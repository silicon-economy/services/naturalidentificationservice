// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ImageWithDescription } from 'src/app/models/image-with-description';
import { PalletDetail } from 'src/app/models/pallet-model';
import { Shipment } from 'src/app/models/shipment-model';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  pallet: PalletDetail;
  palletFeetImages: ImageWithDescription[];

  shipment: Shipment;
  private objID: string;

  constructor(
    private route: ActivatedRoute,
    private natureIdentService: NaturidentService,
    private toastr: ToastrService) {
   
  }

  ngOnInit() {
    this.getCarrierDetailInformation();
  }

  getCarrierDetailInformation() {
    this.route.paramMap.subscribe(
      (paramMap: ParamMap) => {
        // TODO Error Handling - shipment not found + does not have id
        if (paramMap.has('objID')) {

          this.objID = paramMap.get('objID');
          this.natureIdentService.getShipment(this.objID).subscribe({
            next: shipment => {
              this.shipment = shipment;
              this.updatePallet(shipment);
            },
            error: e => {
              this.toastr.error(`Error receiving Shipment`, 'Shipment');
            }
          });
        } else {
          console.error('Could not find objID');
          this.toastr.error(`Could not find objID`, 'Shipment');
        }
      }
    );
  }

  private updatePallet(shipment: Shipment) {
    if (!shipment) {
      this.palletFeetImages = []
      return;
    }

    this.natureIdentService.getPallet(shipment.carrier_detail.palletId).subscribe({
      next: palletDetail => {
        this.pallet = palletDetail;
        if (palletDetail.scans && palletDetail.scans.length > 0) {
          const scan = palletDetail.scans.slice(-1)[0];
          this.palletFeetImages = scan.pallet_feet.map((pf, idx) => <ImageWithDescription>{
            description: pf.description,
            imgurl: this.natureIdentService.getPalletFeetImageUrl(palletDetail.palletId, scan.scanId, idx)
          });
        } else {
          this.palletFeetImages = []
        }
      },
      error: error => {
        // TODO show error message to user
        console.error(`Error pallet with id ${shipment.carrier_detail.palletId}`);
      }
    });
  }
}
