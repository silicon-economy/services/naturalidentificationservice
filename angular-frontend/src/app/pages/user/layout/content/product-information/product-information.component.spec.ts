// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { RouterTestingModule } from '@angular/router/testing';
import {MatListModule} from '@angular/material/list';

import { ProductInformationComponent } from './product-information.component';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { ProductInformation } from 'src/app/models/product-model';

describe('ProductInformationComponent', () => {
  let component: ProductInformationComponent;
  let fixture: ComponentFixture<ProductInformationComponent>;

  const productInfo: ProductInformation = {
    weight: 14.2,
    country_of_origin: 'Italy',
    description: 'Zitronen',
    warning_labels: []
  }

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [    
        MatCardModule,
        MatListModule,
        MatDividerModule,
        MatGridListModule,
        RouterTestingModule
      ],
      declarations: [ ProductInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductInformationComponent);
    component = fixture.componentInstance;
    component.productInformation = productInfo;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
