// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {Component, Input, OnInit} from '@angular/core';
import {ProductInformation} from '../../../../../models/product-model';

@Component({
  selector: 'app-product-information',
  templateUrl: './product-information.component.html',
  styleUrls: ['./product-information.component.scss']
})
export class ProductInformationComponent implements OnInit {
  @Input() productInformation: ProductInformation;

  constructor() {
  }

  ngOnInit() {
  }
}
