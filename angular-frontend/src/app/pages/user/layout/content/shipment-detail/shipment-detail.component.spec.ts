// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';

import { ShipmentDetailComponent } from './shipment-detail.component';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { of } from 'rxjs';
import { Shipment } from '../../../../../models/shipment-model';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';

export class MdDialogMock {
  // When the component calls this.dialog.open(...) we'll return an object
  // with an afterClosed method that allows to subscribe to the dialog result observable.
  open() {
    return {
      afterClosed: () => of({
        sscc_nve: 'dialog_test',
        sender: '',
        last_destination: '',
        next_destination: '',
        loaded_in: '',
        recipient: '',
        shipment_id: 'dialog'
      })
    };
  }
}

describe('ShipmentDetailComponent', () => {
  let component: ShipmentDetailComponent;
  let fixture: ComponentFixture<ShipmentDetailComponent>;

  const niServiceSpy = jasmine.createSpyObj('NaturidentService', ['getShipment', 'updateShipmentDetail']);

  let mockNIService;
  let mockShipment: Shipment;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatTableModule,
        MatListModule,
        MatDividerModule,
        MatGridListModule,
        RouterTestingModule,
        MatDialogModule,
        ToastrModule.forRoot()
      ],
      declarations: [ShipmentDetailComponent],
      providers: [
        ToastrService,
        {
          provide: NaturidentService,
          useValue: niServiceSpy
        },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(convertToParamMap({ objID: 1 }))
          }
        },
        {
          provide: MatDialog, useClass: MdDialogMock,
        }
      ]
    });
    mockShipment = {
      shipment_id: '1',
      carrier_detail: {
        pallet_type: 'Europoolpalette (Pressspan)',
        palletId: '4221312323',
        status: 'Im Lager',
        scans: [],
        first_ever_scan: new Date('01.01.1970'),
        last_scan: new Date('15.02.2021'),
        last_event: 'Wareneingang',
        shipment_id: '1'
      },
      shipment_detail: {
        sscc_nve: '141421356237309504880',
        sender: 'Fraunhofer IML, Joseph-von-Fraunhofer Straße 2-4, 44227 Dortmund',
        last_destination: 'Umschlaglager Bayern Süd',
        next_destination: 'Umschlaglager NRW Mitte',
        recipient: 'Bundeskanzleramt, Willy-Brandt-Straße 1, 10557 Berlin'
      },
      product_info: {
        description: 'Gefahrgut',
        warning_labels: ['battery'],
        weight: 10.5,
        country_of_origin: 'Germany'
      }
    };

    mockNIService = TestBed.inject(NaturidentService);

    mockNIService.getShipment.and.returnValue(of(mockShipment));

    mockNIService.updateShipmentDetail.and.returnValue(of(null));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmentDetailComponent);
    component = fixture.componentInstance;
    component.shipment = mockShipment;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not get shipment from service', () => {
    expect(mockNIService.getShipment).toBeDefined();
    expect(mockNIService.getShipment).toHaveBeenCalledTimes(0);
    expect(component.shipment.shipment_detail.sscc_nve).toEqual('141421356237309504880');
  });

  it('should update shipmentDetail from dialog', () => {
    component.openDialog();
    expect(mockNIService.updateShipmentDetail).toHaveBeenCalled();
  });
});
