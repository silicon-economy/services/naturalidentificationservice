// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {Component, Inject, Input, OnInit} from '@angular/core';
import {Shipment, ShipmentDetail} from '../../../../../models/shipment-model';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';


@Component({
  selector: 'app-shipment-detail',
  templateUrl: './shipment-detail.component.html',
  styleUrls: ['./shipment-detail.component.scss']
})
export class ShipmentDetailComponent implements OnInit {

  @Input() shipment: Shipment;

  constructor(private naturIdentService: NaturidentService, public dialog: MatDialog, private toastr: ToastrService) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ShipmentDetailEditDialog, {
      width: '50%',
      data: {...this.shipment.shipment_detail},
      restoreFocus: false

    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) { // check if dialog was canceled
        this.naturIdentService.updateShipmentDetail(this.shipment, result).subscribe({
          next: shipment => {
            this.shipment = shipment;
            this.toastr.success("Shipment updated");
          },
          error: e => {
            this.toastr.error("Failed to update Shipment information");
          }
        });
      }
    });
  }

  ngOnInit() {
  }
}

@Component({
  selector: 'app-carrier-detail-edit-dialog',
  templateUrl: 'shipment-detail-edit-dialog.html',
  styleUrls: ['./shipment-detail.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class ShipmentDetailEditDialog {

  constructor(
    public dialogRef: MatDialogRef<ShipmentDetailEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ShipmentDetail) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

