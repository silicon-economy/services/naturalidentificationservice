// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrderOverviewComponent } from './order-overview.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { Observable, Subject, Subscriber, throwError } from 'rxjs';
import { Shipment } from 'src/app/models/shipment-model';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';

describe('OrderOverviewComponent', () => {
  let component: OrderOverviewComponent;
  let fixture: ComponentFixture<OrderOverviewComponent>;

  let niServiceSpy = jasmine.createSpyObj('NaturidentService', ['getShipments']);
  let mockNIService;

  const toastrServiceSpy = jasmine.createSpyObj('ToastrService', ['success', 'error']);
  let mockToastrService;

  let mockShipments: Shipment[];
  let mocksubscriber: Subscriber<Shipment[]>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatTableModule,
        MatSortModule,
        HttpClientModule,
        NoopAnimationsModule,
        RouterTestingModule,
        ToastrModule.forRoot()
      ],
      declarations: [OrderOverviewComponent],
      providers: [
        {
          provide: ToastrService,
          useValue: toastrServiceSpy
        },
        {
          provide: NaturidentService,
          useValue: niServiceSpy
        },
      ]
    })
    .compileComponents();

    mockShipments = [{
      shipment_id: '1',
      carrier_detail: {
        pallet_type: 'Europoolpalette (Pressspan)',
        palletId: '4221312323',
        status: 'Im Lager',
        scans: [],
        first_ever_scan: new Date('01.01.1970'),
        last_scan: new Date('15.02.2021'),
        last_event: 'Wareneingang',
        shipment_id: '1'
      },
      shipment_detail: {
        sscc_nve: '141421356237309504880',
        sender: 'Fraunhofer IML, Joseph-von-Fraunhofer Straße 2-4, 44227 Dortmund',
        last_destination: 'Umschlaglager Bayern Süd',
        next_destination: 'Umschlaglager NRW Mitte',
        recipient: 'Bundeskanzleramt, Willy-Brandt-Straße 1, 10557 Berlin'
      },
      product_info: {
        description: 'Gefahrgut',
        warning_labels: ['battery'],
        weight: 10.5,
        country_of_origin: 'Germany'
      }
    },
    {
      shipment_id: '2',
      carrier_detail: {
        pallet_type: 'Europoolpalette (Pressspan)',
        palletId: '4421312323',
        status: 'Im Lager',
        scans: [],
        first_ever_scan: new Date('05.01.1970'),
        last_scan: new Date('18.02.2021'),
        last_event: 'Wareneingang',
        shipment_id: '2'
      },
      shipment_detail: {
        sscc_nve: '9742121356237309504880',
        sender: 'Fraunhofer IML, Joseph-von-Fraunhofer Straße 2-4, 44227 Dortmund',
        last_destination: 'Umschlaglager Bayern Süd',
        next_destination: 'Umschlaglager NRW Mitte',
        recipient: 'Bundeskanzleramt, Willy-Brandt-Straße 1, 10557 Berlin'
      },
      product_info: {
        description: 'Orangen',
        warning_labels: [],
        weight: 10.5,
        country_of_origin: 'Germany'
      }
    }];

    mockNIService = TestBed.inject(NaturidentService);
    mockToastrService = TestBed.inject(ToastrService);

    const observable = new Observable<Shipment[]>(subscriber => {
      mocksubscriber = subscriber;
    });

    mockNIService.getShipments.and.returnValue(observable);

    mockToastrService.success.and.returnValue(null);
    mockToastrService.error.and.returnValue(null);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should request shipments from api', () => {
    mocksubscriber.next(mockShipments);

    expect(mockNIService.getShipments).toBeDefined();
    expect(mockNIService.getShipments).toHaveBeenCalled();

    expect(component.shipments).toBeDefined();
    expect(component.shipments.length).toEqual(mockShipments.length);
    expect(mockToastrService.success).toHaveBeenCalledTimes(0);
  });

  it('should display an error if shipments request fails', () => {
    mocksubscriber.error('Something went wrong');

    expect(mockNIService.getShipments).toBeDefined();
    expect(mockNIService.getShipments).toHaveBeenCalled();

    expect(component.shipments).toBeDefined();
    expect(component.shipments.length).toEqual(0);
    expect(mockToastrService.error).toHaveBeenCalled();
  });
});
