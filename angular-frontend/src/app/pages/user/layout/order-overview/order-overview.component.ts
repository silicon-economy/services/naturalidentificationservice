// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatSortable} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Shipment} from '../../../../models/shipment-model';
import {Subscription} from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';


@Component({
  selector: 'app-order-overview',
  templateUrl: './order-overview.component.html',
  styleUrls: ['./order-overview.component.scss']
})
export class OrderOverviewComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor(private naturIdentService: NaturidentService, private toastr: ToastrService) {
  }

  displayedColumns: string[] = ['sscc_nve', 'palletId', 'last_scan'];
  shipments: Shipment[] = [];
  dataSource = new MatTableDataSource(this.shipments);
  private shipmentSub: Subscription;


  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    // Let the table be sorted at the start

    this.shipmentSub = this.naturIdentService.getShipments().subscribe({

      next: shipments => {
        this.shipments = shipments;
        this.dataSource = new MatTableDataSource(this.shipments);

        // we need to define own data accessor here because we use nested objects
        this.dataSource.sortingDataAccessor = (shipment: Shipment, property) => {
          switch (property) {
            case 'sscc_nve':
              return shipment.shipment_detail.sscc_nve;
            case 'palletId':
              return shipment.carrier_detail.palletId;
            case 'last_scan':
              return shipment.carrier_detail.last_scan;
            default:
              return shipment[property];
          }
        };
        this.sort.sort(({id: 'last_scan', start: 'desc'}) as MatSortable);
        this.dataSource.sort = this.sort;
      },
      error: error => {
        this.shipments = [];
        this.dataSource = new MatTableDataSource(this.shipments);

        console.error(`Error receiving shipments from API`);
        this.toastr.error(`Error receiving shipments from API`, 'Shipments');
      }
    });
  }

  ngOnDestroy(): void {
    // we have to explicitly call unsubscribe for observables we created ourselves to prevent memory leaks
    this.shipmentSub.unsubscribe();
  }
}
