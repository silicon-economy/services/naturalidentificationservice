// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {TerminalViewComponent} from './terminal-view.component';
import {RouterTestingModule} from '@angular/router/testing';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {of, Subject} from 'rxjs';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';
import { TerminalStatusArrival } from 'src/app/models/TerminalStatus';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { HttpClientModule } from '@angular/common/http';
import { PalletDetail } from 'src/app/models/pallet-model';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { ScanStatus, ScanStatusUpdate } from 'src/app/models/ScanStatus';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Shipment } from 'src/app/models/shipment-model';
import { TerminalModel } from 'src/app/models/terminal-model';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { WebsocketService } from 'src/app/shared/services/api/websocket.service';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';


describe('TerminalViewComponent', () => {
  let component: TerminalViewComponent;
  let fixture: ComponentFixture<TerminalViewComponent>;

  const wsServiceSpy = jasmine.createSpyObj('WebsocketService', ['getCamImageUpdateListener', 'subscribeCamStream', 'getStatusArrivalUpdateListener', 'subscribeTerminalStatusStream', 'getScanStatusUpdateListener', 'subscribeScanStatusStream']);
  const niServiceSpy = jasmine.createSpyObj('NaturidentService', ['getShipment', 'getTerminal']);
  let mockWSService;
  let mockNIService;
  let sanitizer: DomSanitizer;
  let camMockMessage: Uint8Array;
  let statusMockSubject: Subject<string>;
  let scanStatusMockSubject: Subject<ScanStatusUpdate>;
  let getShipmentMockSubject: Subject<Shipment>;
  let getTerminalMockSubject: Subject<TerminalModel>;

  const testTerminal: TerminalModel = {
    terminalId: "tdeparture",
    terminalType: "departure"
  }

  beforeEach(waitForAsync(() => {
    const mockRoute: Partial<ActivatedRoute> = {
      paramMap: of(convertToParamMap({ terminalID: testTerminal.terminalId }))
    };

    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatDividerModule,
        MatGridListModule,
        RouterTestingModule,
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot()
      ],
      declarations: [TerminalViewComponent],
      providers: [
        ToastrService,
        {
          provide: WebsocketService,
          useValue: wsServiceSpy
        },
        {
          provide: NaturidentService,
          useValue: niServiceSpy
        },
        {
          provide: ActivatedRoute,
          useValue: mockRoute
        }
      ]
    })
    .compileComponents();

    camMockMessage = new TextEncoder().encode('mockString');

    sanitizer = TestBed.inject(DomSanitizer);

    statusMockSubject = new Subject<string>();
    scanStatusMockSubject = new Subject<ScanStatusUpdate>();
    getShipmentMockSubject = new Subject<Shipment>();
    getTerminalMockSubject = new Subject<TerminalModel>();


    mockNIService = TestBed.inject(NaturidentService);
    mockNIService.getShipment.and.returnValue(getShipmentMockSubject.asObservable());
    mockNIService.getTerminal.and.returnValue(getTerminalMockSubject.asObservable());

    mockWSService = TestBed.inject(WebsocketService);
    mockWSService.getCamImageUpdateListener.and.returnValue(of(camMockMessage));
    mockWSService.getStatusArrivalUpdateListener.and.returnValue(statusMockSubject.asObservable());
    mockWSService.getScanStatusUpdateListener.and.returnValue(scanStatusMockSubject.asObservable());

    
    statusMockSubject.next(TerminalStatusArrival.scan);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminalViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get terminalid from route', () => {
    expect(component.terminalId).toBeDefined();
    expect(component.terminalId).toEqual(testTerminal.terminalId);
  });

  it('should get Terminal', () => {
    expect(mockNIService.getTerminal).toHaveBeenCalledWith(testTerminal.terminalId);
    getTerminalMockSubject.next(testTerminal);
    expect(component.terminal).toBeDefined();
    expect(component.terminal.terminalId).toEqual(testTerminal.terminalId);
  });

  it('should subscribe to streams after getting terminal', () => {
    getTerminalMockSubject.next(testTerminal);

    expect(mockWSService.getCamImageUpdateListener).toHaveBeenCalledWith(testTerminal.terminalId)
    expect(mockWSService.subscribeCamStream).toHaveBeenCalledWith(testTerminal.terminalId)
    
    expect(mockWSService.getScanStatusUpdateListener).toHaveBeenCalledWith(testTerminal.terminalId)
    expect(mockWSService.subscribeScanStatusStream).toHaveBeenCalledWith(testTerminal.terminalId)

    expect(mockWSService.getStatusArrivalUpdateListener).toHaveBeenCalledWith(testTerminal.terminalId)
    expect(mockWSService.subscribeTerminalStatusStream).toHaveBeenCalledWith(testTerminal.terminalId)
  });

  it('should get img string from service', () => {
    getTerminalMockSubject.next(testTerminal);

    expect(mockWSService.getCamImageUpdateListener).toBeDefined();
    const expected = sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64, ' + camMockMessage.toString()) as any;
    expect(component.camImg).toEqual(expected);
  });

  it('should have status scanning', () => {
    expect(mockWSService.getStatusArrivalUpdateListener).toBeDefined();
    expect(component.currentStatusArrival).toEqual(TerminalStatusArrival.scan);
  });

  it('should switch to new status', () => {
    getTerminalMockSubject.next(testTerminal);

    statusMockSubject.next(TerminalStatusArrival.signature);
    expect(component.currentStatusArrival).toEqual(TerminalStatusArrival.signature);

    statusMockSubject.next(TerminalStatusArrival.sending);
    expect(component.currentStatusArrival).toEqual(TerminalStatusArrival.sending);

    statusMockSubject.next(TerminalStatusArrival.done);
    expect(component.currentStatusArrival).toEqual(TerminalStatusArrival.done);

    const pd: PalletDetail = {
      palletId: 'p123',
      pallet_type: 'Europoolpalette (Pressspan)',
      status: 'Im Lager',
      first_ever_scan: new Date('01.01.1970'),
      last_scan: new Date('15.02.2021'),
      last_event: 'Wareneingang',
      shipment_id: 'shipment1',
      scans: [
        {
          scanId: 'scan1',
          certainty: 0.42,
          confirmed: null,
          terminalId: 'terminal1',
          timeStamp: new Date(),
          pallet_feet: []
        }
      ]
    };

    const shipment: Shipment = {
      shipment_id: 'shipment1',
      carrier_detail: pd,
      product_info: {
        country_of_origin: 'Germany',
        description: 'Spargel',
        weight: 30,
        warning_labels: []
      },
      shipment_detail: {
        sscc_nve: '141421356237309504123',
        last_destination: 'Umschlaglager',
        sender: 'Fraunhofer IML, Joseph-von-Fraunhofer Straße 2-4, 44227 Dortmund',
        next_destination: 'Testzentrum',
        recipient: 'Fraunhofer IML, Joseph-von-Fraunhofer Straße 2-4, 44227 Dortmund'
      }
    }

    const scanUpdate: ScanStatusUpdate = {
      message: 'the scan was sucessfully processed',
      status: ScanStatus.done,
      palletId: 'p123', 
      scanId: 'scan1',
      shipmentId: 'shipment1'
    }

    scanStatusMockSubject.next(scanUpdate);
    getShipmentMockSubject.next(shipment);
    expect(mockNIService.getShipment).toHaveBeenCalledWith('shipment1');
    expect(component.lastScanPalletDetail).toBeDefined();
    expect(component.lastScanPalletDetail).toBeTruthy();
    expect(component.lastScanPalletDetail.palletId).toEqual('p123');

    statusMockSubject.next(TerminalStatusArrival.scan);
    expect(component.currentStatusArrival).toEqual(TerminalStatusArrival.scan);
  });

  it('should display an error if the scan processing fails', () => {
    const scanUpdate: ScanStatusUpdate = {
      message: 'error processing scan',
      status: ScanStatus.error,
      palletId: 'p123', 
      scanId: 'scan1',
      shipmentId: null
    }
    scanStatusMockSubject.next(scanUpdate);
  });

  it('should display an error if the shipment was not found', () => {
    getTerminalMockSubject.next(testTerminal);

    const scanUpdate: ScanStatusUpdate = {
      message: 'the scan was sucessfully processed',
      status: ScanStatus.done,
      palletId: 'p123', 
      scanId: 'scan1',
      shipmentId: 'shipment1'
    }
    scanStatusMockSubject.next(scanUpdate);
    getShipmentMockSubject.error(new Error('failed shipment'));
    expect(mockNIService.getShipment).toHaveBeenCalledWith('shipment1');
  });

  it('should error status on invalid incoming status', () => {
    getTerminalMockSubject.next(testTerminal);

    statusMockSubject.next('invalidStatus');
    expect(component.currentStatusArrival).toEqual(TerminalStatusArrival.error);
  });
});
