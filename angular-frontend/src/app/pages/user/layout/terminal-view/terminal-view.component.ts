// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';
import {TerminalStatusArrival} from '../../../../models/TerminalStatus';
import {PalletDetail} from 'src/app/models/pallet-model';
import { ScanStatus, ScanStatusUpdate } from 'src/app/models/ScanStatus';
import { ToastrService } from 'ngx-toastr';
import { Shipment } from 'src/app/models/shipment-model';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { TerminalModel } from 'src/app/models/terminal-model';
import { WebsocketService } from 'src/app/shared/services/api/websocket.service';
import { NaturidentService } from 'src/app/shared/services/api/naturident.service';

@Component({
  selector: 'app-terminal-view',
  templateUrl: './terminal-view.component.html',
  styleUrls: ['./terminal-view.component.scss']
})
export class TerminalViewComponent implements OnInit, OnDestroy {
  terminalStatusArrival = TerminalStatusArrival;
  camImg = '';
  private camSubscription: Subscription;
  private statusArrivalSubscription: Subscription;
  private scanStatusSubscription: Subscription;
  currentStatusArrival: TerminalStatusArrival = TerminalStatusArrival.scan;

  lastScanShipment: Shipment;
  lastScanPalletDetail: PalletDetail;
  terminalId: string;
  terminal: TerminalModel;


  constructor(private route: ActivatedRoute,
              private webSocketService: WebsocketService, 
              private natureidentService: NaturidentService,
              private sanitizer: DomSanitizer,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.getTerminal();
  }

  subscribeStreams() {
    if (this.terminalId) {
      this.closeSubscriptions();

      this.camSubscription = this.webSocketService.getCamImageUpdateListener(this.terminalId).subscribe(
        (base64string: any) => {
          // We need to explicitly call bypassSecurityTrustUrl in order to display the base64 string as it exposes a security risk
          const img64str = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64, ' + base64string) as any;
          this.camImg = img64str;
        }
      );
      this.webSocketService.subscribeCamStream(this.terminalId);

      this.scanStatusSubscription = this.webSocketService.getScanStatusUpdateListener(this.terminalId).subscribe({
        next: (statusUpdate: ScanStatusUpdate) => {
          switch(statusUpdate.status) {
            case ScanStatus.done: {
              console.warn(statusUpdate);
              this.lastScanPalletDetail = null;
              this.lastScanShipment = null;

              this.natureidentService.getShipment(statusUpdate.shipmentId).subscribe({
                next: (shipment: Shipment) => {
                  const pallet = shipment.carrier_detail;
                  const scan = pallet.scans.find(s => s.scanId == statusUpdate.scanId);
                  if (scan) {
                    this.lastScanPalletDetail = pallet;
                    this.lastScanShipment = shipment;
                  } else {
                    console.error(`The scan was not found`);
                    this.toastr.error('The scan was not found', 'Scan');
                  }
                },
                error: error => {
                  console.error(`Error receiving last scan for terminal`);
                  this.toastr.error(`Error receiving last scan for terminal`, 'Scan');
                }
              });
              break;
            }
            case ScanStatus.error: {
              console.error(`There was an error processing the scan`);
              this.toastr.error(statusUpdate.message, 'Scan error');
              break;
            }
          }
        }
      });
      this.webSocketService.subscribeScanStatusStream(this.terminalId);

      this.statusArrivalSubscription = this.webSocketService.getStatusArrivalUpdateListener(this.terminalId).subscribe(
        (newStatus) => {
          console.log('New status received: ', newStatus);
          switch (newStatus) {
            case TerminalStatusArrival.scan: {
              this.currentStatusArrival = TerminalStatusArrival.scan;
              break;
            }
            case TerminalStatusArrival.signature: {
              this.currentStatusArrival = TerminalStatusArrival.signature;
              break;
            }
            case TerminalStatusArrival.sending: {
              this.currentStatusArrival = TerminalStatusArrival.sending;
              break;
            }
            case TerminalStatusArrival.done: {
              this.currentStatusArrival = TerminalStatusArrival.done;
              break;
            }
            default: {
              console.warn('Wrong status arrival update received: ', newStatus);
              this.currentStatusArrival = TerminalStatusArrival.error;
              break;
            }
          }
        }
      );

      this.webSocketService.subscribeTerminalStatusStream(this.terminalId);
    }
  }
  
  getTerminal() {
    this.route.paramMap.subscribe(
      (paramMap: ParamMap) => {
        // TODO Error Handling - shipment not found + does not have id
        if (paramMap.has('terminalID')) {

          this.terminalId = paramMap.get('terminalID');
          this.natureidentService.getTerminal(this.terminalId).subscribe({
            next: terminal => {
              this.terminal = terminal;
              this.subscribeStreams();
            },
            error: e => {
              this.toastr.error(`Error receiving Termianl`, 'Terminal');
            }
          });
        } else {
          console.error('Could not find terminalID');
          this.toastr.error(`Could not find terminalID`, 'Terminal');
        }
      }
    );
  }

  updatePalletScanConfirmation(newvalue: boolean) {
    const palletId = this.lastScanPalletDetail.palletId;
    const scanId = this.lastScanPalletDetail.scans.slice(-1)[0].scanId;
    this.natureidentService.updatePalletScanConfirmation(palletId, scanId, newvalue).subscribe({
      next: res => {
        this.toastr.success(`Scan confirmation updated`, 'Confirmation');  
      },
      error: e => {
        this.toastr.error(`Error updating the confirmation of a scan`, 'Confirmation');
      }
    });
    this.lastScanPalletDetail = null;
  }

  confirmLastPalletScan() {
    this.updatePalletScanConfirmation(true);
  }

  declineLastPalletScan() {
    this.updatePalletScanConfirmation(false);
  }

  closeSubscriptions() {
    if (this.camSubscription) {
      this.camSubscription.unsubscribe();
    }

    if (this.statusArrivalSubscription) {
      this.statusArrivalSubscription.unsubscribe();
    }

    if(this.scanStatusSubscription) {
      this.scanStatusSubscription.unsubscribe();
    }
  }

  ngOnDestroy(): void {
    // we have to explicitly call unsubscribe for observables we created ourselves to prevent memory leaks
    this.closeSubscriptions();
  }
}
