// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {OrderOverviewComponent} from './layout/order-overview/order-overview.component';
import {ContentComponent} from './layout/content/content.component';
import {TerminalViewComponent} from './layout/terminal-view/terminal-view.component';


const routes: Routes = [
  {path: 'overview', component: OrderOverviewComponent},
  {path: 'content/:objID', component: ContentComponent},
  {path: 'content', component: ContentComponent},
  {path: 'terminal/:terminalID', component: TerminalViewComponent},
  {path: '', component: OrderOverviewComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
