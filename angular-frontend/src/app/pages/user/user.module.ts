// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserRoutingModule} from './user-routing.module';
import {HttpClientModule} from '@angular/common/http';

import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';

import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatDividerModule} from '@angular/material/divider';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';

import {MatInputModule} from '@angular/material/input';
import {ContentComponent} from './layout/content/content.component';
import {CarrierImagesComponent} from './layout/content/carrier-images/carrier-images.component';
import {
  CarrierDetailComponent,
  CarrierDetailEditDialog
} from './layout/content/carrier-detail/carrier-detail.component';
import {
  ShipmentDetailComponent,
  ShipmentDetailEditDialog
} from './layout/content/shipment-detail/shipment-detail.component';
import {ProductInformationComponent} from './layout/content/product-information/product-information.component';
import {OrderOverviewComponent} from './layout/order-overview/order-overview.component';
import {MatSortModule} from '@angular/material/sort';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {TerminalViewComponent} from './layout/terminal-view/terminal-view.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


@NgModule({
  declarations: [
    ContentComponent,
    CarrierImagesComponent,
    CarrierDetailComponent,
    ShipmentDetailComponent,
    ProductInformationComponent,
    OrderOverviewComponent,
    TerminalViewComponent,
    CarrierDetailEditDialog,
    ShipmentDetailEditDialog,
  ],
  entryComponents: [
    CarrierDetailEditDialog,
    ShipmentDetailEditDialog
  ],
  imports: [
    MatListModule,
    MatCardModule,
    MatProgressBarModule,
    HttpClientModule,
    MatTableModule,
    MatDividerModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatButtonModule,
    CommonModule,
    UserRoutingModule,
    LoggerModule.forRoot({
      serverLoggingUrl: '/api/logs',
      level: NgxLoggerLevel.TRACE,
      serverLogLevel: NgxLoggerLevel.ERROR,
      disableConsoleLogging: false
    }),
    MatSortModule,
    FormsModule,
    MatExpansionModule,
    MatDialogModule,
    MatProgressSpinnerModule,


  ],
  providers: [HttpClientModule],
})
export class UserModule {
}
