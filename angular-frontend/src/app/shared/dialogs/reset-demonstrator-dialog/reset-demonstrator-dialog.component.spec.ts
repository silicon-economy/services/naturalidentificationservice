import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import { ToastrModule, ToastrService } from 'ngx-toastr';
import { Observable, of, Subject, throwError } from 'rxjs';
import { NaturidentService } from '../../services/api/naturident.service';
import { ResetDemonstratorDialogComponent } from './reset-demonstrator-dialog.component';


export class MatDialogRefMock {
  private closedSubject: Subject<any> = new Subject<any>();

  afterClosed(): Observable<any> {
    return this.closedSubject.asObservable();
  }

  close(val: any = undefined) {
    this.closedSubject.next(val)
  }
}

describe('ResetDemonstratorDialogComponent', () => {
  let component: ResetDemonstratorDialogComponent;
  let fixture: ComponentFixture<ResetDemonstratorDialogComponent>;
  
  let mockNIService;
  let mockToastrService;


  beforeEach(async () => {
    const toastrServiceSpy = jasmine.createSpyObj('ToastrService', ['success', 'error']);
    const niServiceSpy = jasmine.createSpyObj('NaturidentService', ['resetDemonstrator']);
    await TestBed.configureTestingModule({
      declarations: [ ResetDemonstratorDialogComponent ],
      imports: [
        MatDialogModule,
        ToastrModule.forRoot(),
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: ToastrService,
          useValue: toastrServiceSpy
        },
        {
          provide: NaturidentService,
          useValue: niServiceSpy
        },
        {
          provide: MatDialogRef, 
          useClass: MatDialogRefMock,
        }
      ]
    })
    .compileComponents();

    
    mockNIService = TestBed.inject(NaturidentService);
    mockToastrService = TestBed.inject(ToastrService);

    mockToastrService.success.and.returnValue(null);
    mockToastrService.error.and.returnValue(null);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetDemonstratorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the reset if dialog was submitted endpoint', () => {
    mockNIService.resetDemonstrator.and.returnValue(of({}));

    component.dialogRef.close(true);

    expect(mockNIService.resetDemonstrator).toHaveBeenCalled();
    expect(mockToastrService.success).toHaveBeenCalled();
    expect(mockToastrService.error).toHaveBeenCalledTimes(0);
  });

  it('should display an error if the call to the reset endpoint fails', () => {
    mockNIService.resetDemonstrator.and.returnValue(throwError(null));

    component.dialogRef.close(true);

    expect(mockNIService.resetDemonstrator).toHaveBeenCalled();
    expect(mockToastrService.error).toHaveBeenCalled();
    expect(mockToastrService.success).toHaveBeenCalledTimes(0);
  });

  it('should not call reset endpoint if the dialog was canceld', () => {
    mockNIService.resetDemonstrator.and.returnValue(of({}));

    component.dialogRef.close();

    expect(mockNIService.resetDemonstrator).toHaveBeenCalledTimes(0);
    expect(mockToastrService.error).toHaveBeenCalledTimes(0);
    expect(mockToastrService.success).toHaveBeenCalledTimes(0);
  });
});
