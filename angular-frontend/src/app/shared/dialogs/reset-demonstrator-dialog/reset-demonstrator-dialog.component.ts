import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { NaturidentService } from '../../services/api/naturident.service';

@Component({
  selector: 'app-reset-demonstrator-dialog',
  templateUrl: './reset-demonstrator-dialog.component.html',
  styleUrls: ['./reset-demonstrator-dialog.component.scss']
})
export class ResetDemonstratorDialogComponent implements OnInit {

  constructor(
              public dialogRef: MatDialogRef<ResetDemonstratorDialogComponent>,
              public naturIdentService: NaturidentService, 
              public toastr: ToastrService) { }

  ngOnInit(): void {
    this.dialogRef.afterClosed().subscribe(result => {
      if (result) { // check if dialog was canceled
        this.naturIdentService.resetDemonstrator().subscribe({
          next: obj => {
            this.toastr.success("Demonstrator is now reset");
          },
          error: e => {
            this.toastr.error("Failed to reset the demonstrator");
          }
        });
      }
    });
  }
}
