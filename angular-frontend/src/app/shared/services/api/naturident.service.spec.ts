// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { TestBed } from '@angular/core/testing';

import { NaturidentService } from './naturident.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { PalletDetail } from '../../../models/pallet-model';
import { PalletScanDetail } from '../../../models/pallet-scan-model';
import { Shipment } from '../../../models/shipment-model';
import { TerminalModel } from '../../../models/terminal-model';


describe('NaturidentService', () => {

  let service: NaturidentService;
  let httpMock: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [NaturidentService]
    });

    // inject service
    service = TestBed.inject(NaturidentService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    const exampleService: NaturidentService = TestBed.inject(NaturidentService);
    expect(exampleService).toBeTruthy();
  });

  /**
  * get http answer through a http mock, check that the shipment is correct
  */
   it('should get the correct shipment', () => {
    service.getShipment('shipment1').subscribe((data: any) => {
      expect(data[0].shipment_id).toBe('shipment1');
    });

    const req = httpMock.expectOne(`http://${window.location.hostname}:5000/shipments/shipment1`, 'call to api');
    expect(req.request.method).toBe('GET');

    const testShipment: Shipment = {
      shipment_id: 'shipment1',
      product_info: null,
      shipment_detail: null,
      carrier_detail: {
        palletId: 'p100',
        pallet_type: 'Europoolpalette (Pressspan)',
        status: 'Im Lager',
        first_ever_scan: new Date('01.01.2001'),
        last_scan: new Date('15.02.2021'),
        last_event: 'Wareneingang',
        shipment_id: '1',
        scans: []
      }
    };

    req.flush([testShipment]);

    httpMock.verify();
  });


  /**
  * get http answer through a http mock, check that the terminal is correct
  */
   it('should get the correct terminal', () => {
    service.getTerminal('terminal1').subscribe((data: any) => {
      expect(data[0].terminalId).toBe('terminal1');
    });

    const req = httpMock.expectOne(`http://${window.location.hostname}:5000/terminals/terminal1`, 'call to api');
    expect(req.request.method).toBe('GET');

    const testTerminal: TerminalModel = {
      terminalId: "terminal1",
      terminalType: "arrival"
    }

    req.flush([testTerminal]);

    httpMock.verify();
  });

  it('should make a PUT call to api for pallet update', () => {
    const testPallet: PalletDetail = {
      palletId: 'p100',
      pallet_type: 'Europoolpalette (Pressspan)',
      status: 'Im Lager',
      first_ever_scan: new Date('01.01.2001'),
      last_scan: new Date('15.02.2021'),
      last_event: 'Wareneingang',
      shipment_id: '1',
      scans: []
    };

    service.updatePallet('p100', testPallet).subscribe((data: any) => {
      expect(data[0].palletId).toBe('p100');
    });

    const req = httpMock.expectOne(`http://${window.location.hostname}:5000/pallets/p100`, 'call to api');
    expect(req.request.method).toBe('PUT');

    req.flush([testPallet]);

    httpMock.verify();
  });

  it('should make a PUT call to api for shipment detail update', () => {
    const testShipment: Shipment = {
      shipment_id: 'shipment1',
      product_info: null,
      shipment_detail: {
        last_destination: 'Munich',
        next_destination: 'Dortmund',
        recipient: 'Fraunhofer IML',
        sender: 'Fraunhofer',
        sscc_nve: '123'
      },
      carrier_detail: {
        palletId: 'p100',
        pallet_type: 'Europoolpalette (Pressspan)',
        status: 'Im Lager',
        first_ever_scan: new Date('01.01.2001'),
        last_scan: new Date('15.02.2021'),
        last_event: 'Wareneingang',
        shipment_id: 'shipment1',
        scans: []
      }
    };

    service.updateShipmentDetail(testShipment, testShipment.shipment_detail).subscribe((data: any) => {
      expect(data[0].shipment_id).toBe('shipment1');
    });

    const req = httpMock.expectOne(`http://${window.location.hostname}:5000/shipments/shipment1`, 'call to api');
    expect(req.request.method).toBe('PUT');
    expect(!('carrier_detail' in req.request.body))

    req.flush([testShipment]);

    httpMock.verify();
  });

  /**
  * get http answer through a http mock, check that the pallet is the first in the getPallet()-Observable
  */
  it('should get the correct pallet', () => {
    service.getPallet('p100').subscribe((data: any) => {
      expect(data[0].palletId).toBe('p100');
    });

    const req = httpMock.expectOne(`http://${window.location.hostname}:5000/pallets/p100`, 'call to api');
    expect(req.request.method).toBe('GET');

    const testPallet: PalletDetail = {
      palletId: 'p100',
      pallet_type: 'Europoolpalette (Pressspan)',
      status: 'Im Lager',
      first_ever_scan: new Date('01.01.2001'),
      last_scan: new Date('15.02.2021'),
      last_event: 'Wareneingang',
      shipment_id: '1',
      scans: []
    };

    req.flush([testPallet]);

    httpMock.verify();
  });

  /**
  * get http answer through a http mock, check that the pallet is the first in the getTerminalLastScanPalletDetails()-Observable
  */
   it('should get the correct last scanned pallet of a terminal', () => {
    service.getTerminalLastScanPalletDetails('t100').subscribe((data: any) => {
      expect(data[0].palletId).toBe('p100');
    });

    const req = httpMock.expectOne(`http://${window.location.hostname}:5000/terminals/t100/lastScannedPallet`, 'call to api');
    expect(req.request.method).toBe('GET');

    const testPallet: PalletDetail = {
      palletId: 'p100',
      pallet_type: 'Europoolpalette (Pressspan)',
      status: 'Im Lager',
      first_ever_scan: new Date('01.01.2001'),
      last_scan: new Date('15.02.2021'),
      last_event: 'Wareneingang',
      shipment_id: '1',
      scans: [
        <PalletScanDetail> {
          terminalId: 't100',
          certainty: 0.55,
          confirmed: true,
          pallet_feet: [],
          scanId: 'scan123',
          timeStamp: new Date(2021, 6, 1)
        }
      ]
    };

    req.flush([testPallet]);

    httpMock.verify();
  });

  /**
  * get http answers through a http mock, to check that both calls happen
  */
   it('should call delete and post on the general endpoint when calling resetDemonstrator', () => {
    service.resetDemonstrator().subscribe((data: any) => {
      expect(data).toBeDefined();
    });

    const requests = httpMock.match(`http://${window.location.hostname}:5000/general/`);
    expect(requests.length).toEqual(1)

    requests[0].flush(["OK"])


    const requests2 = httpMock.match(`http://${window.location.hostname}:5000/general/`);
    expect(requests2.length).toEqual(1)
    requests2[0].flush(["OK"])

    expect(requests[0].request.method).toBe('DELETE');
    expect(requests2[0].request.method).toBe('POST');

    httpMock.verify();
  });

  it('should create pallet feet image url', () => {
    const palletId = 'p100';
    const scanId = 's1000';
    const palletFeetIdx = 1;

    const url = service.getPalletFeetImageUrl(palletId, scanId, palletFeetIdx);
    expect(url.endsWith('pallets/p100/s1000/1')).toBeTruthy();
  });

});
