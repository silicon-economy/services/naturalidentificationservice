// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';
import { Shipment, ShipmentDetail } from 'src/app/models/shipment-model';
import { PalletDetail } from 'src/app/models/pallet-model';
import { TerminalModel } from 'src/app/models/terminal-model';

@Injectable({
  providedIn: 'root'
})
export class NaturidentService {


  private static readonly API_URL = `${environment.apiHost}:${environment.apiPort}`;

  constructor(private http: HttpClient) {
    console.log("API_URL: " + NaturidentService.API_URL);
  }

  getShipments(): Observable<Shipment[]> {
    // Method to load the shipment data from the backend
    const url = NaturidentService.API_URL + `/shipments`;
    return this.http.get<Shipment[]>(url);
  }

  getShipment(shipment_id: string): Observable<Shipment> {
    const url = NaturidentService.API_URL + `/shipments/${shipment_id}`;
    return this.http.get<Shipment>(url);
  }

  getTerminal(terminalId: string): Observable<TerminalModel> {
    const url = NaturidentService.API_URL + `/terminals/${terminalId}`;
    return this.http.get<TerminalModel>(url);
  }

  getTerminalLastScanPalletDetails(terminalid: string): Observable<PalletDetail> {
    const lastscan_url = NaturidentService.API_URL + `/terminals/${terminalid}/lastScannedPallet`;
    return this.http.get<PalletDetail>(lastscan_url);
  }

  getPallet(palletid: string): Observable<PalletDetail> {
    const url = NaturidentService.API_URL + `/pallets/${palletid}`;
    return this.http.get<PalletDetail>(url);
  }

  resetDemonstrator(): Observable<Object> {
    const update_url = NaturidentService.API_URL + `/general/`;

    return this.http.delete(update_url).pipe(
      concatMap(_deleData => this.http.post(update_url, {}).pipe(map(_createTerminals => ({}))))
    );
  }

  updatePallet(palletId: string, pallet: PalletDetail): Observable<PalletDetail> {
    const update_url = NaturidentService.API_URL + `/pallets/${palletId}`;
    const body = {
      "palletId": pallet.palletId,
      "pallet_type": pallet.pallet_type,
      "first_ever_scan": pallet.first_ever_scan,
      "last_scan": pallet.last_scan,
      "last_event": pallet.last_event,
      "status": pallet.status,
      "shipment_id": pallet.shipment_id,
    };
    return this.http.put<PalletDetail>(update_url, body);
  }

  updateShipmentDetail(shipment: Shipment, shipmentDetail: ShipmentDetail): Observable<Shipment> {
    const update_url = NaturidentService.API_URL + `/shipments/${shipment.shipment_id}`;
    const body = {
      shipment_id: shipment.shipment_id,
      shipment_detail: shipmentDetail
    };
    return this.http.put<Shipment>(update_url, body);
  }

  updatePalletScanConfirmation(palletId: string, scanId: string, confirmed: boolean) {
    const update_url = NaturidentService.API_URL + `/pallets/${palletId}/${scanId}`;
    const body = {
      "confirmed": confirmed
    };
    return this.http.put(update_url, body);
  }

  getPalletFeetImageUrl(palletId: string, scanId: string, idx: number): string {
    return NaturidentService.API_URL + `/pallets/${palletId}/${scanId}/${idx}`;
  }
}
