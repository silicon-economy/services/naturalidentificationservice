// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {TestBed} from '@angular/core/testing';

import {WebsocketService} from './websocket.service';
import {Observable, of} from 'rxjs';
import { OrderIdServiceSocket } from 'src/app/app.module';
import { TerminalStatusArrival } from 'src/app/models/TerminalStatus';
import { ScanStatus, ScanStatusUpdate } from 'src/app/models/ScanStatus';


describe('WebsocketService', () => {
  const spy = jasmine.createSpyObj('Socket', ['on']);
  let mockObservable: Observable<Uint8Array>;
  let mockMessage: string;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [],
      providers: [{
        provide: OrderIdServiceSocket,
        useValue: spy
      }]
    });


    mockMessage = 'mockstringfortesting';
    const uint8array = new TextEncoder().encode(mockMessage);

    mockObservable = of(uint8array);

    const mockSocket = TestBed.inject(OrderIdServiceSocket);
    // mockSocket.on.and.returnValue(mockObservable); // ????
  });

  it('should be created', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);

    expect(service).toBeTruthy();
  });

  it('should return empty string', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);
    const emptyStr = '';
    const uint8array = new TextEncoder().encode(emptyStr);
    expect(emptyStr).toEqual(service.uint8ToString(uint8array));
  });

  it('should return same string', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);
    let exampleStr = '';
    for (let i = 0; i < 2048; i += 1) {
      exampleStr = exampleStr.concat(String(i));
    }
    const uint8array = new TextEncoder().encode(exampleStr);
    expect(exampleStr).toEqual(service.uint8ToString(uint8array));
  });

  it('should return an observable', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);
    expect(service.getCamImageUpdateListener('tdeparture')).toBeInstanceOf(Observable);
  });

  it('should return an observable', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);
    expect(service.getScanStatusUpdateListener('tdeparture')).toBeInstanceOf(Observable);
  });

  it('should contain new imgstring', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);
    const message = {
      message: 'hello, test!',
      img64: new TextEncoder().encode('test')
    };

    var observable = service.getCamImageUpdateListener('tdeparture');
    var observableArrival = service.getCamImageUpdateListener('tarrival');

    
    service._handleCamStreamMessage('tdeparture', message);
    service._handleCamStreamMessage('tarrival', message);


    var expectedImage = service.uint8ToString(message.img64);
    observable.subscribe({
      next: m => {
        expect(expectedImage).toEqual(m);
      }
    });

    observableArrival.subscribe({
      next: m => {
        expect(expectedImage).toEqual(m);
      }
    });
  });

  it('should contain new status', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);
    const message = {
      message: 'hello, test!',
      status: TerminalStatusArrival.done
    };

    // use ts ignore here to access private fields for unit testing
    // @ts-ignore
    expect(service.statusArrival).toEqual(TerminalStatusArrival.scan);
    service._handleTerminalStatusStreamMessage('tarrival', message);
    service._handleTerminalStatusStreamMessage('tdeparture', {status: TerminalStatusArrival.sending, message: 'hello departure terminal'});
    // @ts-ignore
    expect(service.statusArrival).toEqual(TerminalStatusArrival.done);
    // @ts-ignore
    expect(service.statusDeparture).toEqual(TerminalStatusArrival.sending);
  });

  it('should forward new scan status', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);
    const messageDeparture: ScanStatusUpdate = {
      message: 'hello, test!',
      status: ScanStatus.done,
      scanId: 'scan123',
      palletId: 'pallet123',
      shipmentId: 'shipment123'
    };
    const messageArrival: ScanStatusUpdate = {
      message: 'hello, arrival terminal!',
      status: ScanStatus.done,
      scanId: 'scan1',
      palletId: 'pallet1',
      shipmentId: 'shipment1'
    };

    var observableDeparture = service.getScanStatusUpdateListener('tdeparture');
    var observableArrival = service.getScanStatusUpdateListener('tarrival');


    service._handleScanStatusStreamMessage('tdeparture', messageDeparture);
    service._handleScanStatusStreamMessage('tarrival', messageArrival);
    
    observableDeparture.subscribe({
      next: m => {
        expect(messageDeparture).toEqual(m);
      }
    });

    observableArrival.subscribe({
      next: m => {
        expect(messageArrival).toEqual(m);
      }
    })
  });

  it('should get correct observables for arrival terminal', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);

    expect(service.getCamImageUpdateListener("tarrival")).toBeDefined();
    expect(service.getStatusArrivalUpdateListener("tarrival")).toBeDefined();
    expect(service.getScanStatusUpdateListener("tarrival")).toBeDefined();
  });

  it('should get correct observables for departure terminal', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);

    expect(service.getCamImageUpdateListener("tdeparture")).toBeDefined();
    expect(service.getStatusArrivalUpdateListener("tdeparture")).toBeDefined();
    expect(service.getScanStatusUpdateListener("tdeparture")).toBeDefined();
  });

  it('should return null for unknown terminal ids', () => {
    const service: WebsocketService = TestBed.inject(WebsocketService);

    expect(service.getCamImageUpdateListener("tunknownid")).toBeNull();
    expect(service.getStatusArrivalUpdateListener("tunknownid")).toBeNull();
    expect(service.getScanStatusUpdateListener("tunknownid")).toBeNull();
  });
});
