// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import { OrderIdServiceSocket } from 'src/app/app.module';
import { ScanStatusUpdate } from 'src/app/models/ScanStatus';
import { TerminalStatusArrival } from 'src/app/models/TerminalStatus';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private statusArrival: TerminalStatusArrival = TerminalStatusArrival.scan;
  private statusUpdatedArrival = new Subject<TerminalStatusArrival>();

  private statusDeparture: TerminalStatusArrival = TerminalStatusArrival.scan;
  private statusUpdatedDeparture = new Subject<TerminalStatusArrival>();

  private camArrivalImageListener = new Subject<string>();
  private camDepartureImageListener = new Subject<string>();

  private statusUpdatedScanArrival = new Subject<ScanStatusUpdate>();
  private statusUpdatedScanDeparture = new Subject<ScanStatusUpdate>();


  constructor(private socket: OrderIdServiceSocket) {
    this.socket.on('connect', () => console.log('Socket io connected'));
  }

  uint8ToString(u8a: Uint8Array): string {
    let CHUNK_SZ = 0x8000;
    let c = [];
    for (let i = 0; i < u8a.length; i += CHUNK_SZ) {
      c.push(String.fromCharCode.apply(null, u8a.subarray(i, i + CHUNK_SZ)));
    }
    return c.join('');
  }

  /* istanbul ignore next */
  subscribeCamStream(terminalId: string) {
    this.socket.on('cam_arrival_update/' + terminalId,
      message => {
        this._handleCamStreamMessage(terminalId, message);
      });
  }

  _handleCamStreamMessage(terminalId: string, message) {
    console.log('Received cam arrival update', terminalId, message);
    const arr = new Uint8Array(message.img64);
    const imgstr = this.uint8ToString(arr);

    switch (terminalId) {
      case "tdeparture":
        this.camDepartureImageListener.next(imgstr);    
        break;
      case "tarrival":
          this.camArrivalImageListener.next(imgstr);    
          break;
      default:
        console.warn("Unknown terminalId");
        break;
    }
  }

  /* istanbul ignore next */
  subscribeTerminalStatusStream(terminalId: string) {
    this.socket.on('status_arrival_update/' + terminalId, (message) => {
      this._handleTerminalStatusStreamMessage(terminalId, message);
    });
  }

  _handleTerminalStatusStreamMessage(terminalId: string, message) {
    console.log('Received status arrival update');
    
    switch (terminalId) {
      case "tdeparture":
        this.statusDeparture = message.status;
        this.statusUpdatedDeparture.next(this.statusDeparture);    
        break;
      case "tarrival":
          this.statusArrival = message.status;
          this.statusUpdatedArrival.next(this.statusArrival);   
          break;
      default:
        console.warn("Unknown terminalId");
        break;
    }
  }

  /* istanbul ignore next */
  subscribeScanStatusStream(terminalId: string) {
    this.socket.on('scan_status_update/' + terminalId, (message: ScanStatusUpdate) => {
      this._handleScanStatusStreamMessage(terminalId, message);
    });
  }

  _handleScanStatusStreamMessage(terminalId: string, message: ScanStatusUpdate) {
    console.log('Received scan status update');

    switch (terminalId) {
      case "tdeparture":
        this.statusUpdatedScanDeparture.next(message);    
        break;
      case "tarrival":
          this.statusUpdatedScanArrival.next(message);    
          break;
      default:
        console.warn("Unknown terminalId");
        break;
    }
  }

  getCamImageUpdateListener(terminalId: string): Observable<string> {
    switch (terminalId) {
      case "tdeparture":
        return this.camDepartureImageListener.asObservable();
      case "tarrival":
        return this.camArrivalImageListener.asObservable();
      default:
        console.warn("Unknown terminalId");
        return null;
    }
  }

  getStatusArrivalUpdateListener(terminalId: string): Observable<TerminalStatusArrival> {
    switch (terminalId) {
      case "tdeparture":
        return this.statusUpdatedDeparture.asObservable();
      case "tarrival":
        return this.statusUpdatedArrival.asObservable();
      default:
        console.warn("Unknown terminalId");
        return null;
    }
  }

  getScanStatusUpdateListener(terminalId: string): Observable<ScanStatusUpdate> {
    switch (terminalId) {
      case "tdeparture":
        return this.statusUpdatedScanDeparture.asObservable();
      case "tarrival":
        return this.statusUpdatedScanArrival.asObservable();
      default:
        console.warn("Unknown terminalId");
        return null;
    }
  }
}
