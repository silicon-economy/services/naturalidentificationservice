import { TestBed } from '@angular/core/testing';

import { FooterService } from './footer.service';

describe('FooterService', () => {
  let service: FooterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FooterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should read saved state false on init', () => {
    test_read_saved_state_on_init(service, false);
  });

  it('should read saved state true on init', () => {
    test_read_saved_state_on_init(service, true);
  });

  it('should toggle footer state and save to localstore', () => {
    const localStorageSetSpy = spyOn(Object.getPrototypeOf(localStorage), 'setItem');

    const oldState = service.getFooterState();

    service.toggleFooterState();

    expect(localStorageSetSpy).toHaveBeenCalledOnceWith(service.getStorageKey(), String(!oldState));
    expect(service.getFooterState()).toEqual(!oldState);
  });
});

function test_read_saved_state_on_init(service: FooterService, savedState:boolean) {
  const localStorageGetSpy = spyOn(Object.getPrototypeOf(localStorage), 'getItem').and.returnValue(String(savedState));

  service.initFooterState();


  expect(localStorageGetSpy).toHaveBeenCalledOnceWith(service.getStorageKey());
  expect(service.getFooterState()).toEqual(savedState);
}

