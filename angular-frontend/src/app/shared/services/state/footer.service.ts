import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FooterService {

  private readonly storageKey = 'footer-state';

  private readonly footerState = new BehaviorSubject<boolean>(false);
  readonly footerState$ = this.footerState.asObservable();

  resetFooterState(): void {
    this.footerState.next(false);
  }

  // Initialize footer state from local storage
  initFooterState(): void {
    this.footerState.next((localStorage.getItem(this.storageKey) == 'true'));
  }

  getFooterState(): boolean {
    return this.footerState.getValue();
  }

  setFooterState(state: boolean): void {
    this.footerState.next(state);
    localStorage.setItem(this.storageKey, String(state))
  }

  toggleFooterState(): void {
    this.setFooterState(!this.getFooterState());
  }

  getStorageKey() {
    return this.storageKey;
  }
}
