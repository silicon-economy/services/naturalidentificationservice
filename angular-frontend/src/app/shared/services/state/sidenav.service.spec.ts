import { TestBed } from '@angular/core/testing';

import { SidenavService } from './sidenav.service';

describe('SidenavService', () => {
  let service: SidenavService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SidenavService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should default to true if saved state is undefined', () => {
    const localStorageGetSpy = spyOn(Object.getPrototypeOf(localStorage), 'getItem').and.returnValue(undefined);

    service.initSidenavState();
  
    expect(localStorageGetSpy).toHaveBeenCalledTimes(1);
    expect(service.getSidenavState()).toEqual(true);
  });

  it('should read saved state false on init', () => {
    test_read_saved_state_on_init(service, false);
  });

  it('should read saved state true on init', () => {
    test_read_saved_state_on_init(service, true);
  });

  it('should toggle sidenav state and save to localstore', () => {
    const localStorageSetSpy = spyOn(Object.getPrototypeOf(localStorage), 'setItem');

    const oldState = service.getSidenavState();

    service.toggleSidenavState();

    expect(localStorageSetSpy).toHaveBeenCalledOnceWith(service.getStorageKey(), String(!oldState));
    expect(service.getSidenavState()).toEqual(!oldState);
  });
});

function test_read_saved_state_on_init(service: SidenavService, savedState:boolean) {
  const localStorageGetSpy = spyOn(Object.getPrototypeOf(localStorage), 'getItem').and.returnValue(String(savedState));

  service.initSidenavState();

  expect(localStorageGetSpy).toHaveBeenCalledTimes(2);
  expect(service.getSidenavState()).toEqual(savedState);
}
