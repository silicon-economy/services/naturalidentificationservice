import { TestBed } from '@angular/core/testing';

import { ThemeService } from './theme.service';

describe('ThemeService', () => {
  let service: ThemeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThemeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should default to mode 0 if new mode is invalid', () => {
    // prepare state differnt from default
    service.setMode('light-theme');

    const localStorageGetSpy = spyOn(Object.getPrototypeOf(localStorage), 'getItem').and.returnValue('invalid-mode-name');

    service.cycleMode();

    expect(service.getModeStr()).toEqual('dark-theme');
    expect(localStorageGetSpy).toHaveBeenCalledTimes(1);
  });

  it('should default to mode 0 if new mode is invalid', () => {
    // prepare state differnt from default
    service.setMode('light-theme');

    service.setMode('invalid-mode-name');

    expect(service.getModeStr()).toEqual('dark-theme');
  });

  it('should cycle through modes', () => {
    const localStorageGetSpy1 = spyOn(Object.getPrototypeOf(localStorage), 'getItem').and.returnValue('dark-theme');

    service.cycleMode();

    expect(service.getModeStr()).toEqual('light-theme');
    expect(localStorageGetSpy1).toHaveBeenCalledTimes(1);

    const localStorageGetSpy2 = localStorageGetSpy1.and.returnValue('light-theme');

    service.cycleMode();

    expect(service.getModeStr()).toEqual('dark-theme');
    expect(localStorageGetSpy2).toHaveBeenCalledTimes(2);
  });

  it('should reset mode to dark-theme', () => {
    service.setMode('light-theme');

    expect(service.getModeStr()).toEqual('light-theme');

    service.resetMode();

    expect(service.getModeStr()).toEqual('dark-theme');
  });

  it('should reset theme to blue', () => {
    service.setTheme('yellow');

    expect(service.getThemeStr()).toEqual('yellow');

    service.resetTheme();

    expect(service.getThemeStr()).toEqual('blue');
  });

  it('should return current mode object', () => {
    service.resetMode();

    expect(service.getModeStr()).toEqual('dark-theme');

    expect(service.getModeObj()).toEqual({ name: 'dark-theme', icon: 'dark_mode' });
  });

  it('should return current theme object', () => {
    service.resetTheme();

    expect(service.getThemeStr()).toEqual('blue');

    expect(service.getThemeObj()).toEqual({ name: 'blue', label: 'Blue' });
  });

  it('should default to theme 0 if new theme is invalid', () => {
    // prepare state differnt from default
    service.setTheme('yellow');

    service.setTheme('invalid-theme-name');

    expect(service.getThemeStr()).toEqual('blue');
  });

  it('should return default mode storage key', () => {
    expect(service.getModeStorageKey()).toEqual('current-mode');
  });

  it('should return default theme storage key', () => {
    expect(service.getThemeStorageKey()).toEqual('current-theme');
  });

  it('should return all modes', () => {
    expect(service.getAvailableModes()).toEqual([
      { name: 'dark-theme', icon: 'dark_mode' },
      { name: 'light-theme', icon: 'light_mode' }
    ]);
  });

  it('should return all themes', () => {
    expect(service.getAvailableThemes()).toEqual([
      { name: 'blue', label: 'Blue' },
      { name: 'green', label: 'Green' },
      { name: 'lightblue', label: 'Light Blue' },
      { name: 'darkblue', label: 'Dark Blue' },
      { name: 'lightgreen', label: 'Light Green' },
      { name: 'darkgreen', label: 'Dark Green' },
      { name: 'yellow', label: 'Yellow' },
      { name: 'red', label: 'Red' }
    ]);
  });
});
