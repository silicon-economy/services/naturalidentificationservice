// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

/**
 * Loads environment variables.
 * Needs to be done in assetfolder since environment.ts is barely accessable after build.
 * Attention: New environment variables also need to be added to assets/env.template.js
 * Detailed Information: https://pumpingco.de/blog/environment-variables-angular-docker/
 */

(function(window){
    window["env"]=window["env"]||{}
    window['env']['production'] = false;
}(this));