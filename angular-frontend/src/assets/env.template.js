// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

/**
 * Template to replace environment variables during runtime.
 * Needs to be done in assetfolder since environment.ts is barely accessable after build.
 * Attention: New environment variables also need to be added to assets/env.js.
 * Detailed Information: https://pumpingco.de/blog/environment-variables-angular-docker/
 */

(function(window){
    window["env"]=window["env"]||{}
    window['env']['production'] = "${production}";
    window["env"]["SOCKET_IO_HOST"] = "${SOCKET_IO_HOST}";
    window["env"]["SOCKET_IO_PORT"] = "${SOCKET_IO_PORT}";
    window["env"]["API_HOST"] = "${API_HOST}";
    window["env"]["API_PORT"] = "${API_PORT}";
}(this))