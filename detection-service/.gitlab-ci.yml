# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

# Extend the default stages with the archive stage intended to push artifacts to the nexus
# Bear in mind that the K8s cluster may pick up docker images from the registry by itself if
# a particular imagestream is configured as such
stages:
  - test
  - archive

# The official sonar-scanner-cli docker image contains a python3 installation with pip already
image: "$NEXUS_DOCKER_REGISTRY:$NEXUS_DOCKER_REGISTRY_PORT/sele/cicdtools/python-sonar:latest"

# For merge requests do not `deploy` but only run static code analysis.
sonarqube-check:
#  tags:
#    # This can only run on a runner that has the necessary hardware attached to run the tests
#    - naturident
  stage: test
  #cache:
  #  paths:
  #    - .cache/pip
  #    - venv/
  variables:
    GIT_DEPTH: 0
  before_script:
    - apt update
    - apt install -y libgl1-mesa-glx
    - cd detection-service/
    - pip3 install -r requirements.txt
    - pip3 install pytest-cov pylint
  script:
  - pytest --cov=flaskserver --cov-branch tests/ --cov-report xml:coverage.xml
  - 'pylint --exit-zero flaskserver/ tests/ -r n --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" | tee pylint.txt'
  - >
    sonar-scanner
    -Dsonar.python.xunit.reportPath=nosetests.xml -Dsonar.python.coverage.reportPaths=coverage.xml
    -Dsonar.sources=. -Dsonar.coverage.exclusions=**__init__**,tests/**,config.py,manage.py,setup.py,flaskserver/templates/**  -Dsonar.exclusions=*.xml
    -Dsonar.python.pylint.reportPath=pylint.txt -Dsonar.language=py
    -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_TOKEN
    -Dsonar.projectKey=naturalidentificationservice.detection-service
    -Dsonar.gitlab.commit_sha=$CI_COMMIT_SHA -Dsonar.gitlab.ref_name=$CI_COMMIT_REF_NAME
    -Dsonar.gitlab.url=$CI_PROJECT_URL -Dsonar.gitlab.project_id=$CI_PROJECT_ID
    -Dsonar.qualitygate.wait=true
  rules:
    # Include QA only for merge requests, default branch (master) and tags
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# Just run the tests for everything else
run-tests:
#  tags:
#    # This can only run on a runner that has the necessary hardware attached to run the tests
#    - naturident
  stage: test
  #cache:
  #  paths:
  #    - .cache/pip
  #    - venv/
  before_script:
    - apt update
    - apt install -y libgl1-mesa-glx
    - cd detection-service/
    - pip3 install wheel
    - pip3 install -r requirements.txt
    - pip3 install pytest-cov pylint
  script:
  - pytest --cov=flaskserver --cov-branch tests/ --cov-report xml:coverage.xml
  rules:
    # Exlude tests for merge requests, default branch (master) and tags
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never
    # "Default clause" meaning: run in all other cases, which here means "branches"
    - when: on_success

third-party-license-check:
  stage: test
  variables:
    REFERENCE_LICENSES_FILE: $CI_PROJECT_DIR/detection-service/third-party-licenses/third-party-licenses.csv
    GENERATED_LICENSES_DIR: $CI_PROJECT_DIR/detection-service/generated
    GENERATED_LICENSES_FILE: $CI_PROJECT_DIR/detection-service/generated/third-party-licenses.csv
  script:
    - mkdir -p $GENERATED_LICENSES_DIR
    - cd detection-service/
    - pip3 install -r requirements.txt
    # For now, ignore the "pkg-resources" package as it seems to be listed in the output even though it is not a dependency.
    # This issue might be related: https://bugs.launchpad.net/ubuntu/+source/python-pip/+bug/1635463
    - 'pip-licenses --ignore-packages pkg-resources --with-urls --format=csv --output-file=$GENERATED_LICENSES_FILE'
    - 'cmp --silent $REFERENCE_LICENSES_FILE $GENERATED_LICENSES_FILE || export LICENSES_CHANGED=true'
    - 'if [ ! -z ${LICENSES_CHANGED} ]; then
        echo Some licenses used by the third-party dependencies have changed.;
        echo Please refer to the README and generate/update them accordingly.;
        git diff --no-index --unified=0 $REFERENCE_LICENSES_FILE $GENERATED_LICENSES_FILE;
      fi'
  rules:
    # License check must succeed in master and MRs
    - if: ($CI_COMMIT_BRANCH == "master") || $CI_MERGE_REQUEST_IID
      allow_failure: false
    # "Default clause" meaning: in all other cases
    - when: on_success
      allow_failure: true


#archive:
#  tags:
    # This can run on any runner that has a docker daemon (i.e. use the group runner here)
#    - docker
#  image: "python:3.8"
#  stage: archive
#  before_script:
#    - pip3 install twine
#  script:
#    - cd detection-service/
#    - python3 setup.py sdist bdist
#    - twine upload --repository-url ${NEXUS_PYPI_REPO} --username "$NEXUS_USER" --password "$NEXUS_PASS" dist/*.tar.gz
#  rules:
#    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

