# Naturident detection-service

## Demonstration 

### Video files

1. Example videos are is inside of the docker image
2. External videos can be added in the directory `./videos` which is a mounted docker volume

### Departure Terminal (signature creation)
1. http://localhost:5002/
2. Select "Departure terminal (signature-creation)" 
3. Refresh/List all video files
4. Click play button next to video name
5. Visualisation on http://localhost:8080/ in Departure terminal

### Arrival Terminal (re-identification)
1. http://localhost:5002/
2. Select "Arrival terminal (re-identification)" 
3. Refresh/List all video files
4. Click play button next to video name
5. Visualisation on http://localhost:8080/ in Arrival terminal



# Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.csv` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.csv` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.
* `third-party-licenses-summary.txt` - Contains a summary of all licenses used by the third-party dependencies in this project.
  The content of this file is/can be generated.

### Generating third-party license reports

This project uses [pip-licenses](https://pypi.org/project/pip-licenses/) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.csv` file can be generated using the following command:

`pip-licenses --ignore-packages pkg-resources --with-urls --format=csv --output-file=third-party-licenses/third-party-licenses.csv`

Third-party dependencies for which the licenses cannot be determined automatically by the license-checker have to be documented manually in `third-party-licenses/third-party-licenses-complementary.csv`.
In the `third-party-licenses/third-party-licenses.csv` file these third-party dependencies have an "UNKNOWN" license.

The `third-party-licenses/third-party-license-summary.txt` file can be generated using the following command:

`pip-licenses --ignore-packages pkg-resources --summary --output-file=third-party-licenses/third-party-licenses-summary.txt`
