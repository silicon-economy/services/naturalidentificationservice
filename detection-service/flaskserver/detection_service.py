# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import os
from enum import Enum
import time
import uuid
import random
import cv2
import socketio
import requests
from flask import Flask
from flask import render_template
from flask import request, jsonify
from flaskserver import utils

app = Flask(__name__)

TERMINAL_ID='tarrival'
EXAMPLE_VIDEO_PATH='./example_videos/'
EXTERNAL_VIDEO_PATH='./external_videos/'

@app.route('/')# pragma: no cover
def index():
    return render_template('index.html')

@app.route('/videofiles', methods=['GET'])# pragma: no cover
def on_get_video_files(path_example=EXAMPLE_VIDEO_PATH, path_ext=EXTERNAL_VIDEO_PATH):
    example_files = get_video_files_from_path(path_example)
    external_files = get_video_files_from_path(path_ext)
    return jsonify(example_files + external_files)

def get_video_files_from_path(path: str) -> list:
    """
    get all video files from path
    and create json
    """
    allfiles = os.listdir(path)
    video_files = []
    for filename in sorted(list(allfiles)):
        if filename.endswith(('.avi', '.mp4')):
            video_files.append({'videoname': filename, 'videopath': path + filename})
    return video_files

@app.route('/playvideo', methods=['POST'])# pragma: no cover
def on_process_video():
    """
    get selected/clicked video file
    and start processing -> detection of pallet feets
    """
    video_file = request.json['video_file']
    global TERMINAL_ID
    TERMINAL_ID = request.json['terminal_id']
    len_pallet = process_video(video_file, send_status=True)
    if(len_pallet) == 3:
        return {'message': 'success'}
    return {'message': 'wrong pallet foot count'}

def create_signature_request(pallet):
    """
    Create json post request with pallet feets
    : return json-array
    """
    base64_json_array = []
    foot_pos = 0
    for palletfoot in pallet:
        base64str = utils.encode2base64(palletfoot).decode()
        base64_json_array.append({'image_string': base64str, 'foot_position': str(foot_pos)})
        foot_pos=foot_pos+1
    json_req = {'pallet_feet_images': base64_json_array}
    return json_req

def send_signature_request(url, json_req):# pragma: no cover
    """
    Send json post request with pallet feets
    : return pallet_id from signature-service
    """
    response = requests.post(url, json=json_req)
    if response.ok:
        return response.json()['pallet_id']
    return None


sio = socketio.Client(logger=False, engineio_logger=False)
@sio.event# pragma: no cover
def connect():
    print('Connection established')
@sio.event# pragma: no cover
def connect_error():
    print('The connection failed!')
@sio.event# pragma: no cover
def disconnect():
    print('Disconnected from server')

class ArrivalStatus(Enum):
    SCAN = 'SCANNING'
    SIGNATURE = 'SIGNATURE'
    SENDING = 'SENDING'
    DONE = 'DONE'


def load_model():
    """
    Load model and set input parameters
    : return model
    """
    net = cv2.dnn.readNet('models/pallet_feet.weights', 'models/pallet_feet.cfg')
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
    model = cv2.dnn_DetectionModel(net)
    model.setInputParams(size=(320, 320), scale=1/255, swapRB=True, crop=False)
    return model


def process_video(path, send_status=True):

    """
    detect pallet feets from video and cmbine to single pallet unit
    send frames and status updates to orderid-service
    send images of pallet feets to signature-service
    recive pallet id from signatue-service
    """
    if send_status:
        # Socket connection
        sio.connect('http://order-id-service:5000')
   
   # Load model
    model=load_model()
    
    # Detection model settings
    conf_th = 0.6
    nms_th = 0.1

    # Image format and resize factor for faster detection
    resize_fac=0.5
    cam_width=1936
    cam_height=1216
    center_frame = (int(resize_fac*cam_width/2), int(resize_fac*cam_height/2))

    # Load video file
    cap = cv2.VideoCapture(path)

    # pallet creation vars
    max_dist=250
    pallet = []
    next_pallet_block=True
    pallet_block_idx=0
    status_scan_send=True
    signature_creation_active=False

    # optimal image size of pallet feets (expected from cnn-model)
    signature_img_size = (128, 384)

    # iterate over all video frames
    while (True):
        ret, frame = cap.read()
        if ret is True:
            # resize for faster detection/inference time [1]=width
            resized = cv2.resize(frame, (int(frame.shape[1]*resize_fac), \
                int(frame.shape[0]*resize_fac)))
            # detect box
            box, resized = utils.detect_box(resized, model, conf_th, nms_th)
            if box is not None and not signature_creation_active:
                if status_scan_send:
                    status_scan_send=False
                    if send_status:
                        sio.emit('status_arrival', {'terminalId': TERMINAL_ID, \
                        'message': 'Status update 1', 'status': ArrivalStatus.SCAN.value})
                # get center from detected box and calc horizontal/x distance
                center_box = (int(box[0]+(box[2]/2)), int(box[1]+(box[3]/2)))
                dist_x = center_frame[0] - center_box[0]
                # if pallet_block is left from center of camera and nearer to center
                if dist_x <= max_dist and dist_x > 0 and len(pallet) < 3: # if pallet not complete
                    box_resized = (box / resize_fac).astype(int)
                    cropped_img = frame[box_resized[1]:box_resized[1]+box_resized[3], box_resized[0]:box_resized[0]+box_resized[2]]
                    cv2.putText(resized, str(pallet_block_idx+1), (box[0]+int(box[2]/2.75),\
                        box[1]+int(box[3]/1.75)),cv2.FONT_HERSHEY_SIMPLEX, 3.5, (0,200,220), 7)
                    if next_pallet_block: # add new pb
                        cropped_and_resized = cv2.resize(cropped_img, signature_img_size)
                        pallet.append(cropped_and_resized)
                        next_pallet_block=False
                    else: # Update current pb
                        cropped_and_resized = cv2.resize(cropped_img, signature_img_size)
                        pallet[pallet_block_idx]=cropped_and_resized
                        cv2.putText(resized, str(pallet_block_idx+1), (box[0]+int(box[2]/2.75),\
                        box[1]+int(box[3]/1.75)),cv2.FONT_HERSHEY_SIMPLEX, 3.5, (0,200,220), 7)
                if dist_x < 0 and not next_pallet_block:# Trigger for next pb
                    next_pallet_block=True
                    pallet_block_idx=pallet_block_idx+1
            # Send video frame
            base64str = utils.encode2base64(resized)
            if send_status: # pragma: no cover
                sio.emit('cam_arrival', {'terminalId': TERMINAL_ID, 'img64': base64str})
                time.sleep(0.04)
        if len(pallet)==3 and not signature_creation_active:
            pallet.reverse()
            signature_creation_active=True
            pallet_block_idx=0
            next_pallet_block=True
            status_scan_send=True
            all_feets = cv2.hconcat(pallet)
            all_feets = cv2.resize(all_feets, (128*3, 128))
            base64str = utils.encode2base64(all_feets)
            if send_status: # pragma: no cover
                sio.emit('cam_arrival', {'terminalId': TERMINAL_ID, 'img64': base64str})
                sio.emit('status_arrival', {'terminalId': TERMINAL_ID,\
                     'message': 'Status update 2', 'status': ArrivalStatus.SIGNATURE.value})
                sio.emit('status_arrival', {'terminalId': TERMINAL_ID,\
                     'message': 'Status update 3', 'status': ArrivalStatus.SENDING.value})
            pallet_id = None
            json_req = create_signature_request(pallet)
            if send_status:
                if TERMINAL_ID == 'tarrival':
                    pallet_id = send_signature_request('http://signature-service:5001/signature/reid_pallet', json_req)
                elif TERMINAL_ID == 'tdeparture':
                    pallet_id = send_signature_request('http://signature-service:5001/signature/create_pallet', json_req)
            print(pallet_id)
            pallet_feet = []
            for img in reversed(pallet):
                b64_string = utils.encode2base64(img)
                pallet_feet.append({'img64': b64_string, 'description': 'pos_xy'}) #TODO: Change description to real position
            scan_data = {
                'scanId': str(uuid.uuid4()),
                'terminalId': str(TERMINAL_ID),
                'palletId': str(pallet_id),
                'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HA\
                    wCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
                'certainty': random.random(), #TODO: Request certainty from signature service
                'pallet_feet': pallet_feet
            }
            if send_status:# pragma: no cover
                sio.emit('scan', scan_data)
                sio.emit('status_arrival', {'terminalId': TERMINAL_ID,\
                'message': 'Status update 4', 'status': ArrivalStatus.DONE.value})
                time.sleep(0.5)
                sio.disconnect()
            return len(pallet_feet)

if __name__ =='__main__':
    requests.post('http://order-id-service:5000/general', '')
    app.run(host='0.0.0.0', port=5002, debug=True)
