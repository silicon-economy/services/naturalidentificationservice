// Copyright 2022 Open Logistics Foundation

// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file. 

function generate_table() {
  var video_names = http_get("http://localhost:5002/videofiles");
  console.log(video_names);

  var table = document.createElement("TABLE");
  var columnCount = video_names.length;

  for (var i = 0; i < columnCount; i++) {

    var play_button = document.createElement("INPUT");
    play_button.setAttribute("type", "button");
    play_button.setAttribute("value", "Play");
    play_button.setAttribute("class", "btn inline");
    play_button.setAttribute("id", "list_btn" + i.toString());

    row = table.insertRow(-1);

    var createClickHandler = function(row) {
      return function() {
        var cell = row.getElementsByTagName("label")[0];
        var video_id = cell.innerHTML;
        http_post("http://localhost:5002/playvideo", video_id);
      };
    };
    play_button.onclick = createClickHandler(row);

    var cell_content = document.createElement("DIV");
    cell_content.setAttribute("class", "list_item");
    var cell_text = document.createElement("LABEL");
    cell_text.setAttribute("class", "list_text");
    cell_text.setAttribute("for", "list_btn" + i.toString());
    cell_text.innerHTML = video_names[i]['videopath'];
    cell_content.appendChild(cell_text);
    cell_content.appendChild(play_button);

    var cell = row.insertCell(-1);
    cell.appendChild(cell_content);
  }

  var dvTable = document.getElementById("video_table");
  dvTable.innerHTML = "";
  var headline = document.createElement("H3");
  headline.innerHTML = "Filename";
  dvTable.appendChild(headline);
  dvTable.appendChild(table);
}

function play_video(video_name) {
    http_post(path="http://localhost:5002/playvideo", video_name)
}

function http_get(url){
    var request = new XMLHttpRequest();
    request.open("GET", url, false );
    request.send( null );
    return JSON.parse(request.responseText);
}

function http_post(url, video_name){
    terminalId = document.querySelector('input[name="terminal"]:checked').value
    var data = {'video_file': video_name, 'terminal_id': terminalId};
    var req = new XMLHttpRequest();
    req.open('POST', url, true);
    req.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
    req.onreadystatechange = function () {
        if (req.readyState === 4 && req.status === 200) {
            var res = JSON.parse(req.response);
            console.log(res);
        }
    };
    req.send(JSON.stringify(data));
    alert("The video " + video_name + " was send to terminal " + terminalId + ".");
}
