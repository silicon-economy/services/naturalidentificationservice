# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import cv2
import pybase64 as base64

def encode2base64(frame):
    """
    convert opencv frame to base64
    : return base64-string
    """
    if frame.size == 0:
        return None
    else:
        ret, buffer = cv2.imencode('.jpg', frame)
        if ret == True:
            return base64.b64encode(buffer)

        
def draw_filled_rect(img_original, box, label, alpha=0.05):
    """
    draw detected rectangle (box)
    draw confidence of detection
    : return processed image
    """
    color = [10, 220, 10]
    img_rect_filled = img_original.copy()
    cv2.rectangle(img_rect_filled, box , color=(0, 225, 0), thickness=-1)
    img_processed=cv2.addWeighted(img_rect_filled, alpha, img_original, 1 - alpha, 0)
    img_processed=cv2.rectangle(img_processed, box , color=(0, 225, 0), thickness=3)
    cv2.putText(img_processed, label + ' %', (380, 45), cv2.FONT_HERSHEY_SIMPLEX, 1.5, color, 3)
    return img_processed

def detect_box(resized, model, conf, nms):
    """
    detect and draw pallet feets in the image
    : return processed image 
    """
    class_names = ['palletblock']
    classes, scores, boxes = model.detect(resized, conf, nms)
    if len(boxes) > 0:
        for (classid, score, box) in zip(classes, scores, boxes):
            label = '%s : %.2f' % (class_names[classid], 100*score)
            resized = draw_filled_rect(resized, box, label)
            return box, resized
    else:
        return None, resized