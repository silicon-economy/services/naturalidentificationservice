# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Some tests for detection
"""
import unittest
import numpy as np
import flaskserver.detection_service as ds

class TestDetection(unittest.TestCase):

    def setUp(self):
        self.yolo_model = ""
        self.yolo_model_cfg = ""
        self.image = "base64"

    def test_get_videos(self):
        videofiles = ds.get_video_files_from_path("tests/testdata")
        self.assertEqual(len(videofiles), 1)
        self.assertEqual(type(videofiles), list)

    def test_create_signature_request(self):
        size = 128, 384, 3
        frame = np.zeros(size, dtype=np.uint8)
        pallet = [frame,frame,frame]
        result = ds.create_signature_request(pallet)
        image_data_array = result['pallet_feet_images']
        self.assertEqual(len(image_data_array), 3)
           
    def test_process_video(self):
        result = ds.process_video("tests/testdata/testvideo1.avi", send_status=False)
        self.assertEqual(3, result)
        

if __name__ == '__main__':
    unittest.main()
