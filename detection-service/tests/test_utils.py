# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Some tests for utils
"""
import unittest
import cv2
import numpy as np
from flaskserver import utils

class TestUtils(unittest.TestCase):

    def setUp(self):
        self.yolo_model = "model/pallet_feet.weights"
        self.yolo_model_cfg = "model/pallet_feet.cfg"
        self.image_pallet = "tests/testdata/with_pallet.jpg"
        self.image_no_pallet = "tests/testdata/no_pallet.jpg"

    def test_encode2base64(self):
        size = 128, 384, 3
        frame = np.zeros(size, dtype=np.uint8)
        base64str = utils.encode2base64(frame)
        self.assertNotEqual(base64str, None)
        empty_frame = np.array([])
        base64str_none = utils.encode2base64(empty_frame)
        self.assertEqual(base64str_none, None)

    def test_draw_filled_rect(self):
        size = 200, 200, 3
        frame = np.zeros(size, dtype=np.uint8)
        box = [50, 50, 100, 100]
        draw = utils.draw_filled_rect(frame, box,"test", alpha=0.05)
        self.assertNotEqual((frame == draw).all(), True)

    def test_detect_box(self):
        # init model
        conf_th = 0.90
        nms_th = 0.1
        net = cv2.dnn.readNet("models/pallet_feet.weights", "models/pallet_feet.cfg")
        net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
        net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
        model = cv2.dnn_DetectionModel(net)
        model.setInputParams(size=(320, 320), scale=1/255, swapRB=True, crop=False)

        # load images
        frame_pallet = cv2.imread(self.image_pallet)
        frame_no_pallet = cv2.imread(self.image_no_pallet)

        # run model image with pallet
        resized = cv2.resize(frame_pallet, (int(frame_pallet.shape[1]*0.5),\
            int(frame_pallet.shape[0]*0.5)))
        box, resized = utils.detect_box(resized, model, conf_th, nms_th)
        self.assertEqual(len(box), 4)

        # run model image without pallet
        resized = cv2.resize(frame_no_pallet, (int(frame_no_pallet.shape[1]*0.5),\
            int(frame_no_pallet.shape[0]*0.5)))
        box, resized = utils.detect_box(resized, model, conf_th, nms_th)
        self.assertEqual(box, None)
        self.assertNotEqual(type(np.array), type(resized))

if __name__ == '__main__':
    unittest.main()
