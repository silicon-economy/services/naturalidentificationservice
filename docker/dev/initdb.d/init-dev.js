// Copyright 2022 Open Logistics Foundation
//
// Licensed under the Open Logistics License 1.0.
// For details on the licensing terms, see the LICENSE file.

// Init mongodb for usage

db.auth('admin', 'password');

db = db.getSiblingDB('admin');

db.createUser({
  user: 'user',
  pwd: 'password',
  roles: [
    {
      role: 'readWrite',
      db: 'palletDB'
    }
  ]
});
