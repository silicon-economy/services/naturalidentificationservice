# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=naturalidentificationservice}"

# Switch to project if it exists or create a new one
oc project "$NAMESPACE"
# Upgrade or install
helm upgrade --namespace "$NAMESPACE" -i naturid-angular-frontend . || exit 1
# Ensure image stream picks up the new docker image right away
oc import-image naturid-angular-frontend
