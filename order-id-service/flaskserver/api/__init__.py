# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
API specification

All resources are added in their own namespaces
"""

from flask import Blueprint
from flask_restx import Api

from flaskserver.routes.flask_restx_models import api as models_api
from flaskserver.routes.general import api as general_api
from flaskserver.routes.terminals import api as terminal_api
from flaskserver.routes.pallets import api as pallets_api
from flaskserver.routes.shipments import api as shipments_api


# create the api endpoint
api_bp = Blueprint("api", __name__)
api = Api(api_bp, version="1.0", title="Naturident order-id-service API")


# add namespace here
api.add_namespace(models_api)
api.add_namespace(general_api)
api.add_namespace(terminal_api)
api.add_namespace(pallets_api)
api.add_namespace(shipments_api)


__all__ = ["api_bp"]
