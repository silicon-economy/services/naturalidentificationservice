# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

# pylint: disable=too-few-public-methods

"""
Configuration for the Natureident backend.
"""

import os
from urllib.parse import quote_plus

MONGODB_HOSTNAME = "MONGODB_HOSTNAME"
MONGODB_PORT = "MONGODB_PORT"
MONGODB_DB = "MONGODB_DB"
MONGODB_USERNAME = "MONGODB_USERNAME"
MONGODB_PASSWORD = "MONGODB_PASSWORD"
MONGODB_AUTHENTICATION_SOURCE = "MONGODB_AUTHENTICATION_SOURCE"


class BaseConfig:
    """
    Base configuration - allow specific configurations to override settings
    """

    DEBUG = False
    TESTING = False
    SECRET_KEY = "secret!"
    SESSION_COOKIE_SECURE = True

    MONGO_URI = ""


class ProductionConfig(BaseConfig):
    """
    Configuration is the same as in the base object, which should be safe as default
    You at least need to override the secret key for production
    """

    mongodb_hostname = os.environ.get(MONGODB_HOSTNAME, "mongodb")
    mongodb_port = os.environ.get(MONGODB_PORT, "27017")
    mongodb_database = quote_plus(os.environ.get(MONGODB_DB, "palletDB"))
    mongodb_user = quote_plus(os.environ.get(MONGODB_USERNAME, "user"))
    mongodb_password = quote_plus(os.environ.get(MONGODB_PASSWORD, "password"))
    mongodb_authdb = quote_plus(os.environ.get(MONGODB_AUTHENTICATION_SOURCE, "admin"))

    # connection parameters read by mongoengine
    MONGODB_HOST = "mongodb://{}:{}@{}:{}/{}?authSource={}".format(
        mongodb_user,
        mongodb_password,
        mongodb_hostname,
        mongodb_port,
        mongodb_database,
        mongodb_authdb,
    )
    MONGODB_CONNECT = True


class DevelopmentConfig(BaseConfig):
    """
    Production used for local development
    """

    DEBUG = True
    SESSION_COOKIE_SECURE = False

    # connection parameters read by mongoengine
    MONGODB_HOST = "mongodb://user:password@localhost:27017/palletDB?authSource=admin"
    MONGODB_CONNECT = False


class TestConfig(BaseConfig):
    """
    Configuration used for tests
    """

    TESTING = True
    SESSION_COOKIE_SECURE = False

    # connection parameters read by mongoengine
    MONGODB_HOST = "mongomock://localhost"
    MONGODB_CONNECT = False
