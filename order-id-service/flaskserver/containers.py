# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.
# pylint: disable=too-few-public-methods

"""
Containers module used for dependency injection.
"""

from dependency_injector import containers, providers

from flaskserver.services.demonstrator_shipment_data_service import (
    DemonstratorShipmentDataService,
)


class Container(containers.DeclarativeContainer):

    # config = providers.Configuration()

    # Gateways

    # Services

    shipment_data_service = providers.Singleton(DemonstratorShipmentDataService)
