# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from mongoengine import EmbeddedDocument, ImageField, StringField


class PalletFeetModel(EmbeddedDocument):
    """
    Model that represents a single pallet feet
    """

    image = ImageField()
    description = StringField(max_length=50)
