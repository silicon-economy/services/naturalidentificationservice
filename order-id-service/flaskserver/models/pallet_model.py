# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from datetime import datetime
from flask_mongoengine import Document
from mongoengine import StringField, ListField
from mongoengine.fields import DateTimeField, EmbeddedDocumentListField

from flaskserver.models.pallet_scan_model import PalletScanModel


class PalletModel(Document):
    """
    Model that represents a whole pallet
    """

    palletId = StringField(required=True, max_length=200, unique=True)
    scans = EmbeddedDocumentListField(PalletScanModel)
    tags = ListField(StringField(max_length=30))  # e.g. Material
    pallet_type = StringField(max_length=50, default="EPAL pallet")  # e.g. EPAL
    first_ever_scan = DateTimeField(default=datetime.utcnow)
    last_scan = DateTimeField()
    last_event = StringField()
    status = StringField()
    shipment_id = StringField()  # back reference to shipment
