# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from datetime import datetime
from mongoengine import (
    EmbeddedDocument,
    ImageField,
    DateTimeField,
    FloatField,
    StringField,
    EmbeddedDocumentListField,
    BooleanField,
)

from flaskserver.models.pallet_feet_model import PalletFeetModel


class PalletScanModel(EmbeddedDocument):
    """
    Model that represents the results of a pallet scan process
    """

    scanId = StringField(
        max_length=200, required=True
    )  # unique does not work here, need to use sparse index
    timeStamp = DateTimeField(default=datetime.utcnow)
    terminalId = StringField(max_length=200)
    certainty = FloatField(min_value=0.0, max_value=1.0)
    image = ImageField(
        thumbnail_size=(750, 250, True)
    )  # resize thumbnail to be 750x250 fix
    pallet_feet = EmbeddedDocumentListField(PalletFeetModel)  # images of pallet feet
    confirmed = BooleanField(
        required=False, default=None
    )  # save user's confirmation decision
