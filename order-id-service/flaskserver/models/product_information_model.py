# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from mongoengine.fields import FloatField, ListField
from mongoengine import StringField, EmbeddedDocument


class ProductInformationModel(EmbeddedDocument):
    """
    Model that holds some information on products being shipped on a pallet
    """

    description = StringField()
    weight = FloatField()
    country_of_origin = StringField()
    warning_labels = ListField(StringField(max_length=30))  # e.g. battery, explosive
