# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from mongoengine import StringField, EmbeddedDocument


class ShipmentDetailModel(EmbeddedDocument):
    """
    Model that holds some information on shipment specific information
    """

    sscc_nve = StringField(max_length=100)
    sender = StringField()
    recipient = StringField()
    last_destination = StringField()
    next_destination = StringField()
