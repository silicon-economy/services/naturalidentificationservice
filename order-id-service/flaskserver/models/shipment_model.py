# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from flask_mongoengine import Document
from mongoengine.fields import ReferenceField, EmbeddedDocumentField, StringField

from flaskserver.models.pallet_model import PalletModel
from flaskserver.models.shipment_detail_model import ShipmentDetailModel
from flaskserver.models.product_information_model import ProductInformationModel


class ShipmentModel(Document):
    """
    Model that represents a whole shipment
    """

    shipment_id = StringField(unique=True)
    carrier_detail = ReferenceField(PalletModel)
    product_info = EmbeddedDocumentField(ProductInformationModel)
    shipment_detail = EmbeddedDocumentField(ShipmentDetailModel)
