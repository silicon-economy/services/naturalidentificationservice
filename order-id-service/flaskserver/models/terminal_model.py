# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from mongoengine import StringField, GeoPointField, ReferenceField
from flask_mongoengine import Document

from flaskserver.models.pallet_model import PalletModel


class TerminalModel(Document):
    """
    Model that represents a physical scan terminal
    """

    terminalId = StringField(max_length=200, required=True, unique=True)
    terminalType = StringField(max_length=50, required=True)
    position = GeoPointField()
    lastScannedPallet = ReferenceField(PalletModel)
