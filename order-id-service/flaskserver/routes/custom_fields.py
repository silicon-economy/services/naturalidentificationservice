# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Some Custom Flask-RestX Fields to convert MongoDB Documents (Models) to them
"""

from flask.helpers import url_for
from flask_restx import fields

from flaskserver.models.pallet_feet_model import PalletFeetModel
from flaskserver.models.pallet_scan_model import PalletScanModel
from flaskserver.models.pallet_model import PalletModel


def _get_parent_pallet(pallet_scan: PalletScanModel) -> PalletModel:
    if not isinstance(pallet_scan._instance, PalletModel):  # pragma: no cover
        msg = "Unable to marshal image url for scan. PalletScanModel does not have a PalletModel as parent instance"
        raise fields.MarshallingError(msg)
    parent_pallet: PalletModel = pallet_scan._instance
    return parent_pallet


class PalletFeetImageUrl(fields.Raw):
    """
    Custom field to store a URL to a pallet feet image
    """

    __schema_type__ = "string"

    def output(self, key, obj, **kwargs):
        if not isinstance(obj, PalletFeetModel):  # pragma: no cover
            msg = "Unable to marshal image url for scan because obj is not of expected type PalletFeetModel"
            raise fields.MarshallingError(msg)

        if not isinstance(obj._instance, PalletScanModel):  # pragma: no cover
            msg = "Unable to marshal image url for scan because obj instance (parent Document) is not of expected type"
            raise fields.MarshallingError(msg)
        parent_scan: PalletScanModel = obj._instance

        parent_pallet = _get_parent_pallet(parent_scan)

        pallet_feet_idx = parent_scan.pallet_feet.index(obj)
        value = url_for(
            "api.pallets_pallet_scan_pallet_feet_image",
            palletid=parent_pallet.palletId,
            scanid=parent_scan.scanId,
            pallet_feet_img_nr=pallet_feet_idx,
            _external=True,
        )
        return value


class PalletScanImageUrl(fields.Raw):
    """
    Custom field to store a URL to a pallet scan image
    """

    __schema_type__ = "string"

    def output(self, key, obj, **kwargs):
        if not isinstance(obj, PalletScanModel):  # pragma: no cover
            msg = (
                "Unable to marshal image url for scan because obj is not of expected type PalletScanModel: "
                + str(obj)
            )
            raise fields.MarshallingError(msg)

        parent_pallet: PalletModel = _get_parent_pallet(obj)

        value = url_for(
            "api.pallets_pallet_scan_image",
            palletid=parent_pallet.palletId,
            scanid=obj.scanId,
            _external=True,
        )
        return value
