# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from flask_restx import Namespace, fields as apifields


from flaskserver.routes.custom_fields import PalletFeetImageUrl, PalletScanImageUrl

api = Namespace('terminals', description='The terminals.')


terminal_dao = api.model('Terminal', {
    'terminalId': apifields.String(required=True, attribute='terminalId', description='The terminal\'s unique identifier'),
    'terminalType': apifields.String(required=True, attribute='terminalType', description='The terminal type')
})

pallet_feet_dao = api.model('Pallet feet', {
    'image_url': PalletFeetImageUrl(description='URL to the image of this pallet feet at the parent scan'),
    'description': apifields.String(required=False, description='some description of the pallet feet image (i.e. position)')
})

pallet_scan_dao = api.model('Pallet Scan', {
    'scanId': apifields.String(required=True, description='The scan\'s unique identifier'),
    'timeStamp': apifields.DateTime(required=False, description='The timestamp of the scan'),
    'terminalId': apifields.String(required=False, description='The terminal where the scan happend'),
    'certainty': apifields.Float(required=False, description='The certainty of the scan being correct'),
    'image_url': PalletScanImageUrl(required=False, description='URL to the image of the pallet at this scan'),
    'pallet_feet': apifields.List(apifields.Nested(pallet_feet_dao)),
    'confirmed': apifields.Boolean(required=False, description='True/False if a user confirmed the scan is correct/incorrect')
})

pallet_dao = api.model('Pallet', {
    'palletId': apifields.String(required=True, attribute='palletId', description='The pallet\'s unique identifier'),
    'pallet_type': apifields.String(required=False, attribute='pallet_type', description='The pallet type'),
    'status': apifields.String(required=False, attribute='status', description='The pallet status'),
    'last_event': apifields.String(required=False, attribute='last_event', description='The pallet\'s last event'),
    'first_ever_scan': apifields.DateTime(required=False, attribute='first_ever_scan', description='The timestamp of pallet\'s first ever scan'),
    'last_scan': apifields.DateTime(required=False, attribute='last_scan', description='The timestamp of pallet\'s last scan'),
    'scans': apifields.List(apifields.Nested(pallet_scan_dao)),
    'tags': apifields.List(apifields.String)
})

shipment_detail_dao = api.model('Shipment detail', {
    'sscc_nve': apifields.String(),
    'sender': apifields.String(),
    'recipient': apifields.String(),
    'last_destination': apifields.String(),
    'next_destination': apifields.String()
})

product_info_dao = api.model('Product info', {
    'description': apifields.String(),
    'weight': apifields.Float(),
    'country_of_origin': apifields.String(),
    'warning_labels': apifields.List(apifields.String())
})

shipment_dao = api.model('Shipment', {
    'shipment_id': apifields.String(required=True, attribute='shipment_id', description='The shipment\'s unique identifier'),
    'carrier_detail': apifields.Nested(pallet_dao, required=True),
    'product_info': apifields.Nested(product_info_dao, required=True),
    'shipment_detail': apifields.Nested(shipment_detail_dao, required=True)
})
