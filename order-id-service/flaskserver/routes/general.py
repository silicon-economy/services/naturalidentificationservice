# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.


"""
HTTP REST API for general Actions

Implements a action to reset the demonstrator to default data
"""


import mongoengine
from mongoengine.errors import NotUniqueError
from flask_restx import Namespace, Resource, abort

from flaskserver.models.terminal_model import TerminalModel
from flaskserver.models.shipment_model import ShipmentModel
from flaskserver.models.pallet_model import PalletModel

api = Namespace("general", description="General API Actions.")


@api.route("/")
class General(Resource):
    """Implements some general API management actions"""

    @api.doc("reset_db")
    def delete(self):
        """Resets all data entries in the database"""
        PalletModel.objects.delete()
        ShipmentModel.objects.delete()
        TerminalModel.objects.delete()

        return "Ok", 200

    @api.doc("init_demonstrator")
    def post(self):
        """Creates the default structure used in the demonstrator"""
        terminal_arrival = TerminalModel(terminalId="tarrival", terminalType="arrival")
        terminal_departure = TerminalModel(
            terminalId="tdeparture", terminalType="departure"
        )

        try:
            terminal_arrival.save()
            terminal_departure.save()
        except NotUniqueError:
            return abort(400, "non unique terminalId")
        except mongoengine.errors.ValidationError:
            return abort(400, "failed to save object to database: ValidationError")
        return "Created", 201
