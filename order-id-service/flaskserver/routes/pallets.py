# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.


"""
HTTP REST API for pallets management

Implements basic CRUD
"""

from typing import Callable
from io import BytesIO

import mongoengine
from mongoengine.errors import NotUniqueError
from mongoengine.fields import ImageField
from flask_restx import Namespace, Resource, abort
from flask.wrappers import Response
from flask.helpers import send_file

from flaskserver.routes.flask_restx_models import pallet_scan_dao, pallet_dao
from flaskserver.models.pallet_model import PalletModel
from flaskserver.models.pallet_feet_model import PalletFeetModel
from flaskserver.models.pallet_scan_model import PalletScanModel

api = Namespace("pallets", description="The pallets.")


@api.route("/")
class PalletList(Resource):
    """Shows a list of all pallets, and lets you POST to add new pallets"""

    @api.doc("list_pallets")
    @api.marshal_list_with(pallet_dao)
    def get(self):
        """Endpoint for GET all pallets

        Returns:
            200: An json array of pallets
        """
        pallets = PalletModel.objects()
        return list(pallets), 200

    @api.doc("create_pallet")
    @api.expect(pallet_dao, validate=True)
    @api.marshal_with(pallet_dao, code=201)
    def post(self):
        """Endpoint to POST a new pallet

        Returns:
            201: the newly created pallet as json object
            400: the palletId is not unique
        """
        pallet = PalletModel(**api.payload)

        try:
            pallet.save()
        except NotUniqueError:
            return abort(400, "non unique palletId")
        except mongoengine.errors.ValidationError:
            return abort(400, "failed to save object to database: ValidationError")
        return pallet, 201


@api.route("/<string:palletid>")
@api.param("palletid", description="the id of the pallet you are looking for")
class Pallet(Resource):
    """Show a single pallet and lets you update and delete them"""

    @api.doc("get_pallet")
    @api.marshal_with(pallet_dao)
    @api.response(404, "Pallet not found")
    def get(self, palletid: str):
        """Endpoint to GET a single pallet

        Args:
            palletid (str): the id of the pallet you are looking for

        Returns:
            200: the pallet as json
            404: there is no pallet with that id
        """
        pallet = PalletModel.objects.get_or_404(palletId=palletid)
        return pallet

    @api.expect(pallet_dao)
    @api.marshal_with(pallet_dao)
    def put(self, palletid: str):
        """Endpoint to update a single pallet with PUT

        Args:
            palletid (str): the id of the pallet you want to update

        Returns:
            200: the updated pallet as json object
            404: there is no pallet with that id
        """
        pallet = PalletModel.objects.get_or_404(palletId=palletid)

        try:
            pallet.update(**api.payload)
        except NotUniqueError:
            return abort(400, "non unique palletId")
        except mongoengine.errors.ValidationError:
            return abort(400, "failed to save object to database: ValidationError")

        if "palletId" in api.payload:
            palletid = api.payload["palletId"]
        return PalletModel.objects.get_or_404(palletId=palletid)

    @api.doc("delete_pallet")
    @api.response(200, "Pallet deleted")
    def delete(self, palletid: str):
        """Endpoint to DELETE a single pallet

        Args:
            palletid (str): the id of the pallet you want to delete

        Returns:
            200: the id of the deleted pallet
            404: there is no pallet with that id
        """
        pallet = PalletModel.objects.get_or_404(palletId=palletid)
        pallet.delete()
        return str(palletid), 200

    @api.doc("add_pallet_scan")
    @api.expect(pallet_scan_dao, validate=True)
    @api.marshal_with(pallet_scan_dao, code=201)
    def post(self, palletid: str):
        """Endpoint to POST a new scan for a pallet

        Args:
            palletid (str): the id of the pallet you want to add a scan to

        Returns:
            201: the newly created scan as json
            400: the scan id is not unique
        """
        pallet: PalletModel = PalletModel.objects.get_or_404(palletId=palletid)

        body = api.payload

        # TODO: This should be handled by the database
        x = list(filter(lambda scan: scan.scanId == body["scanId"], pallet.scans))
        if any(x):
            return abort(400, "non unique scanid")

        scan = pallet.scans.create(**body)

        try:
            pallet.save()
        except NotUniqueError:
            return abort(400, "non unique palletId")
        except mongoengine.errors.ValidationError:
            return abort(400, "failed to save object to database: ValidationError")
        return scan, 201


@api.route("/<string:palletid>/<string:scanid>")
@api.param("palletid", description="the id of the pallet you are looking for")
@api.param("scanid", description="the id of the scan you are looking for")
class PalletScan(Resource):
    """Show a single pallet scan and lets you update and delete them"""

    @api.doc("get_pallet_scan")
    @api.marshal_with(pallet_scan_dao)
    @api.response(404, "Pallet or scan not found")
    @api.response(500, "Found multiple scans with same id")
    def get(self, palletid: str, scanid: str):
        """Endpoint to GET a single pallet scan

        Args:
            palletid (str): the id of the pallet you are looking for
            scanid (str): the id of the scan you are looking for

        Returns:
            200: the pallet as json
            404: there is no pallet with that id
            404: there is no scan with that id
        """
        pallet: PalletModel = PalletModel.objects.get_or_404(palletId=palletid)
        filterkey: Callable[[PalletScanModel], bool] = lambda x: x.scanId == scanid
        filtered_scans = list(filter(filterkey, pallet.scans))
        if len(filtered_scans) == 1:
            scan: PalletScanModel = filtered_scans[0]
            return scan, 200
        if len(filtered_scans) > 1:
            return abort(500, "Found multiple scans with same id")
        return abort(404)

    @api.doc("update_pallet_scan_confirmed")
    @api.expect(pallet_scan_dao)
    @api.marshal_with(pallet_scan_dao)
    def put(self, palletid: str, scanid: str):
        """Endpoint to update the confirmed status of a scan with PUT

        Args:
            palletid (str): the id of the pallet
            scanid (str): the id of the scan whose confirmed status shall be updated

        Returns:
            200: the updated scan as json object
            404: the palletid was not found
            404: the scanid was not found
            500: there were multiple scans with scanid
            400: there was no new confirmed status provied in the message body
        """
        pallet: PalletModel = PalletModel.objects.get_or_404(palletId=palletid)

        filterkey: Callable[[PalletScanModel], bool] = lambda x: x.scanId == scanid
        filtered_scans = list(filter(filterkey, pallet.scans))
        if len(filtered_scans) > 1:
            return abort(500, "Found multiple scans with same id")
        elif len(filtered_scans) == 0:
            return abort(404)

        body = api.payload
        if not "confirmed" in body:
            return abort(400, "Missing confirmed attribute in post data body")

        scan: PalletScanModel = filtered_scans[0]
        scan.confirmed = body["confirmed"]

        try:
            pallet.save()
        except NotUniqueError:
            return abort(400, "non unique id")
        except mongoengine.errors.ValidationError:
            return abort(400, "failed to save object to database: ValidationError")

        if "scanId" in body:
            scanid = body["scanId"]

        pallet: PalletModel = PalletModel.objects.get_or_404(palletId=palletid)
        filterkey: Callable[[PalletScanModel], bool] = lambda x: x.scanId == scanid
        filtered_scans = list(filter(filterkey, pallet.scans))
        if len(filtered_scans) == 1:
            scan: PalletScanModel = filtered_scans[0]
            return scan, 200
        if len(filtered_scans) > 1:
            return abort(500, "Found multiple scans with same id")
        return abort(404)


def _send_image_response(
    image: ImageField, filename=""
) -> Response:  # pragma: no cover
    """sends an image as file read from an mongodb GridFS ImageField

    Args:
        image (ImageField): the image to send
        filename (str, optional): the filename to send with the image Defaults to image.filename.

    Returns:
        Response: the flask repsonse object containing the image
    """
    filename = filename if filename else image.filename
    return send_file(
        BytesIO(image.read()), attachment_filename=filename, mimetype=image.content_type
    )


@api.route("/<string:palletid>/<string:scanid>/image")
@api.param("palletid", description="the id of the pallet you are looking for")
@api.param("scanid", description="the id of the scan you are looking for")
class PalletScanImage(Resource):
    """Show a single pallet scan image and lets you update and delete them"""

    @api.doc("get_pallet_scan_image")
    @api.response(200, description="jpg image file")
    @api.response(404, "Pallet or scan not found")
    @api.response(500, "Found multiple scans with same id")
    @api.produces(["image/jpg", "application/json", "text/plain"])
    def get(self, palletid: str, scanid: str):
        """Endpoint to GET the image of the last scan

        Args:
            palletid (str): the id of the pallet you are looking for
            scanid (str): the id of the scan you are looking for

        Returns:
            200: the image of the scan
            404: the palletid was not found
            404: the scanid was not found
            500: if multiple scans were found
        """
        pallet: PalletModel = PalletModel.objects.get_or_404(palletId=palletid)
        filterkey: Callable[[PalletScanModel], bool] = lambda x: x.scanId == scanid
        filtered_scans = list(filter(filterkey, pallet.scans))
        if len(filtered_scans) == 1:
            scan: PalletScanModel = filtered_scans[0]
            return _send_image_response(scan.image)
        if len(filtered_scans) > 1:
            return abort(500, "Found multiple scans with same id")
        return abort(404, "Scan not found")


@api.route("/<string:palletid>/<string:scanid>/<int:pallet_feet_img_nr>")
@api.param("palletid", description="the id of the pallet you are looking for")
@api.param("scanid", description="the id of the scan you are looking for")
@api.param(
    "pallet_feet_img_nr", description="the nr of the pallet feet you are looking for"
)
class PalletScanPalletFeetImage(Resource):
    """Show a single pallet scan image and lets you update and delete them"""

    @api.doc("get_pallet_scan_pallet_feet_image")
    @api.response(200, description="jpg image file")
    @api.response(404, "Pallet, scan or number not found")
    @api.response(500, "Found multiple scans with same id")
    @api.produces(["image/jpg"])
    def get(self, palletid: str, scanid: str, pallet_feet_img_nr: int):
        """Endpoint to GET a specific pallet feet image of a pallet scan

        Args:
            palletid (str): the id of the pallet you are looking for
            scanid (str): the id of the scan you are looking for
            pallet_feet_img_nr (int): the index number of the pallet feet image you are looking for
        Returns:
            200: the image of the pallet feet
            404: if the pallet was not found
            404: if the scan was not found
            404: if the scan was pallet feet number was not found
            500: if multiple scans were found
        """
        pallet: PalletModel = PalletModel.objects.get_or_404(palletId=palletid)
        filterkey: Callable[[PalletScanModel], bool] = lambda x: x.scanId == scanid
        filtered_scans = list(filter(filterkey, pallet.scans))
        if len(filtered_scans) == 1:
            scan: PalletScanModel = filtered_scans[0]
            if pallet_feet_img_nr in range(len(scan.pallet_feet)):  # pragma: no cover
                pallet_feet: PalletFeetModel = scan.pallet_feet[pallet_feet_img_nr]
                return _send_image_response(pallet_feet.image)
            else:
                return abort(404, "Pallet feet image nr not found")
        if len(filtered_scans) > 1:
            return abort(500, "Found multiple scans with same id")
        return abort(404, "Scan not found")
