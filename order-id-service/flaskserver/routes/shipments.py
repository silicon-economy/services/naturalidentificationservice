# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.


"""
HTTP REST API for shipments management

Implements basic CRUD
"""

import mongoengine
from mongoengine.errors import NotUniqueError
from flask_restx import Namespace, Resource, abort

from flaskserver.models.shipment_model import ShipmentModel
from flaskserver.models.pallet_model import PalletModel
from flaskserver.routes.flask_restx_models import shipment_dao


api = Namespace("shipments", description="The shipments.")


@api.route("/")
class ShipmentList(Resource):
    """Shows a list of all shipments, and lets you POST to add new shipments"""

    @api.doc("list_shipments")
    @api.marshal_list_with(shipment_dao)
    def get(self):
        """Endpoint for GET all shipments

        Returns:
            200: An json array of shipments
        """
        shipments = ShipmentModel.objects()
        return list(shipments), 200

    @api.doc("create_shipment")
    @api.expect(shipment_dao, validate=True)
    @api.marshal_with(shipment_dao, code=201)
    def post(self):
        """Endpoint to POST a new shipment

        Returns:
            201: the newly created shipment as json object
            400: the shipmentId is not unique
        """
        shipment = ShipmentModel(**api.payload)

        try:
            shipment.save()
        except NotUniqueError:
            return abort(400, "non unique shipment_id")
        except mongoengine.errors.ValidationError:
            return abort(400, "failed to save object to database: ValidationError")
        return shipment, 201


@api.route("/<string:shipment_id>")
@api.param("shipment_id", description="the id of the shipment you are looking for")
class Shipment(Resource):
    """Show a single shipment and lets you update and delete them"""

    @api.doc("get_shipment")
    @api.marshal_with(shipment_dao)
    @api.response(404, "Shipment not found")
    def get(self, shipment_id: str):
        """Endpoint to GET a single shipment

        Args:
            shipment_id (str): the id of the shipment you are looking for

        Returns:
            200: the shipment as json
            404: there is no shipment with that id
        """
        shipment = ShipmentModel.objects.get_or_404(shipment_id=shipment_id)
        return shipment

    @api.expect(shipment_dao, validate=False)
    @api.marshal_with(shipment_dao)
    def put(self, shipment_id: str):
        """Endpoint to update a single shipment with PUT

        Args:
            shipment_id (str): the id of the shipment you want to update

        Returns:
            200: the updated shipment as json object
            404: there is no shipment with that id
        """
        shipment = ShipmentModel.objects.get_or_404(shipment_id=shipment_id)

        try:
            shipment.update(**api.payload)
        except NotUniqueError:
            return abort(400, "non unique shipment_id")
        except mongoengine.errors.ValidationError:
            return abort(400, "failed to save object to database: ValidationError")

        if "shipment_id" in api.payload:
            shipment_id = api.payload["shipment_id"]
        return ShipmentModel.objects.get_or_404(shipment_id=shipment_id)

    @api.doc("delete_shipment")
    @api.response(200, "Shipment deleted")
    def delete(self, shipment_id: str):
        """Endpoint to DELETE a single shipment

        Args:
            shipment_id (str): the id of the shipment you want to delete

        Returns:
            200: the id of the deleted shipment
            404: there is no shipment with that id
        """
        shipment = ShipmentModel.objects.get_or_404(shipment_id=shipment_id)
        shipment.delete()

        linked_pallet: PalletModel = PalletModel.objects(
            shipment_id=shipment_id
        ).first()
        if linked_pallet:
            linked_pallet.shipment_id = ""
            linked_pallet.save()
        return str(shipment_id), 200
