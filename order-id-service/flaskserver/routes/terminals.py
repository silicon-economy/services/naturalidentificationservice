# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.


"""
HTTP REST API for terminal management

Implements basic CRUD
"""

import mongoengine
from mongoengine.errors import NotUniqueError
from flask_restx import Namespace, Resource, abort

from flaskserver.models.terminal_model import TerminalModel
from flaskserver.routes.flask_restx_models import terminal_dao, pallet_dao

api = Namespace("terminals", description="The terminals.")


@api.route("/")
class TerminalList(Resource):
    """Shows a list of all terminals, and lets you POST to add new terminals"""

    @api.doc("list_terminals")
    @api.marshal_list_with(terminal_dao)
    def get(self):
        """Shows a list of all terminals"""
        terminals = TerminalModel.objects()
        return list(terminals), 200

    @api.doc("create_terminal")
    @api.expect(terminal_dao, validate=True)
    @api.marshal_with(terminal_dao, code=201)
    def post(self):
        """Create a new terminal"""
        terminal = TerminalModel(**api.payload)

        try:
            terminal.save()
        except NotUniqueError:
            return abort(400, "non unique terminalId")
        except mongoengine.errors.ValidationError:
            return abort(400, "failed to save object to database: ValidationError")
        return terminal, 201


@api.route("/<string:terminalid>")
@api.param("terminalid", description="the id of the terminal you are looking for")
class Terminal(Resource):
    """Show a single terminal and lets you update and delete them"""

    @api.doc("get_terminal")
    @api.marshal_with(terminal_dao)
    def get(self, terminalid: str):
        """fetch a single terminal"""
        terminal = TerminalModel.objects.get_or_404(terminalId=terminalid)
        return terminal

    @api.expect(terminal_dao)
    @api.marshal_with(terminal_dao)
    def put(self, terminalid: str):
        """update a given terminal"""
        terminal = TerminalModel.objects.get_or_404(terminalId=terminalid)

        try:
            terminal.update(**api.payload)
        except NotUniqueError:
            return abort(400, "non unique terminalId")
        except mongoengine.errors.ValidationError:
            return abort(400, "failed to save object to database: ValidationError")

        if "terminalId" in api.payload:
            terminalid = api.payload["terminalId"]
        return TerminalModel.objects.get_or_404(terminalId=terminalid)

    @api.doc("delete_terminal")
    @api.response(200, "Terminal deleted")
    def delete(self, terminalid: str):
        """Delete a terminal given its identifier"""
        terminal = TerminalModel.objects.get_or_404(terminalId=terminalid)
        terminal.delete()
        return str(terminalid), 200


@api.route("/<string:terminalid>/lastScannedPallet")
@api.param("terminalid", description="the id of the terminal you are looking for")
class TerminalPallet(Resource):
    @api.doc("get_terminal_last_scanned_pallet")
    @api.marshal_with(pallet_dao)
    def get(self, terminalid: str):
        terminal: TerminalModel = TerminalModel.objects.get_or_404(
            terminalId=terminalid
        )
        if not terminal.lastScannedPallet:
            return abort(404)
        return terminal.lastScannedPallet, 200
