# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Starts flask and socketIO server.
Initializes flask and socketIO server.
Initializes loggers.
"""

# Flack-SocketIO docs:
# it is recommended that you apply the monkey patching at the top of your main script,
# even above your imports.
import sys
from gevent import monkey

monkey.patch_all()


import logging
import os

from flask_mongoengine import MongoEngine
from flask import Flask
from flask_cors import CORS
from flask_socketio import SocketIO

from flaskserver.api import api_bp
from flaskserver.socketioapi.terminal import TerminalNamespace
from flaskserver.containers import Container


class Server:
    """ "
    Contains main logic to setup this service
    """

    BACKEND_DEBUG_MODE_PARAM_NAME = "BACKEND_DEBUG_MODE"
    backend_debug = (
        os.getenv(BACKEND_DEBUG_MODE_PARAM_NAME, "False") == "True"
    )  # don't use bool()

    logger = logging.getLogger(__name__)

    def __init__(self):
        Server.init_loggers()

    def start(self, config: str = "production"):
        app = Server.create_app(config)
        self.backend_debug = (
            os.getenv(self.BACKEND_DEBUG_MODE_PARAM_NAME, "False") == "True"
        )  # don't use bool()
        self.logger.info("Starting Python backend")
        sio = Server.add_socketio(app)

        self.run(app, sio)

    def run(self, app: Flask, sio: SocketIO):
        self.logger.debug("Running socketio in debug mode: %s", str(self.backend_debug))
        self.logger.info("Running socketio on port 5000 and all interfaces 0.0.0.0")
        sio.run(app, debug=self.backend_debug, port=5000, host="0.0.0.0")

    @staticmethod
    def create_app(config: str) -> Flask:
        """
        Creates flask app.
        Registers blue print for api.
        Removes CORS filter
        :return: the flask app
        """
        flask_app = Flask(__name__)

        if flask_app.config["ENV"] == "development" or config == "development":
            flask_app.config.from_object("flaskserver.config.DevelopmentConfig")
        elif config == "test":
            flask_app.config.from_object("flaskserver.config.TestConfig")
        elif config == "production":
            flask_app.config.from_object("flaskserver.config.ProductionConfig")

        CORS(flask_app)
        Server.register_blueprints(flask_app)
        Server.configure_extensions(flask_app)
        return flask_app

    @staticmethod
    def register_blueprints(flask_app: Flask):
        """
        Register all blueprints used in the python-backend
        """
        flask_app.register_blueprint(api_bp)

    @staticmethod
    def configure_extensions(flask_app: Flask):
        """
        Configure all extensions like MongoDB.
        """
        MongoEngine(flask_app)

    @staticmethod
    def add_socketio(flask_app: Flask) -> SocketIO:
        """
        Adds Socketio to the flask sever.

        :param flask_app:
        :return: socketIO server to send and receive socketio messages.
        """
        socketio = SocketIO(
            flask_app,
            cors_allowed_origins="*",
            engineio_logger=False,
            logger=False,
            async_mode="gevent",
        )
        Server.register_socketio_namespaces(socketio)
        return socketio

    @staticmethod
    def register_socketio_namespaces(socketio: SocketIO):
        """
        Register all socketio namespaces used in the python-backend
        """
        socketio.on_namespace(
            TerminalNamespace(namespace="/")
        )  # todo: register as root namespace for now

    @staticmethod
    def init_loggers():
        """
        Initializes loggers of used libraries/frameworks.
        """
        logging.getLogger("socketio").setLevel(logging.ERROR)
        logging.getLogger("gevent").setLevel(logging.ERROR)
        logging.getLogger("engineio").setLevel(logging.ERROR)
        logging.getLogger("geventwebsocket").setLevel(logging.ERROR)
        logging.basicConfig(level=logging.INFO)


if __name__ == "__main__":
    container = Container()
    container.init_resources()
    container.wire(modules=[sys.modules[__name__]])

    Server().start()
