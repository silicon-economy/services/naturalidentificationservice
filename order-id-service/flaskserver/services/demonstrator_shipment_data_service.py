# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import random
from typing import List
from flaskserver.models.product_information_model import ProductInformationModel
from flaskserver.models.shipment_detail_model import ShipmentDetailModel
from flaskserver.models.shipment_model import ShipmentModel


class DemonstratorShipmentDataService:
    """
    Service to generate demo shipment data for use with the demonstrator

    Implements random selection of products to use in shipments
    """

    unassigned_shipments: List[ShipmentModel]

    def __init__(self) -> None:
        self.reset()

    def reset(self):
        self.unassigned_shipments = DemonstratorShipmentDataService.generate_mock_data()

    def get_unassigned_random_shipment(self):
        """Randomly chooses a unassigned shipment
        and removes it from the list of unassigned shipments

        Raises:
            ValueError: Thrown if the list of unassigned shipments is empty

        Returns:
            ShipmentModel: the randomly selected shipment
        """
        if len(self.unassigned_shipments) == 0:
            raise ValueError("no unassigned shipments to choose from")
        return self.unassigned_shipments.pop(
            random.randrange(len(self.unassigned_shipments))
        )

    @staticmethod
    def generate_mock_data():
        """Generates a list of shipments to use for demonstration purpose

        Returns:
            List[ShipmentModel]: The list of shipments
        """
        prodinfo1 = ProductInformationModel(
            description="Red apples", weight=12, country_of_origin="Deutschland"
        )
        shipmentdtl1 = ShipmentDetailModel(
            sscc_nve="589185173254393926346",
            sender="EDEKA Logistics Nord, Warenstraße 1, 20354 Hamburg",
            recipient="EDEKA Dortmund Süd, Märkische Str. 82a, 44141 Dortmund",
            last_destination="EDEKA Distributionszentrum Nord",
            next_destination="EDEKA Dortmund Süd",
        )
        shipment1 = ShipmentModel(
            shipment_id="1", product_info=prodinfo1, shipment_detail=shipmentdtl1
        )

        prodinfo2 = ProductInformationModel(
            description="Green apples", weight=7, country_of_origin="Deutschland"
        )
        shipmentdtl2 = ShipmentDetailModel(
            sscc_nve="314159265358979323846",
            sender="EDEKA Logistics Nord, Warenstraße 1, 20354 Hamburg",
            recipient="EDEKA Düsseldorf Mitte, Berliner Allee 52, 40212 Düsseldorf",
            last_destination="EDEKA Distributionszentrum Nord",
            next_destination="EDEKA Düsseldorf Mitte",
        )
        shipment2 = ShipmentModel(
            shipment_id="2", product_info=prodinfo2, shipment_detail=shipmentdtl2
        )

        prodinfo3 = ProductInformationModel(
            description="Bananas", weight=22.5, country_of_origin="Costa Rica"
        )
        shipmentdtl3 = ShipmentDetailModel(
            sscc_nve="173205080756887729352",
            sender="ALDI SÜD Dienstleistungs-GmbH & Co. oHG Burgstraße 37 45476 Mülheim an der Ruhr",
            recipient="ALDI SE & Co. KG St. Augustin, Im Mittelfeld 11, 53757 St. Augustin",
            last_destination="Umschlaglager Dortmund",
            next_destination="Umschlaglager Bonn",
        )
        shipment3 = ShipmentModel(
            shipment_id="3", product_info=prodinfo3, shipment_detail=shipmentdtl3
        )

        prodinfo4 = ProductInformationModel(
            description="Mandarins", weight=22.5, country_of_origin="Spanien"
        )
        shipmentdtl4 = ShipmentDetailModel(
            sscc_nve="143926335893285179546",
            sender="Wal-Mart Stores, Inc., 702 SW 8th St, Bentonville, AR 72712, United States",
            recipient="Walmart Supercenter, 2100 88th St, North Bergen, NJ 07047, United States",
            last_destination="Umschlaglager Memphis",
            next_destination="Umschlaglager New York",
        )
        shipment4 = ShipmentModel(
            shipment_id="4", product_info=prodinfo4, shipment_detail=shipmentdtl4
        )

        prodinfo5 = ProductInformationModel(
            description="Oranges", weight=80, country_of_origin="Italien"
        )
        shipmentdtl5 = ShipmentDetailModel(
            sscc_nve="935891439263851732546",
            sender="ALDI SÜD Dienstleistungs-GmbH & Co. oHG Burgstraße 37 45476 Mülheim an der Ruhr",
            recipient="ALDI SÜD, Sophienstraße 21, 70178 Stuttgart",
            last_destination="Umschlaglager Mühlheim an der Ruhr",
            next_destination="Umschlaglager Stuttgart",
        )
        shipment5 = ShipmentModel(
            shipment_id="5", product_info=prodinfo5, shipment_detail=shipmentdtl5
        )

        prodinfo6 = ProductInformationModel(
            description="Pears", weight=13, country_of_origin="Frankreich"
        )
        shipmentdtl6 = ShipmentDetailModel(
            sscc_nve="535897931413238592646",
            sender="Lidl Distributionszentrum, 30625 Hannover",
            recipient="Berckhusenstraße 95, 30625 Hannover",
            last_destination="Umschlaglager Hannover Nord",
            next_destination="Umschlaglager Hannover Süd",
        )
        shipment6 = ShipmentModel(
            shipment_id="6", product_info=prodinfo6, shipment_detail=shipmentdtl6
        )

        prodinfo7 = ProductInformationModel(
            description="Lemons", weight=42, country_of_origin="Italien"
        )
        shipmentdtl7 = ShipmentDetailModel(
            sscc_nve="335358914173292638546",
            sender="Lidl Distributionszentrum, 30625 Hannover",
            recipient="Lidl-Filiale, Essener Str. 71, 45141 Stoppenberg",
            last_destination="Umschlaglager Hannover",
            next_destination="Lidl-Filiale",
        )
        shipment7 = ShipmentModel(
            shipment_id="7", product_info=prodinfo7, shipment_detail=shipmentdtl7
        )


        prodinfo8 = ProductInformationModel(
            description="Tomatoes", weight=31, country_of_origin="Spanien"
        )
        shipmentdtl8 = ShipmentDetailModel(
            sscc_nve="335358914173292631768",
            sender="Lidl Distributionszentrum, 30625 Hannover",
            recipient="Lidl-Filiale, Essener Str. 71, 45141 Stoppenberg",
            last_destination="Umschlaglager Hannover",
            next_destination="Lidl-Filiale",
        )
        shipment8 = ShipmentModel(
            shipment_id="8", product_info=prodinfo8, shipment_detail=shipmentdtl8
        )


        prodinfo9 = ProductInformationModel(
            description="Blueberries", weight=40, country_of_origin="Norwegen"
        )
        shipmentdtl9 = ShipmentDetailModel(
            sscc_nve="33235894784092638546",
            sender="ALDI SÜD Dienstleistungs-GmbH & Co. oHG Burgstraße 37 45476 Mülheim an der Ruhr",
            recipient="ALDI SÜD, Sophienstraße 21, 70178 Stuttgart",
            last_destination="Umschlaglager Mühlheim an der Ruhr",
            next_destination="Umschlaglager Stuttgart",
        )
        shipment9 = ShipmentModel(
            shipment_id="9", product_info=prodinfo9, shipment_detail=shipmentdtl9
        )


        prodinfo10 = ProductInformationModel(
            description="Avocado", weight=34, country_of_origin="Marokko"
        )
        shipmentdtl10 = ShipmentDetailModel(
            sscc_nve="143926001235285176490",
            sender="Wal-Mart Stores, Inc., 702 SW 8th St, Bentonville, AR 72712, United States",
            recipient="Walmart Supercenter, 2100 88th St, North Bergen, NJ 07047, United States",
            last_destination="Umschlaglager Memphis",
            next_destination="Umschlaglager New York",
        )
        shipment10 = ShipmentModel(
            shipment_id="10", product_info=prodinfo10, shipment_detail=shipmentdtl10
        )

        prodinfo11 = ProductInformationModel(
            description="Peach", weight=27, country_of_origin="Griechenland"
        )
        shipmentdtl11 = ShipmentDetailModel(
            sscc_nve="241585173254393921664",
            sender="EDEKA Logistics Nord, Warenstraße 1, 20354 Hamburg",
            recipient="EDEKA Dortmund Süd, Märkische Str. 82a, 44141 Dortmund",
            last_destination="EDEKA Distributionszentrum Nord",
            next_destination="EDEKA Dortmund Süd",
        )
        shipment11 = ShipmentModel(
            shipment_id="11", product_info=prodinfo11, shipment_detail=shipmentdtl11
        )

        shipments = [
            shipment1,
            shipment2,
            shipment3,
            shipment4,
            shipment5,
            shipment6,
            shipment7,
            shipment8,
            shipment9,
            shipment10,
            shipment11
        ]
        return shipments
