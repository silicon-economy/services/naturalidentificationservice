# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
This class handles the SocketIO connections from the terminals
"""
from flaskserver.models.shipment_model import ShipmentModel
from typing import List
import uuid
import logging
import os
import base64

import cv2
import numpy as np
from flask_socketio import Namespace, emit
from dependency_injector.wiring import inject, Provide

from flaskserver.containers import Container
from flaskserver.models.terminal_model import TerminalModel
from flaskserver.models.pallet_scan_model import PalletScanModel
from flaskserver.models.pallet_model import PalletModel
from flaskserver.models.pallet_feet_model import PalletFeetModel
from flaskserver.services.demonstrator_shipment_data_service import DemonstratorShipmentDataService

class TerminalNamespace(Namespace):

    logger = logging.getLogger(__name__)

    @inject
    def __init__(self, namespace=None, shipment_data_service: DemonstratorShipmentDataService = Provide[Container.shipment_data_service]):
        super().__init__(namespace=namespace)
        self.connections = 0
        self.shipment_data_service = shipment_data_service

    def on_connect(self):
        self.connections += 1
        self.logger.info('Client connected. Connection count %d', self.connections)
        emit('message', {'data': 'Connected', 'count': self.connections}, broadcast=True)

    def on_disconnect(self):
        self.connections -= 1
        self.logger.info('Client disconnected. Connection count %d', self.connections)

    def on_cam_arrival(self, data):
        if not ('terminalId' in data and isinstance(data['terminalId'], str)):
            self.logger.warning('received invalid terminal cam data: %s', 'missing terminalId')
            return
        terminalid = data['terminalId']
        self.logger.debug('[%s] cam from termial received %d bytes', terminalid, len(data['img64']))
        msg = {'message': 'Cam arrival update', 'img64': data['img64']}
        emit(f'cam_arrival_update/{terminalid}', msg, broadcast=True)

    def on_status_arrival(self, data):
        if not ('terminalId' in data and isinstance(data['terminalId'], str)):
            self.logger.warning('received invalid terminal status data: %s', 'missing terminalId')
            return
        terminalid = data['terminalId']
        self.logger.debug('[%s] status received: %s', terminalid, data['message'])
        msg = {'message': 'arrival status update', 'status': data['status']}
        emit(f'status_arrival_update/{terminalid}', msg, broadcast=True)

    # example JSON to test at https://amritb.github.io/socketio-client-tool/
    # {
    #   "scanId": "0d443d9b-e984-46d2-94e6-61d0f862e93d",
    #   "terminalId": "t590",
    #   "palletId": "p123",
    #   "img64": "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=",
    #   "pallet_feet": [ 
    #       {"img64": "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=", "description": "left"},
    #       {"img64": "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=", "description": "center"},
    #       {"img64": "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=", "description": "right"},
    #   ]
    # }
    def on_scan(self, data):
        if not (data and isinstance(data, dict)):
            self.logger.warning('received invalid terminal scan message: %s', 'expected json like dictionary')
            return
        elif not ('scanId' in data and isinstance(data['scanId'], str)):
            self.logger.warning('received invalid terminal scan message: %s', 'missing scanId')
            return
        elif not ('terminalId' in data and isinstance(data['terminalId'], str)):
            self.logger.warning('received invalid terminal scan message: %s', 'missing terminalId')
            return
        elif not ('palletId' in data and isinstance(data['palletId'], str)):
            self.logger.warning('received invalid terminal scan message: %s', 'missing palletId')
            return
        elif not ('img64' in data and isinstance(data['img64'], str)):
            self.logger.warning('received invalid terminal scan message: %s', 'expected base64 image img64')
            return
        elif ('pallet_feet' in data):
            if not isinstance(data['pallet_feet'], list):
                self.logger.warning('received invalid terminal scan message: %s', 'expected pallet_feet to be a list')
                return
            if any('img64' not in pf for pf in data['pallet_feet']):
                self.logger.warning('received invalid terminal scan message: %s', 'expected all entries in pallet_feet to have an image specified as img64')
                return

        self.logger.info('processing terminal scan message with scanId \"%s\"', data['scanId'])

        filenames = []
        try:
            terminal = TerminalModel.objects(terminalId=data['terminalId']).first()
            if not isinstance(terminal, TerminalModel):
                self.logger.warning('could not find terminalId in db')
                return

            pallet = PalletModel.objects(palletId=data['palletId']).first()
            if not isinstance(pallet, PalletModel):
                # Pallet does not exist in DB -> create new Pallet Model
                pallet = PalletModel(palletId=data['palletId'])

            scan = PalletScanModel()
            scan.scanId = data['scanId']
            if 'certainty' in data:
                scan.certainty = float(data['certainty'])

            filenames = [TerminalNamespace.save_image(scan, data['img64'])]

            if 'pallet_feet' in data:
                for pf in data['pallet_feet']:
                    pallet_feet = PalletFeetModel(description = pf['description'])
                    filenames.append(TerminalNamespace.save_pallet_feet_image(pallet_feet, img64=pf['img64']))
                    scan.pallet_feet.append(pallet_feet)

            scan.terminalId = terminal.terminalId
            
            pallet.last_scan = scan.timeStamp
            pallet.last_event = f"{terminal.terminalType} ({terminal.terminalId})"

            shipment = None
            if terminal.terminalType == 'departure':
                if pallet.shipment_id and pallet.shipment_id != '':
                    raise PalletAlreadyAssignedException(f"Pallet already assigned to shipment {pallet.shipment_id}")

                pallet.status = 'Transport'
                shipment = self.shipment_data_service.get_unassigned_random_shipment()
                shipment.carrier_detail = pallet
                pallet.shipment_id = shipment.shipment_id

            elif terminal.terminalType == 'arrival':
                shipment = ShipmentModel.objects(shipment_id=pallet.shipment_id).first()
                pallet.status = 'In warehouse'
            else:
                pallet.status = ''

            pallet.scans.append(scan)
            pallet.save()

            terminal.lastScannedPallet = pallet
            terminal.save()

            if shipment:
                shipment.save()
                self.logger.info('Scan %s at %s processed', scan.scanId, scan.timeStamp)
                msg = {
                    'message': 'the scan was sucessfully processed', 
                    'status': 'DONE', 
                    'terminalId': terminal.terminalId, 
                    'palletId': pallet.palletId, 
                    'scanId': scan.scanId, 
                    'shipmentId': shipment.shipment_id
                }
                emit(f'scan_status_update/{terminal.terminalId}', msg, broadcast=True)
            else:
                self.logger.error('Failed to process scan %s at %s because the shipment was not found', scan.scanId, scan.timeStamp)
                msg = {
                    'message': 'pallet shipment not found', 
                    'status': 'ERROR', 
                    'terminalId': terminal.terminalId, 
                    'palletId': pallet.palletId, 
                    'scanId': scan.scanId, 
                    'shipmentId': ''
                }
                emit(f'scan_status_update/{terminal.terminalId}', msg, broadcast=True)
        except PalletAlreadyAssignedException:
            self.logger.error('Failed to process scan %s at %s because the pallet is already assigned to a shipment', scan.scanId, scan.timeStamp)
            msg = {
                'message': 'pallet is already assigned to a shipment', 
                'status': 'ERROR', 
                'terminalId': terminal.terminalId, 
                'palletId': pallet.palletId, 
                'scanId': scan.scanId, 
                'shipmentId': ''
            }
            emit(f'scan_status_update/{terminal.terminalId}', msg, broadcast=True)
        except Exception:
            self.logger.exception('Failed to process scan %s at %s', scan.scanId, scan.timeStamp)
            msg = {
                'message': 'an error occured while processing the scan', 
                'status': 'ERROR', 
                'terminalId': terminal.terminalId, 
                'palletId': pallet.palletId, 
                'scanId': scan.scanId, 
                'shipmentId': ''
            }
            emit(f'scan_status_update/{terminal.terminalId}', msg, broadcast=True)
        finally:
            # delete temporary image files
            if filenames:
                TerminalNamespace.delete_files(filenames)

    @classmethod
    def delete_files(cls, filenames: List[str]):
        try:
            for f in filenames:
                os.remove(f)
        except IOError:
            cls.logger.warning('failed to delete file %s', f)


    @classmethod
    def save_image(cls, scan: PalletScanModel, img64: str) -> str:
        """
        implement this as classmethod for easy mocking in tests
        Mocking the GridFSProxy used by the image field is hard otherwise
        """
        filename = ('scan-' + scan.scanId + '--' + str(scan.timeStamp) + '.jpg').replace(' ', '_')
        cv2.imwrite(filename, TerminalNamespace.img64_to_cv2(img64))

        with open(filename, 'rb') as tmp_img_file:
            scan.image.put(tmp_img_file, filename=filename)

        return filename


    @classmethod
    def save_pallet_feet_image(cls, pallet_feet: PalletFeetModel, img64: str) -> str:
        """
        implement this as classmethod for easy mocking in tests
        Mocking the GridFSProxy used by the image field is hard otherwise
        """
        filename = ('scan-pallet-feet-' + str(uuid.uuid4()) + '.jpg').replace(' ', '_')
        cv2.imwrite(filename, TerminalNamespace.img64_to_cv2(img64))

        with open(filename, 'rb') as tmp_img_file:
            pallet_feet.image.put(tmp_img_file, filename=filename)
        
        return filename


    @staticmethod
    def img64_to_cv2(img64: str):
        im_bytes = base64.b64decode(img64)
        im_arr = np.frombuffer(im_bytes, dtype=np.uint8)  # im_arr is one-dim Numpy array
        img = cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR)
        return img


class PalletAlreadyAssignedException(Exception):
    pass
