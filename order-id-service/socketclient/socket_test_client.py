# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import os
import random
import socketio
import time
import cv2
import pybase64 as base64
from enum import Enum
import threading
import signal
import uuid

terminalId = 'tarrival'
sio = socketio.Client(logger=True, engineio_logger=True)

@sio.event
def connect():
    print('connection established')


@sio.event
def my_message(data):
    print('message received with ', data)
    sio.emit('my response', {'response': 'my response'})


@sio.event
def connect_error():
    print("The connection failed!")


@sio.event
def disconnect():
    print('disconnected from server')


sio.connect('http://localhost:5000')


def encode2base64(frame):
    ret, buffer = cv2.imencode('.jpg', frame)
    # ret = False
    if ret == True:
        return base64.b64encode(buffer)
    else:
        return None


class ArrivalStatus(Enum):
    SCAN = 'SCANNING'
    SIGNATURE = 'SIGNATURE'
    SENDING = 'SENDING'
    DONE = 'DONE'

def send_video_frame(frame):
    scale_percent = 50
    # calculate the 50 percent of original dimensions
    # width = int(frame.shape[1] * scale_percent / 100)
    # height = int(frame.shape[0] * scale_percent / 100)
    # dsize = (width, height)
    # resize image
    # output = cv2.resize(frame, dsize)
    base64str = encode2base64(frame)
    sio.emit("cam_arrival", {'terminalId': terminalId, 'message': 'Hello, World!', 'img64': base64str})
    cv2.imshow('frame', frame)

frame_counter = 0

exit_event = threading.Event()
def webcam_stream():
    stream_fps = 5
    next_call = time.time()
    vid = cv2.VideoCapture('palette_demonstrator_long_text.mp4')
    while not exit_event.is_set():
        loop_start = time.time()
        ret, frame = vid.read()
        if not ret:
            break
        
        global frame_counter
        frame_counter += 1

        if(time.time() > next_call) or frame_counter in range(165, 175):
            send_video_frame(frame)
            next_call = time.time()+(1/stream_fps)
        
        frame_delay = max(int(1000/25 - (time.time() - loop_start)), 0)
        wk = cv2.waitKey(frame_delay)
        if wk & 0xFF == ord('q'):
            break
    vid.release()
    cv2.destroyAllWindows()

webcamThread = None
def start_webcam_stream():
    webcamThread = threading.Thread(target=webcam_stream)
    webcamThread.daemon = True
    webcamThread.start()

def signal_handler(signum, frame):
    print("Setting exit event")
    exit_event.set()

def send_status_updates():
    wait = 2
    global frame_counter
    sio.emit("status_arrival", {'terminalId': terminalId, 'message': 'Status update 1', 'status': ArrivalStatus.SCAN.value})
    while frame_counter < 178:
        time.sleep(0.1)
    sio.emit("status_arrival", {'terminalId': terminalId, 'message': 'Status update 2', 'status': ArrivalStatus.SIGNATURE.value})
    time.sleep(wait)
    sio.emit("status_arrival", {'terminalId': terminalId, 'message': 'Status update 3', 'status': ArrivalStatus.SENDING.value})
    time.sleep(wait)

    
    sio.emit("scan", get_scan_data())
    time.sleep(wait)
    sio.emit("status_arrival", {'terminalId': terminalId, 'message': 'Status update 4', 'status': ArrivalStatus.DONE.value})
    time.sleep(wait)
    # sio.emit("status_arrival", {'terminalId': terminalId, 'message': 'Status update 1', 'status': ArrivalStatus.SCAN.value})
    # time.sleep(wait)


def get_scan_data():
    pallet_feet = []
    for img_name in os.listdir('pallet_feet_images'):
        img_path = os.path.join('pallet_feet_images', img_name)
        with open(img_path, 'rb') as img_file:
            b64_string = base64.b64encode(img_file.read())
            pallet_feet.append({'img64': b64_string, 'description': img_name})

    scan_data = {
        'scanId': str(uuid.uuid4()),
        'terminalId': terminalId,
        'palletId': 'p1008',
        'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
        'certainty': random.random(),
        'pallet_feet': pallet_feet
    }

    return scan_data


signal.signal(signal.SIGINT, signal_handler)

start_webcam_stream()
send_status_updates()

# Termination
exit_event.set()
sio.disconnect()
if webcamThread != None:
    webcamThread.join()
