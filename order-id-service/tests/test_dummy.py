# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import unittest


class TestDummy(unittest.TestCase):

    """an ever succeeding test"""

    def test_dummy(self):
        """Success!"""
        assert True
