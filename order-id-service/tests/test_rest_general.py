# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Tests for the /general REST Endpoints
"""

import unittest
from typing import List
from mongoengine import disconnect

from flaskserver.server import Server
from flaskserver.models.pallet_model import PalletModel
from flaskserver.models.shipment_model import ShipmentModel
from flaskserver.models.terminal_model import TerminalModel


class TestRestGeneral(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        disconnect()

    def setUp(self):
        self.server = Server()

        # connects to mongomock database configured in test config (i.e. 'mongomock://localhost')
        self.app = self.server.create_app(config="test")
        self.app.config["TESTING"] = True
        self.client = self.app.test_client()

    def tearDown(self) -> None:
        disconnect()

    def test_reset_db(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        p2 = PalletModel(palletId="ptest2")
        p1.save()
        p2.save()

        t1 = TerminalModel(terminalId="t1", terminalType="departure")
        t1.save()

        shpmt1 = ShipmentModel(shipment_id="shp1")
        shpmt2 = ShipmentModel(shipment_id="shp2")
        shpmt1.save()
        shpmt2.save()

        # act
        response = self.client.delete("general/")

        # assert
        self.assertEqual(200, response.status_code)
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 0)
        terminals_in_db = TerminalModel.objects
        self.assertEqual(len(terminals_in_db), 0)
        shipments_in_db = ShipmentModel.objects
        self.assertEqual(len(shipments_in_db), 0)

    def test_init_demonstrator(self):
        # prepare

        # act
        response = self.client.post("general/")

        # assert
        self.assertEqual(201, response.status_code)

        terminals_in_db = TerminalModel.objects
        self.assertEqual(len(terminals_in_db), 2)

        terminals: List[TerminalModel] = list(
            sorted(terminals_in_db, key=lambda t: t.terminalId)
        )
        self.assertEqual(terminals[0].terminalId, "tarrival")
        self.assertEqual(terminals[0].terminalType, "arrival")

        self.assertEqual(terminals[1].terminalId, "tdeparture")
        self.assertEqual(terminals[1].terminalType, "departure")


if __name__ == "__main__":
    unittest.main()
