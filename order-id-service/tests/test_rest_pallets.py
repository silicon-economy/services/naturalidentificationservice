# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Tests for the REST Pallets API
"""

import json
from datetime import datetime
import dateutil.parser
import unittest
from typing import List
from mongoengine import disconnect

from flaskserver.models.pallet_scan_model import PalletScanModel
from flaskserver.server import Server
from flaskserver.models.pallet_model import PalletModel


class TestRestPallets(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        disconnect()

    def setUp(self):
        self.server = Server()

        # connects to mongomock database configured in test config (i.e. 'mongomock://localhost')
        self.app = self.server.create_app(config="test")
        self.app.config["TESTING"] = True
        self.client = self.app.test_client()

    def tearDown(self) -> None:
        disconnect()

    def test_pallets(self):
        # prepare
        p = PalletModel(palletId="ptest123")
        p.save()

        # act
        response = self.client.get("pallets/")

        # assert
        self.assertEqual(200, response.status_code)
        responseobj = response.json
        self.assertIsNotNone(responseobj)
        self.assertEqual(p.palletId, responseobj[0]["palletId"])

    def test_get_one_pallet(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        p2 = PalletModel(palletId="ptest2", last_scan=datetime(2021, 6, 22, 8, 32, 25))
        p1.save()
        p2.save()

        # act
        response = self.client.get("pallets/ptest2")

        # assert
        self.assertEqual(200, response.status_code)
        responseobj = response.json
        self.assertIsNotNone(responseobj)
        self.assertEqual(p2.palletId, responseobj["palletId"])
        last_scan = dateutil.parser.isoparse(responseobj["last_scan"])
        self.assertEqual(datetime(2021, 6, 22, 8, 32, 25), last_scan)

    def test_add_pallet(self):
        # prepare
        data = {"palletId": "ptest"}

        # act
        response = self.client.post(
            "pallets/", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(201, response.status_code)
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 1)
        self.assertEqual("ptest", pallets_in_db[0].palletId)
        self.assertEqual([], pallets_in_db[0].tags)
        self.assertEqual([], pallets_in_db[0].scans)

    def test_add_pallet_duplicate_id(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        p1.save()
        data = {"palletId": "ptest1"}

        # act
        response = self.client.post(
            "pallets/", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(400, response.status_code)
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 1)
        self.assertEqual("ptest1", pallets_in_db[0].palletId)

    def test_update_pallet(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        p1.save()

        new_tags = ["tag1", "tag2"]
        data = {
            "palletId": "ptest1",
            "tags": new_tags,
            "last_scan": "2021-06-22T11:30:21+02:00",
        }

        # act
        response = self.client.put(
            "pallets/ptest1", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(200, response.status_code)
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 1)
        pallet: PalletModel = pallets_in_db[0]
        self.assertEqual("ptest1", pallet.palletId)
        self.assertEqual(new_tags, pallet.tags)
        self.assertEqual(
            datetime(2021, 6, 22, 9, 30, 21), pallet.last_scan
        )  # Timezone saved with is +02:00

    def test_update_pallet_changes_palletid(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        p1.save()

        new_tags = ["tag1", "tag2"]
        data = {"palletId": "ptest2", "tags": new_tags}

        # act
        response = self.client.put(
            "pallets/ptest1", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(200, response.status_code)
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 1)
        self.assertEqual("ptest2", pallets_in_db[0].palletId)
        self.assertEqual(new_tags, pallets_in_db[0].tags)

    def test_update_pallet_does_not_change_palletid(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        p1.save()

        new_tags = ["tag1", "tag2"]
        data = {"tags": new_tags}

        # act
        response = self.client.put(
            "pallets/ptest1", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(200, response.status_code)
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 1)
        self.assertEqual("ptest1", pallets_in_db[0].palletId)
        self.assertEqual(new_tags, pallets_in_db[0].tags)

    def test_delete_pallet(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        p2 = PalletModel(palletId="ptest2")
        p1.save()
        p2.save()

        # act
        response = self.client.delete("pallets/ptest1")

        # assert
        self.assertEqual(200, response.status_code)
        self.assertIn(p1.palletId, str(response.data))
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 1)
        self.assertEqual("ptest2", pallets_in_db[0].palletId)

    def test_get_pallet_scan(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.save()

        # act
        response = self.client.get("pallets/ptest1/scan1")

        # assert
        self.assertEqual(200, response.status_code)
        responseobj = response.json
        self.assertIsNotNone(responseobj)
        self.assertEqual(s1.scanId, responseobj["scanId"])
        self.assertEqual(s1.terminalId, responseobj["terminalId"])

    def test_get_pallet_scan_not_found(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.save()

        # act
        response = self.client.get("pallets/ptest1/scan2")

        # assert
        self.assertEqual(404, response.status_code)

    def test_get_pallet_multiple_scans_found(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        s1_1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.scans.append(s1_1)
        p1.save()

        # act
        response = self.client.get("pallets/ptest1/scan1")

        # assert
        self.assertEqual(500, response.status_code)

    def test_add_pallet_scan(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.save()

        data = {"scanId": "scan2", "terminalId": "terminal1"}

        # act
        response = self.client.post(
            "pallets/ptest1", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(201, response.status_code)
        p1.reload()

        self.assertEqual(2, len(p1.scans))
        scans: List[PalletScanModel] = list(sorted(p1.scans, key=lambda s: s.timeStamp))
        self.assertEqual("scan1", scans[0].scanId)
        self.assertEqual("scan2", scans[1].scanId)

    def test_add_pallet_scan_duplicate_id(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.save()

        data = {"scanId": "scan1", "terminalId": "terminal1", "pallet_feet": []}

        # act
        response = self.client.post(
            "pallets/ptest1", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(400, response.status_code)
        p1.reload()

        self.assertEqual(1, len(p1.scans))
        scans: List[PalletScanModel] = list(sorted(p1.scans, key=lambda s: s.timeStamp))
        self.assertEqual("scan1", scans[0].scanId)

    def test_update_pallet_scan(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        p1.scans.create(scanId="scan1", terminalId="terminal1")
        p1.scans.create(scanId="scan3", terminalId="terminal1")
        p1.save()

        p2 = PalletModel(palletId="ptest2")
        p2.scans.create(scanId="scan2", terminalId="terminal1")
        p2.save()

        update_data = {"confirmed": True}

        # act
        response = self.client.put(
            "pallets/ptest1/scan1",
            data=json.dumps(update_data),
            content_type="application/json",
        )

        # assert
        self.assertEqual(200, response.status_code)
        pallets_in_db = PalletModel.objects
        self.assertEqual(2, len(pallets_in_db))
        self.assertEqual("ptest1", pallets_in_db[0].palletId)
        pallet: PalletModel = pallets_in_db[0]
        self.assertEqual(2, len(pallet.scans))
        scan1: PalletScanModel = pallet.scans[0]
        self.assertEqual("scan1", scan1.scanId)
        self.assertEqual(True, scan1.confirmed)

        scan3: PalletScanModel = pallet.scans[1]
        self.assertEqual("scan3", scan3.scanId)
        self.assertEqual(None, scan3.confirmed)

    def test_update_pallet_scan_missing_parameter(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        p1.scans.create(scanId="scan1", terminalId="terminal1")
        p1.save()

        update_data = {"testattribute": True}

        # act
        response = self.client.put(
            "pallets/ptest1/scan1",
            data=json.dumps(update_data),
            content_type="application/json",
        )

        # assert
        self.assertEqual(400, response.status_code)

        pallets_in_db = PalletModel.objects
        self.assertEqual(1, len(pallets_in_db))
        self.assertEqual("ptest1", pallets_in_db[0].palletId)
        pallet: PalletModel = pallets_in_db[0]
        self.assertEqual(1, len(pallet.scans))
        scan1: PalletScanModel = pallet.scans[0]
        self.assertEqual("scan1", scan1.scanId)
        self.assertEqual(None, scan1.confirmed)

    def test_get_pallet_scan_image_returns_404_if_scan_doesnt_exist(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.save()

        # act
        response = self.client.get("pallets/ptest1/scan2/image")

        # assert
        self.assertEqual(404, response.status_code)

    def test_get_pallet_feet_returns_404_if_pallet_doesnt_exist(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.save()

        # act
        response = self.client.get("pallets/ptest2/scan1/1")

        # assert
        self.assertEqual(404, response.status_code)

    def test_get_pallet_feet_returns_404_if_scan_doesnt_exist(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.save()

        # act
        response = self.client.get("pallets/ptest1/scan2/1")

        # assert
        self.assertEqual(404, response.status_code)

    def test_get_pallet_feet_returns_404_if_image_doesnt_exist(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.save()

        # act
        response = self.client.get("pallets/ptest1/scan1/100")

        # assert
        self.assertEqual(404, response.status_code)

    def test_get_pallet_feet_returns_500_if_multiple_scans_exist(self):
        # prepare
        p1 = PalletModel(palletId="ptest1")
        s1 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        s2 = PalletScanModel(scanId="scan1", terminalId="terminal1")
        p1.scans.append(s1)
        p1.scans.append(s2)
        p1.save()

        # act
        response = self.client.get("pallets/ptest1/scan1/100")

        # assert
        self.assertEqual(500, response.status_code)


if __name__ == "__main__":
    unittest.main()
