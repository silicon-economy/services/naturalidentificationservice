# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Tests for the REST Shipment API
"""

import json
import unittest
from mongoengine import disconnect

from flaskserver.server import Server
from flaskserver.models.pallet_model import PalletModel
from flaskserver.models.shipment_model import ShipmentModel
from flaskserver.models.product_information_model import ProductInformationModel
from flaskserver.models.shipment_detail_model import ShipmentDetailModel


class TestRestShipments(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        disconnect()

    def setUp(self):
        self.server = Server()

        # connects to mongomock database configured in test config (i.e. 'mongomock://localhost')
        self.app = self.server.create_app(config="test")
        self.app.config["TESTING"] = True
        self.client = self.app.test_client()

    def tearDown(self) -> None:
        disconnect()

    def test_shipments(self):
        # prepare
        pllt = PalletModel(palletId="ptest123")
        pllt.save()

        prodinfo = ProductInformationModel(
            weight=15.5, description="Äpfel", country_of_origin="Germany"
        )

        shpmtdtl = ShipmentDetailModel(sscc_nve="141421356237309504880")
        shpmtdtl.sender = (
            "Fraunhofer IML, Joseph-von-Fraunhofer Straße 2-4, 44227 Dortmund"
        )
        shpmtdtl.recipient = "Bundeskanzleramt, Willy-Brandt-Straße 1, 10557 Berlin"
        shpmtdtl.last_destination = "Umschlaglager Bayern Süd"
        shpmtdtl.next_destination = "Umschlaglager NRW Mitte"

        shpmt = ShipmentModel(
            shipment_id="shp1",
            carrier_detail=pllt,
            product_info=prodinfo,
            shipment_detail=shpmtdtl,
        )
        shpmt.save()

        # act
        response = self.client.get("shipments/")

        # assert
        self.assertEqual(200, response.status_code)
        responseobj = response.json
        self.assertIsNotNone(responseobj)
        self.assertEqual(len(responseobj), 1)
        shipment = responseobj[0]
        self.assertEqual(shipment["shipment_id"], "shp1")

        self.assertIsNotNone(shipment["carrier_detail"])
        carrier_detail = shipment["carrier_detail"]
        self.assertEqual("ptest123", carrier_detail["palletId"])

        self.assertIsNotNone(shipment["product_info"])
        product_info = shipment["product_info"]
        self.assertEqual(product_info["weight"], 15.5)

        self.assertIsNotNone(shipment["shipment_detail"])
        shipment_detail = shipment["shipment_detail"]
        self.assertEqual(shipment_detail["sscc_nve"], "141421356237309504880")

    def test_get_one_shipment(self):
        # prepare
        shpmt1 = ShipmentModel(shipment_id="shp1")
        shpmt2 = ShipmentModel(shipment_id="shp2")
        shpmt1.save()
        shpmt2.save()

        # act
        response = self.client.get("shipments/shp1")

        # assert
        self.assertEqual(200, response.status_code)
        responseobj = response.json
        self.assertIsNotNone(responseobj)
        self.assertEqual(shpmt1.shipment_id, responseobj["shipment_id"])

    def test_add_shipment_missing_carrier_detail_fails(self):
        # prepare
        p1 = PalletModel(palletId="ptesting")
        p1.save()

        data = {
            "shipment_id": "shpmttest",
            # 'carrier_detail': {
            #     'palletId': 'ptesting'
            # },
            "product_info": {
                "description": "some test procuct",
                "weight": 12.5,
                "country_of_origin": "France",
                "warning_labels": [],
            },
            "shipment_detail": {
                "sscc_nve": "",
                "sender": "",
                "recipient": "",
                "last_destination": "",
                "next_destination": "",
            },
        }

        # act
        response = self.client.post(
            "shipments/", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(400, response.status_code)
        shipments_in_db = ShipmentModel.objects
        self.assertEqual(len(shipments_in_db), 0)

    def test_add_shipment_duplicate_id(self):
        # prepare
        shpmt1 = ShipmentModel(shipment_id="shpmttest")
        shpmt1.save()
        data = {"shipment_id": "shpmttest"}

        # act
        response = self.client.post(
            "shipments/", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(400, response.status_code)
        shipments_in_db = ShipmentModel.objects
        self.assertEqual(len(shipments_in_db), 1)
        self.assertEqual("shpmttest", shipments_in_db[0].shipment_id)

    def test_update_shipment(self):
        # prepare
        prodinfo = ProductInformationModel(
            weight=15.5, description="Äpfel", country_of_origin="Germany"
        )
        shpmt1 = ShipmentModel(shipment_id="shpmttest", product_info=prodinfo)
        shpmt1.save()

        data = {"shipment_id": "shpmttestnew", "product_info": {"weight": 16.1}}

        # act
        response = self.client.put(
            "shipments/shpmttest",
            data=json.dumps(data),
            content_type="application/json",
        )

        # assert
        self.assertEqual(200, response.status_code)
        shipments_in_db = ShipmentModel.objects
        self.assertEqual(len(shipments_in_db), 1)
        shipment = shipments_in_db[0]
        self.assertEqual("shpmttestnew", shipment["shipment_id"])
        self.assertEqual(16.1, shipment["product_info"]["weight"])

    def test_update_shipment_fails_with_400_if_id_exists(self):
        # prepare
        prodinfo = ProductInformationModel(
            weight=15.5, description="Äpfel", country_of_origin="Germany"
        )
        shpmt1 = ShipmentModel(shipment_id="shpmttest1", product_info=prodinfo)
        shpmt1.save()

        shpmt2 = ShipmentModel(shipment_id="shpmttest2")
        shpmt2.save()

        data = {"shipment_id": "shpmttest2", "product_info": {"weight": 16.1}}

        # act
        response = self.client.put(
            "shipments/shpmttest1",
            data=json.dumps(data),
            content_type="application/json",
        )

        # assert
        self.assertEqual(400, response.status_code)
        shipments_in_db = ShipmentModel.objects
        self.assertEqual(len(shipments_in_db), 2)
        self.assertIsNotNone(ShipmentModel.objects(shipment_id="shpmttest1").first())
        self.assertIsNotNone(ShipmentModel.objects(shipment_id="shpmttest2").first())

    def test_update_shipment_fails_with_400_if_data_is_not_valild(self):
        # prepare
        prodinfo = ProductInformationModel(
            weight=15.5, description="Äpfel", country_of_origin="Germany"
        )
        shpmt1 = ShipmentModel(shipment_id="shpmttest1", product_info=prodinfo)
        shpmt1.save()

        data = {"product_info": {"weight": "This is not a float"}}

        # act
        response = self.client.put(
            "shipments/shpmttest1",
            data=json.dumps(data),
            content_type="application/json",
        )

        # assert
        self.assertEqual(400, response.status_code)
        shipments_in_db = ShipmentModel.objects
        self.assertEqual(len(shipments_in_db), 1)
        self.assertIsNotNone(ShipmentModel.objects(shipment_id="shpmttest1").first())

    def test_delete_shipment(self):
        # prepare
        shpmt1 = ShipmentModel(shipment_id="shp1")
        shpmt2 = ShipmentModel(shipment_id="shp2")
        shpmt1.save()
        shpmt2.save()

        # act
        response = self.client.delete("shipments/shp1")

        # assert
        self.assertEqual(200, response.status_code)
        self.assertIn(shpmt1.shipment_id, str(response.data))
        shipments_in_db = ShipmentModel.objects
        self.assertEqual(len(shipments_in_db), 1)
        self.assertEqual("shp2", shipments_in_db[0].shipment_id)

    def test_delete_shipment_unlinks_pallet(self):
        # prepare
        pllt = PalletModel(palletId="ptest123", shipment_id="shp1")
        pllt.save()

        prodinfo = ProductInformationModel(
            weight=15.5, description="Äpfel", country_of_origin="Germany"
        )

        shpmtdtl = ShipmentDetailModel(sscc_nve="141421356237309504880")
        shpmtdtl.sender = (
            "Fraunhofer IML, Joseph-von-Fraunhofer Straße 2-4, 44227 Dortmund"
        )
        shpmtdtl.recipient = "Bundeskanzleramt, Willy-Brandt-Straße 1, 10557 Berlin"
        shpmtdtl.last_destination = "Umschlaglager Bayern Süd"
        shpmtdtl.next_destination = "Umschlaglager NRW Mitte"

        shpmt = ShipmentModel(
            shipment_id="shp1",
            carrier_detail=pllt,
            product_info=prodinfo,
            shipment_detail=shpmtdtl,
        )
        shpmt.save()

        # act
        response = self.client.delete("shipments/shp1")

        # assert
        self.assertEqual(200, response.status_code)
        self.assertIn(shpmt.shipment_id, str(response.data))
        shipments_in_db = ShipmentModel.objects
        self.assertEqual(len(shipments_in_db), 0)

        pallet: PalletModel = PalletModel.objects(palletId="ptest123").first()
        self.assertIsNotNone(pallet)
        self.assertEqual(pallet.shipment_id, "")


if __name__ == "__main__":
    unittest.main()
