# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Tests for the REST API used by scan terminals
"""

import unittest
import json

from mongoengine import disconnect

from flaskserver.server import Server
from flaskserver.models.pallet_model import PalletModel
from flaskserver.models.terminal_model import TerminalModel


class TestRestTerminals(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        disconnect()

    def setUp(self):
        self.server = Server()

        # connects to mongomock database configured in test config (i.e. 'mongomock://localhost')
        self.app = self.server.create_app(config="test")
        self.app.config["TESTING"] = True
        self.client = self.app.test_client()

    def tearDown(self) -> None:
        disconnect()

    def test_terminals(self):
        # prepare
        t = TerminalModel(terminalId="t123", terminalType="arrival")
        t.save()

        # act
        response = self.client.get("terminals/")

        # assert
        self.assertEqual(200, response.status_code)
        responseobj = response.json
        self.assertIsNotNone(responseobj)
        self.assertEqual(t.terminalId, responseobj[0]["terminalId"])

    def test_get_one_terminal(self):
        # prepare
        t1 = TerminalModel(terminalId="ttest1", terminalType="arrival")
        t2 = TerminalModel(terminalId="ttest2", terminalType="arrival")
        t1.save()
        t2.save()

        # act
        response = self.client.get("terminals/ttest2")

        # assert
        self.assertEqual(200, response.status_code)
        responseobj = response.json
        self.assertIsNotNone(responseobj)
        self.assertEqual(t2.terminalId, responseobj["terminalId"])

    def test_get_terminal_last_scanned_pallet(self):
        # prepare
        t1 = TerminalModel(terminalId="ttest1", terminalType="arrival")
        p1 = PalletModel(palletId="ptest1")
        p1.save()

        t1.lastScannedPallet = p1
        t1.save()

        # act
        response = self.client.get("terminals/ttest1/lastScannedPallet")

        # assert
        self.assertEqual(200, response.status_code)
        responseobj = response.json
        self.assertIsNotNone(responseobj)
        self.assertEqual(p1.palletId, responseobj["palletId"])

    def test_get_terminal_last_scanned_pallet_404(self):
        # prepare
        t1 = TerminalModel(terminalId="ttest1", terminalType="arrival")
        t1.save()

        # act
        response = self.client.get("terminals/ttest1/lastScannedPallet")

        # assert
        self.assertEqual(404, response.status_code)

    def test_add_terminal(self):
        # prepare
        data = {"terminalId": "testterminal1", "terminalType": "arrival"}

        # act
        response = self.client.post(
            "terminals/", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(201, response.status_code)
        terminals_in_db = TerminalModel.objects
        self.assertEqual(len(terminals_in_db), 1)
        self.assertEqual("testterminal1", terminals_in_db[0].terminalId)
        self.assertEqual("arrival", terminals_in_db[0].terminalType)

    def test_add_terminal_missing_id(self):
        # prepare
        data = {"terminalType": "arrival"}

        # act
        response = self.client.post(
            "terminals/", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(400, response.status_code)
        terminals_in_db = TerminalModel.objects
        self.assertEqual(len(terminals_in_db), 0)

    def test_add_terminal_duplicate_id(self):
        # prepare
        t1 = TerminalModel(terminalId="ttest1", terminalType="arrival")
        t1.save()
        data = {"terminalId": "ttest1", "terminalType": "departure"}

        # act
        response = self.client.post(
            "terminals/", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(400, response.status_code)
        terminals_in_db = TerminalModel.objects
        self.assertEqual(len(terminals_in_db), 1)
        self.assertEqual("ttest1", terminals_in_db[0].terminalId)

    def test_update_terminal_terminalid(self):
        # prepare
        t1 = TerminalModel(terminalId="ttest1", terminalType="arrival")
        t1.save()

        data = {"terminalId": "ttest123", "terminalType": "departure"}

        # act
        response = self.client.put(
            "terminals/ttest1", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(200, response.status_code)
        terminals_in_db = TerminalModel.objects
        self.assertEqual(len(terminals_in_db), 1)
        self.assertEqual("ttest123", terminals_in_db[0].terminalId)
        self.assertEqual("departure", terminals_in_db[0].terminalType)

    def test_update_terminal(self):
        # prepare
        t1 = TerminalModel(terminalId="ttest1", terminalType="arrival")
        t1.save()

        data = {"terminalType": "departure"}

        # act
        response = self.client.put(
            "terminals/ttest1", data=json.dumps(data), content_type="application/json"
        )

        # assert
        self.assertEqual(200, response.status_code)
        terminals_in_db = TerminalModel.objects
        self.assertEqual(len(terminals_in_db), 1)
        self.assertEqual("ttest1", terminals_in_db[0].terminalId)
        self.assertEqual("departure", terminals_in_db[0].terminalType)

    def test_delete_terminal(self):
        # prepare
        t1 = TerminalModel(terminalId="ttest1", terminalType="arrival")
        t2 = TerminalModel(terminalId="ttest2", terminalType="arrival")
        t1.save()
        t2.save()

        # act
        response = self.client.delete("terminals/ttest1")

        # assert
        self.assertEqual(200, response.status_code)
        self.assertIn(t1.terminalId, str(response.data))
        terminals_in_db = TerminalModel.objects
        self.assertEqual(len(terminals_in_db), 1)
        self.assertEqual("ttest2", terminals_in_db[0].terminalId)


if __name__ == "__main__":
    unittest.main()
