# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Integration tests for the SocketIO API used by scan terminals
"""

import sys
import time
import unittest
from unittest.mock import MagicMock

from mongoengine import disconnect

from flaskserver.server import Server
from flaskserver.models.shipment_model import ShipmentModel
from flaskserver.models.terminal_model import TerminalModel
from flaskserver.models.pallet_model import PalletModel
from flaskserver.models.pallet_scan_model import PalletScanModel
from flaskserver.socketioapi.terminal import TerminalNamespace
from flaskserver.containers import Container

class TestSocketIOServer(unittest.TestCase):
    SOCKETIO_TERMINAL_LOGGER = 'flaskserver.socketioapi.terminal'

    @classmethod
    def tearDownClass(cls):
        disconnect()

    def setUp(self):
        # mock the save_image functions because they use MongoDB GridFSProxy
        self.save_image_mock = MagicMock(return_value='img_filename')
        self.save_pallet_feet_image_mock = MagicMock(return_value='pallet_feet_filename')
        TerminalNamespace.save_image = self.save_image_mock
        TerminalNamespace.save_pallet_feet_image = self.save_pallet_feet_image_mock

        self.delete_files_mock = MagicMock()
        TerminalNamespace.delete_files = self.delete_files_mock

        self.container = Container()
        self.container.init_resources()
        self.container.wire(modules=[sys.modules[__name__]])

        self.server = Server()
        self.app = self.server.create_app(config='test')
        self.app.config['TESTING'] = True
        self.sio = self.server.add_socketio(self.app)
    
    def tearDown(self) -> None:
        disconnect()
    
    def test_sio_creation(self):
        self.assertIsNotNone(self.sio)

    def test_connected(self):
        client = self.sio.test_client(self.app)
        client2 = self.sio.test_client(self.app)
        
        self.assertTrue(client.is_connected())
        self.assertTrue(client2.is_connected())
        self.assertNotEqual(client.eio_sid, client2.eio_sid)
        received = client.get_received()
        self.assertEqual(2, len(received))
        self.assertEqual(received[0]['args']['data'], 'Connected')
        self.assertEqual(received[1]['args']['data'], 'Connected')
        client.disconnect()
        self.assertFalse(client.is_connected())
        self.assertTrue(client2.is_connected())
        client2.disconnect()
        self.assertFalse(client2.is_connected())


    def test_cam_arrival(self):
        terminal_client = self.sio.test_client(self.app)
        webapp_client = self.sio.test_client(self.app)

        self.assertTrue(terminal_client.is_connected())
        self.assertTrue(webapp_client.is_connected())

        msg = {'message': 'Hello, World!', 'terminalId': 'testterminal', 'img64': 'test_base64'}
        terminal_client.emit('cam_arrival', msg)
        received_by_terminal = terminal_client.get_received()
        received_by_webapp = webapp_client.get_received()

        self.assertEqual(received_by_terminal[0]['args']['data'], 'Connected')

        # not sure why args contains a list here
        self.assertEqual(received_by_terminal[2]['args'][0]['img64'], 'test_base64')
        self.assertEqual(received_by_webapp[1]['args'][0]['img64'], 'test_base64')
        self.assertEqual(received_by_webapp[1]['name'], 'cam_arrival_update/testterminal')

    def test_cam_arrival_invalid(self):
        terminal_client = self.sio.test_client(self.app)
        webapp_client = self.sio.test_client(self.app)

        self.assertTrue(terminal_client.is_connected())
        self.assertTrue(webapp_client.is_connected())

        msg = {'message': 'Hello, World!', 'img64': 'test_base64'}
        terminal_client.emit('cam_arrival', msg)
        received_by_webapp = webapp_client.get_received()

        self.assertEqual(received_by_webapp[0]['args']['data'], 'Connected')

        self.assertEqual(len(received_by_webapp), 1) # no more messages

    def test_status_arrival(self):
        client = self.sio.test_client(self.app)

        self.assertTrue(client.is_connected())

        msg = {'message': 'Hello, World!', 'terminalId': 'testterminal', 'status': 'new_status'}
        client.emit('status_arrival', msg)
        received = client.get_received()
        self.assertEqual(received[0]['args']['data'], 'Connected')

        # not sure why args contains a list here
        self.assertEqual(received[1]['name'], 'status_arrival_update/testterminal')
        self.assertEqual(received[1]['args'][0]['status'], 'new_status')

    def test_status_arrival_invlaid(self):
        client = self.sio.test_client(self.app)

        self.assertTrue(client.is_connected())

        msg = {'message': 'Hello, World!', 'status': 'new_status'}
        client.emit('status_arrival', msg)
        received = client.get_received()
        self.assertEqual(received[0]['args']['data'], 'Connected')

        self.assertEqual(len(received), 1) # no more messages

    def test_scan_without_pallet_feet(self):
        # prepare
        PalletScanModel.image.put = MagicMock()

        t = TerminalModel(terminalId='terminalscan', terminalType='departure')
        t.save()

        scan_data = {
            'scanId': '0d443d9b-e984-46d2-94e6-61d0f862e93d',
            'terminalId': 'terminalscan',
            'palletId': 'ptestscan',
            'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
            'certainty': 0.42
        }

        scan = self._test_scan(scan_data)

        self.assertEqual(0, len(scan.pallet_feet))
        self.save_pallet_feet_image_mock.assert_not_called()
        self.delete_files_mock.assert_called_once_with(["img_filename"])
        shipments_in_db = ShipmentModel.objects
        self.assertEqual(1, len(shipments_in_db))
        shipment: ShipmentModel = shipments_in_db[0]
        pallet: PalletModel = shipment.carrier_detail
        self.assertEqual('ptestscan', pallet.palletId)


    def test_scan_with_pallet_feet(self):
        # prepare
        PalletScanModel.image.put = MagicMock()

        t = TerminalModel(terminalId='terminalscan', terminalType='arrival')
        t.save()

        scan_data = {
            'scanId': '0d443d9b-e984-46d2-94e6-61d0f862e93d',
            'terminalId': 'terminalscan',
            'palletId': 'ptestscan',
            'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
            'certainty': 0.42,
            'pallet_feet': [ 
                {'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=', 'description': 'left'},
                {'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=', 'description': 'center'},
                {'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=', 'description': 'right'},
            ]
        }

        scan = self._test_scan(scan_data)
        self.assertEqual(3, len(scan.pallet_feet))
        self.save_pallet_feet_image_mock.assert_called()
        self.delete_files_mock.assert_called_once_with(["img_filename", "pallet_feet_filename", "pallet_feet_filename", "pallet_feet_filename"])

    def test_scan_existing_arrival(self):
        self._test_scan_existing('arrival')
        pallet: PalletModel = PalletModel.objects[0]
        self.assertEqual('In warehouse', pallet.status)

    def test_scan_existing_departure(self):
        self._test_scan_existing('departure')
        pallet: PalletModel = PalletModel.objects[0]
        self.assertEqual('Transport', pallet.status)

    def _test_scan_existing(self, terminal_type: str):
        # prepare
        PalletScanModel.image.put = MagicMock()

        t = TerminalModel(terminalId='terminalscan', terminalType=terminal_type)
        t.save()

        p1 = PalletModel(palletId='ptestscan')
        s1 = PalletScanModel(scanId='scan1', terminalId='terminalscan')
        p1.scans.append(s1)
        p1.save()

        scan_data = {
            'scanId': '0d443d9b-e984-46d2-94e6-61d0f862e93d',
            'terminalId': 'terminalscan',
            'palletId': 'ptestscan',
            'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
            'certainty': 0.42
        }

        self._test_scan(scan_data, scan_count=2)
        pallet: PalletModel = PalletModel.objects[0]
        self.assertEqual(f"{terminal_type} (terminalscan)", pallet.last_event)

    def _test_scan(self, scan_data, scan_count = 1):
        client = self.sio.test_client(self.app)

        self.assertTrue(client.is_connected())

        # act
        client.emit('scan', scan_data)
        received = client.get_received()
        self.assertEqual(received[0]['args']['data'], 'Connected')

        # wait for socketio event to be processed
        time.sleep(1)

        # assert
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 1)
        pallet: PalletModel = pallets_in_db[0]
        self.assertEqual('ptestscan', pallet.palletId)
        self.assertEqual(scan_count, len(pallet.scans))
        scan: PalletScanModel = pallet.scans[-1]
        self.assertEqual('0d443d9b-e984-46d2-94e6-61d0f862e93d', scan.scanId)
        self.assertEqual('terminalscan', scan.terminalId)
        self.save_image_mock.assert_called_once()
        return scan

    def test_scan_wrong_format(self):
        # prepare
        PalletScanModel.image.put = MagicMock()

        t = TerminalModel(terminalId='terminalscan', terminalType='arrival')
        t.save()

        client = self.sio.test_client(self.app)

        self.assertTrue(client.is_connected())

        scan_data = 'some invalid data'

        # act
        client.emit('scan', scan_data)
        received = client.get_received()
        self.assertEqual(received[0]['args']['data'], 'Connected')

        # wait for socketio event to be processed
        time.sleep(1)

        # assert
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 0)
        self.save_image_mock.assert_not_called()
        self.save_pallet_feet_image_mock.assert_not_called()


    def test_scan_missing_scanid(self):
        scan_data = {
            'terminalId': 'terminalscan',
            'palletId': 'ptestscan',
            'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
            'certainty': 0.42
        }

        self.scan_missing_parameter(scan_data, 'WARNING:flaskserver.socketioapi.terminal:received invalid terminal scan message: missing scanId')


    def test_scan_missing_terminalid(self):
        scan_data = {
            'scanId': '0d443d9b-e984-46d2-94e6-61d0f862e93d',
            'palletId': 'ptestscan',
            'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
            'certainty': 0.42
        }

        self.scan_missing_parameter(scan_data, 'WARNING:flaskserver.socketioapi.terminal:received invalid terminal scan message: missing terminalId')

    def test_scan_missing_palletId(self):
        scan_data = {
            'scanId': '0d443d9b-e984-46d2-94e6-61d0f862e93d',
            'terminalId': 'terminalscan',
            'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
            'certainty': 0.42
        }

        self.scan_missing_parameter(scan_data, 'WARNING:flaskserver.socketioapi.terminal:received invalid terminal scan message: missing palletId')


    def test_scan_missing_img64(self):
        scan_data = {
            'scanId': '0d443d9b-e984-46d2-94e6-61d0f862e93d',
            'terminalId': 'terminalscan',
            'palletId': 'ptestscan',
            'certainty': 0.42
        }

        self.scan_missing_parameter(scan_data, 'WARNING:flaskserver.socketioapi.terminal:received invalid terminal scan message: expected base64 image img64')

    def test_scan_missing_wrong_pallet_feet_format(self):
        scan_data = {
            'scanId': '0d443d9b-e984-46d2-94e6-61d0f862e93d',
            'terminalId': 'terminalscan',
            'palletId': 'ptestscan',
            'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
            'certainty': 0.42,
            'pallet_feet': 'this should be a list'
        }

        self.scan_missing_parameter(scan_data, 'WARNING:flaskserver.socketioapi.terminal:received invalid terminal scan message: expected pallet_feet to be a list')

    def test_scan_missing_no_image_on_pallet_feet(self):
        scan_data = {
            'scanId': '0d443d9b-e984-46d2-94e6-61d0f862e93d',
            'terminalId': 'terminalscan',
            'palletId': 'ptestscan',
            'img64': 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=',
            'certainty': 0.42,
            'pallet_feet': [{'desc': 'this guy is missing an image'}]
        }

        self.scan_missing_parameter(scan_data, 'WARNING:flaskserver.socketioapi.terminal:received invalid terminal scan message: expected all entries in pallet_feet to have an image specified as img64')

    def scan_missing_parameter(self, scan_data, expected_log):
        # prepare
        PalletScanModel.image.put = MagicMock()

        t = TerminalModel(terminalId='terminalscan', terminalType='arrival')
        t.save()

        client = self.sio.test_client(self.app)

        self.assertTrue(client.is_connected())

        # act
        with self.assertLogs(self.SOCKETIO_TERMINAL_LOGGER, level='DEBUG') as sio_logger:

            client.emit('scan', scan_data)
            received = client.get_received()
            self.assertEqual(received[0]['args']['data'], 'Connected')

            # wait for socketio event to be processed
            time.sleep(1)

        # assert
        pallets_in_db = PalletModel.objects
        self.assertEqual(len(pallets_in_db), 0)
        self.assertIn(expected_log, sio_logger.output)
        self.save_image_mock.assert_not_called()

if __name__ == '__main__':
    unittest.main()
