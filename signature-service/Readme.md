# Naturident Signature-Service
## Rest
The REST-API includes a interactive Swagger documentation at http://localhost:5001.

## Start Signature-Service without docker-compose
To start this Service from CLI without the docker-compose file please ensure that you have Torchreid installed on your machine.
For the installation please follow the instruction of the project at [Torchreid by Zhou, Kaiyang and Xiang, Tao](https://github.com/KaiyangZhou/deep-person-reid).

To ensure the the ML-model please ensure that the working directory is set to:
 `naturalidentificationservice/signature-service/`

In case the machine has low memory and you want to process many IDs you can release the Milvus database from memory by setting `MILVUS_RELEASE_COLLECTIONS=True` in `config.py`.

# Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.csv` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.csv` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.
* `third-party-licenses-summary.txt` - Contains a summary of all licenses used by the third-party dependencies in this project.
  The content of this file is/can be generated.

### Generating third-party license reports

This project uses [pip-licenses](https://pypi.org/project/pip-licenses/) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.csv` file can be generated using the following command:

`pip-licenses --ignore-packages pkg-resources --with-urls --format=csv --output-file=third-party-licenses/third-party-licenses.csv`

Third-party dependencies for which the licenses cannot be determined automatically by the license-checker have to be documented manually in `third-party-licenses/third-party-licenses-complementary.csv`.
In the `third-party-licenses/third-party-licenses.csv` file these third-party dependencies have an "UNKNOWN" license.

The `third-party-licenses/third-party-license-summary.txt` file can be generated using the following command:

`pip-licenses --ignore-packages pkg-resources --summary --output-file=third-party-licenses/third-party-licenses-summary.txt`
