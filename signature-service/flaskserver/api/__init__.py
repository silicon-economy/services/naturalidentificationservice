# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
API specification

All resources are added in their own namespaces
"""

from flask import Blueprint
from flask_restx import Api

from flaskserver.routes.flask_restx_models import api as models_api
from flaskserver.routes.signature_route import api as pallet_feet_api


# create the api endpoint
api_bp = Blueprint("api", __name__)
api = Api(api_bp, version="1.0", title="Naturident Signature-Service API")


# add namespace here
api.add_namespace(models_api)
api.add_namespace(pallet_feet_api)


__all__ = ["api_bp"]
