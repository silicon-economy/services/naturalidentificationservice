# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Configuration for the Natureident Signature-Service.
"""
# pylint: disable=too-few-public-methods

import os

class BaseConfig:
    """
    Base configuration - allow specific configurations to override settings
    """
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'secret!'
    SESSION_COOKIE_SECURE = True

    MILVUS_HOSTNAME = os.environ.get("MILVUS_HOSTNAME", "milvus")
    MILVUS_PORT = os.environ.get("MILVUS_PORT", "19530")
    MILVUS_RELEASE_COLLECTIONS = False  # Set to True if not enough memory available for feature vector extraction
    MILVUS_TEST_CONNECTION = True

    ML_TOP_K = 30
    ML_METRIC_TYPE = "IP"
    ML_MODEL_NAME = "model.pth.tar"

class ProductionConfig(BaseConfig):
    """
    Configuration is the same as in the base object, which should be safe as default
    You at least need to override the secret key for production
    """


class DevelopmentConfig(BaseConfig):
    """
    Production used for local development
    """
    DEBUG = True
    SESSION_COOKIE_SECURE = False

    MILVUS_HOSTNAME = os.environ.get("MILVUS_HOSTNAME", "localhost")
    MILVUS_PORT = os.environ.get("MILVUS_PORT", "19530")
    MILVUS_TEST_CONNECTION = True


class TestConfig(BaseConfig):
    """
    Configuration used for tests
    """
    TESTING = True
    SESSION_COOKIE_SECURE = False

    MILVUS_HOSTNAME = os.environ.get("MILVUS_HOSTNAME", "localhost")
    MILVUS_PORT = os.environ.get("MILVUS_PORT", "19530")
    MILVUS_RELEASE_COLLECTIONS = True
    MILVUS_TEST_CONNECTION = False
