# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
api model
"""

from flask_restx import Namespace, fields as apifields

api = Namespace(
    'signature',
    description='Endpoint for creating and recognizing pallets based on pallet feet images'
)

pallet_foot_image_dao = api.model('Pallet Foot', {
    'image_string':
        apifields.String(
            required=True,
            attribute='image_string',
            description='Image of this pallet foot (base64 encoded)'
        ),
    'foot_position':
        apifields.String(
            required=True,
            attribute='foot_position',
            description='Position of the foot on the pallet (e.g. l, m, r)'
        )
})

pallet_payload_dao = api.model('Payload', {
    'pallet_feet_images':
        apifields.List(
            apifields.Nested(pallet_foot_image_dao)
        )
})

response_payload_dao = api.model('Response', {
    'pallet_id':
        apifields.String(
            attribute='pallet_id',
            description='The ID of the newly created or identified pallet'
        )
})
