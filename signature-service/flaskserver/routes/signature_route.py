# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.


"""
HTTP REST API for signature management

"""
import logging

from flask_restx import Namespace, Resource, abort
from flaskserver.routes.flask_restx_models import pallet_payload_dao, response_payload_dao
from flask_restx.marshalling import marshal
from flaskserver.utils.process_request import process_request_body
from flaskserver.signature_management.signature_management import SignatureManagement, SignaturManagementState

api = Namespace('signature', description='Endpoint for creating and recognizing pallets based on pallet feet images')

logger = logging.getLogger(__name__)

@api.route('/create_pallet')
class PalletFeetImages(Resource):
    """Post pallet feet images to store them as a new pallet"""
    
    @api.doc('create_pallet')
    @api.expect(pallet_payload_dao, validate=True)
    @api.response(code=201, model=response_payload_dao, description="Successfully created a new pallet")
    @api.response(code=400, description="Request-Payload of wrong type")
    @api.response(code=500, description="Internal server error")
    def post(self):
        """Endpoint to POST images for a new pallet

        Returns:
            201: an id for the newly created pallet
            400: request-payload of wrong type
            500: internal server error (e.g. error connecting to milvus database)
        """
        # Get the images from the request -> pallet_feet_images contains tuples of type (cv2_image, foot_position)
        request_body = api.payload # Request must have application/json content type
        pallet_feet_images = process_request_body(request_body)
        if len(pallet_feet_images) == 0:
            return abort(400, "There were no correct images in the request")
        
        # Process images with ML-Based Signature Generation (create signatures)
        status, generated_pallet_it = SignatureManagement().create_signatures_in_db(pallet_feet_images)
        # -> return pallet id
        if status == SignaturManagementState.pallet_id_created:
            return marshal({"pallet_id" : generated_pallet_it}, response_payload_dao), 201
        return abort(400, "Pallet-ID could not be created")


@api.route('/reid_pallet')
class PalletID(Resource):
    """Post pallet feet images to perform a reidentification task"""

    @api.doc('reid_pallet')
    @api.expect(pallet_payload_dao, validate=True)
    @api.response(code=200, model=response_payload_dao, description="Successfully reidentified pallet")
    @api.response(code=400, description="Request-Payload of wrong type")
    @api.response(code=404, description="Pallet not found")
    @api.response(code=500, description="Internal server error")
    def post(self):
        """Endpoint to POST images in order to identify a pallet

        Returns:
            200: pallet id of a found pallet
            400: request-payload of wrong type
            404: pallet not found
            500: internal server error (e.g. error connecting to milvus database)
        """
        # Get the images from the request -> pallet_feet_images contains tuples of type (cv2_image, foot_position)
        request_body = api.payload # Request must have application/json content type
        pallet_feet_images = process_request_body(request_body)
        if len(pallet_feet_images) == 0:
            return abort(400, "There were no correct images in the request")

        # Process images with ML-Based Signature Generation (create signatures)
        status, pallet_id = SignatureManagement().reidentify_signatures_in_db(pallet_feet_images)
        if status == SignaturManagementState.pallet_not_found:
            return abort(404, "No matching pallet found")
        # -> return pallet id with the highest probability of being the same pallet
        return marshal({"pallet_id" : pallet_id}, response_payload_dao), 200
