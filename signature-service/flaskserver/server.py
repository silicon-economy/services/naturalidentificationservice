# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Starts flask server.
Initializes flask server.
Initializes loggers.
Initializes signature management.
"""

# Flack-SocketIO docs: 
# it is recommended that you apply the monkey patching at the top of your main script, 
# even above your imports.

import logging
import os
import socket
import traceback
from contextlib import closing

from gevent import monkey
from gevent.pywsgi import WSGIServer

from flask_cors import CORS
from flask import Flask

from pymilvus import (
    connections
)

from flaskserver.api import api_bp
from flaskserver.signature_management.signature_management import SignatureManagement


class Server:
    """"
    Contains main logic to setup this service
    """

    BACKEND_DEBUG_MODE_PARAM_NAME = 'BACKEND_DEBUG_MODE'
    backend_debug = os.getenv(BACKEND_DEBUG_MODE_PARAM_NAME, 'False') == 'True'  # don't use bool()

    logger = logging.getLogger(__name__)
    
    def __init__(self):
        Server.init_loggers()

    def __del__(self):
        SignatureManagement().close_connection()

    def start(self, config: str = 'production'):
        self.logger.info('Starting Signature-Service')
        self.logger.info(f'Config: {config}')

        if config == 'production':
            monkey.patch_all()

        app = Server.create_app(config)
        self.backend_debug = os.getenv(self.BACKEND_DEBUG_MODE_PARAM_NAME, 'False') == 'True'  # don't use bool()

        if app.config.get("MILVUS_TEST_CONNECTION"):
            host = app.config.get("MILVUS_HOSTNAME")
            port = int(app.config.get("MILVUS_PORT"))
            self.test_milvus_connection(host, port)

        SignatureManagement(app)

        if config == 'production':
            # use production webserver
            http_server = WSGIServer(('0.0.0.0', 5001), app)
            http_server.serve_forever()
        else:
            # use dev webserver
            app.run(host='0.0.0.0', port=5001)

    @staticmethod
    def create_app(config: str) -> Flask:
        """
        Creates flask app.
        Registers blue print for api.
        Removes CORS filter
        :return: the flask app
        """
        flask_app = Flask(__name__)

        if flask_app.config['ENV'] == 'development' or config == 'development':
            flask_app.config.from_object('flaskserver.config.DevelopmentConfig')
        elif config == 'test':
            flask_app.config.from_object('flaskserver.config.TestConfig')
        elif config == 'production':
            flask_app.config.from_object('flaskserver.config.ProductionConfig')

        CORS(flask_app)
        Server.register_blueprints(flask_app)
        return flask_app

    @staticmethod
    def register_blueprints(flask_app: Flask):
        """
        Register all blueprints used in the python-backend
        """
        flask_app.register_blueprint(api_bp)

    @staticmethod
    def init_loggers():
        """
        Initializes loggers of used libraries/frameworks.
        """
        logging.getLogger('gevent').setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    def test_milvus_connection(self, host: str, port: int):
        self.logger.info("Checking if Milvus DB Port is reachable and open")
        self.check_socket(host, port)

        # create test connection
        try:
            self.logger.info("Attempting connecion to milvus DB")
            connections.connect(host=host, port=str(port))

            connections.disconnect('default')
        except Exception:
            self.logger.error(traceback.format_exc())
            self.logger.error("Connection to milvus DB failed")
            return
        self.logger.info("Connection to milvus DB successful")

    def check_socket(self, host: str, port: int):
        """Tests if a port on host is recahble and open

        Args:
            host (str): host to connect to
            port (int): port number to check
        """
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            if sock.connect_ex((host, port)) == 0:
                self.logger.info(f"Port %d on host %s is open", port, host)
            else:
                self.logger.error(f"Port %d on host %s is not open", port, host)


if __name__ == '__main__':
    Server().start(config='development') # run with dev config for now because gevent conflicts with pymilvus
