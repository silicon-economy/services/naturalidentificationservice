# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Extracts feature Vectors with ML Model.
Manages signatures in Milvus DB
"""

import logging
from enum import Enum

import numpy as np
from torchreid.utils import FeatureExtractor

from pymilvus import (
    connections, FieldSchema, CollectionSchema, DataType,
    Collection, list_collections, SearchResult)


class SignaturManagementState(Enum):
    pallet_id_found = 200
    pallet_id_created = 201
    pallet_not_found = 404


class SignatureManagement:
    """
    Singleton class for signature managment
    """

    __instance = None

    def __new__(cls, app=None):
        if SignatureManagement.__instance is None:
            SignatureManagement.__instance = object.__new__(cls)

            SignatureManagement.__instance.logger = logging.getLogger(__name__)

        if app:
            # create feature extractor
            model_name = app.config.get("ML_MODEL_NAME")
            model_path = './flaskserver/model/' + model_name
            SignatureManagement.__instance.extractor = FeatureExtractor(
                model_name='pcb_p4',
                model_path=model_path,
                device='cpu'
            )

            SignatureManagement.__instance.top_k =app.config.get("ML_TOP_K")
            metric_type = app.config.get("ML_METRIC_TYPE")
            SignatureManagement.__instance.metric_type = metric_type
            SignatureManagement.__instance.search_params = {"metric_type": metric_type, "params": {}}
            SignatureManagement.__instance.app = app
            SignatureManagement.__instance.host = app.config.get("MILVUS_HOSTNAME")
            SignatureManagement.__instance.port = app.config.get("MILVUS_PORT")
            SignatureManagement.__instance.release_collections = app.config.get("MILVUS_RELEASE_COLLECTIONS")
            SignatureManagement.__instance.connection = connections.connect(
                "default",
                host=SignatureManagement.__instance.host,
                port=SignatureManagement.__instance.port
            )
            SignatureManagement.__instance.logger.info("Opened connection to Milvus DB")

        return SignatureManagement.__instance

    def close_connection(self):
        connections.disconnect("default")
        self.logger.info("conncection to Milvus closed")

    def create_signatures_in_db(self, pallet_feet_data: [(np.ndarray, int)]) -> (SignaturManagementState, int):
        """
        Register a pallet given its pallet feet images from Database
        :param pallet_feet_data: list of pallet feet images as numpy array and their according positions in the Pallet
        :return: id which the database generated for the pallet
        """

        pallet_feet_images, pallet_feet_positions = map(list, zip(*pallet_feet_data))
        self.logger.info("Pallet feet positions: " + str(pallet_feet_positions))

        # extract vectors
        vectors = self.extract_vectors(pallet_feet_images)

        # load pallet feet collection
        collection = self.create_pallet_feet_collection(len(vectors[0]))

        # load pallet schema
        collection_pallet = self.create_pallet_collection()

        # add new pallet to pallet collection
        mock_vector = np.zeros((1, 1)).astype('float32')
        mock_vector.tolist()
        pallet_id_response = self.insert_in_collection(
            collection_pallet,
            [
                [mock_vector],
            ]

        )

        # add new pallet feet to pallet feet collection
        generated_pallet_id = pallet_id_response.primary_keys[0]
        self.logger.info("Pallet id: {}".format(generated_pallet_id))

        self.insert_in_collection(
            collection,
            [
                [generated_pallet_id for _ in range(len(pallet_feet_positions))],
                [int(pallet_feet_positions[i]) for i in range(len(pallet_feet_positions))],
                vectors
            ]
        )

        self.logger.info("Entity count: " + str(collection.num_entities))
        status = SignaturManagementState.pallet_id_created

        return status, generated_pallet_id

    def reidentify_signatures_in_db(self, pallet_feet_data: [(np.ndarray, int)]) -> (SignaturManagementState, int):
        """
        Reidentify a pallet given its pallet feet images from database
        :param pallet_feet_data: list of pallet feet images as numpy array and their according positions in the Pallet
        :return: id>0 of the pallet if the pallet was identified in the database
        """

        pallet_feet_images, pallet_feet_positions = map(list, zip(*pallet_feet_data))
        self.logger.info("Pallet feet positions: " + str(pallet_feet_positions))

        # extract vectors
        vectors = self.extract_vectors(pallet_feet_images)

        # create pallet feet collection
        collection = self.create_pallet_feet_collection(len(vectors[0]))

        self.logger.info("Search...")
        collection.load()

        # define output_fields of search result
        res = collection.search(
            vectors, "signature", self.search_params, limit=self.top_k,
            output_fields=["pallet_feet_id", "pallet_id", "position"]
        )

        if self.release_collections:
            collection.release()

        pallet_id = self.identify_pallet(res, pallet_feet_positions)

        self.logger.info("Pallet id: {}".format(pallet_id))

        return pallet_id

    def identify_pallet(self, search_result: SearchResult, pallet_feet_positions: [np.ndarray]) -> (SignaturManagementState, int):
        """
        Identifies a pallet by the distances given from the milvus database and the according postion of the pallet feet
        :param search_result: search result of the pallet feet feature vectors from the milvus database
        :param pallet_feet_positions: pallet feet positions according to the input
        :return: pallet id
        """
        pallet_ids = []
        pallet_probabilities = []
        for i, hits in enumerate(search_result):
            if pallet_feet_positions[i] == hits[0].entity.get('position'):
                pallet_ids.append(int(hits[0].entity.get('pallet_id')))
                pallet_probabilities.append(float(hits.distances[0]))
        most_frequent_id = max(set(pallet_ids), key=pallet_ids.count) if len(pallet_ids) > 0 else 0
        self.logger.info("Most frequent id: {}".format(most_frequent_id))

        # check threshold to consider if pallet already exists
        if len(pallet_ids) <= 0 or sum(pallet_probabilities) < 0.6 * len(pallet_feet_positions):
            return SignaturManagementState.pallet_not_found, None

        return SignaturManagementState.pallet_id_found, most_frequent_id

    def extract_vectors(self, pf_images: [np.ndarray]) -> [np.ndarray]:
        """
        Extracts feature vectors with personReid from pallet feet images
        :param pf_images: list of pallet feet images
        :return: list of feature vector
        """
        #extract vector with ML
        self.logger.info("Extract vectors from images")
        db_vectors = self.extractor(pf_images)

        assert db_vectors.shape[0] == len(pf_images), "there should be one vector for each pallet feet"

        db_vectors /= np.linalg.norm(db_vectors, axis=1)[:, np.newaxis]  # normalize vectors before we add them to the index
        vectors = db_vectors.tolist()
        return vectors

    def create_pallet_feet_collection(self, dim: int) -> Collection:
        """
        Creates the pallet feet schema in the milvus database and gives a handle on the Collection Object back.
        If the schema already exists it just gives back the according Collection Object.
        :param dim: dimension of the feature vecotor for the milvus database index
        :return: Collection Object for pallet feet
        """
        collection_name = "pallet_feets"
        if not self.connection.has_collection(collection_name):
            # create pallet feet schema
            self.logger.info("Dimension: {}".format(dim))
            default_fields = [
                FieldSchema(name="pallet_feet_id", dtype=DataType.INT64, is_primary=True, auto_id=True),
                FieldSchema(name="pallet_id", dtype=DataType.INT64),
                FieldSchema(name="position", dtype=DataType.INT64),
                FieldSchema(name="signature", dtype=DataType.FLOAT_VECTOR, dim=dim),
            ]
            default_schema = CollectionSchema(fields=default_fields, description="pallet collection schema")
            self.logger.info("Create collection for pallet feet...")
            collection = Collection(name=collection_name, schema=default_schema)
        else:
            self.logger.info("Load collection for pallet feet...")
            collection = Collection(name=collection_name)

        return collection

    def create_pallet_collection(self) -> Collection:
        """
        Creates the pallet schema in the milvus database and gives a handle on the Collection Object back.
        If the schema already exists it just gives back the according Collection Object.
        :return: Collection Object for pallet
        """
        collection_name = "pallet_counter"
        if not self.connection.has_collection(collection_name):
            # create pallet schema
            default_fields_pallet = [
                FieldSchema(name="pallet_id", dtype=DataType.INT64, is_primary=True, auto_id=True),
                FieldSchema(name="mock_vector", dtype=DataType.FLOAT_VECTOR, dim=1),
            ]
            pallet_schema = CollectionSchema(fields=default_fields_pallet, description="pallet collection schema")
            self.logger.info("Create pallet collection for pallet...")
            collection_pallet = Collection(name=collection_name, schema=pallet_schema)
        else:
            self.logger.info("Load collection for pallet...")
            collection_pallet = Collection(name=collection_name)
        return collection_pallet

    def insert_in_collection(self, collection: Collection, values: []):
        return_value = collection.insert(values)
        self.connection.flush([collection.name])

        if self.release_collections:
            collection.release()

        return return_value
