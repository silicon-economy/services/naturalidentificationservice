# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""

Process post request body and decode image to numpy array

"""

import base64
import binascii
import logging
from enum import Enum
import numpy as np
import cv2

logger = logging.getLogger(__name__)


class FootPosition(Enum):
    """Enum for transltion of directional Foot Position to Int for Database"""
    l = 0
    m = 1
    r = 2


def process_request_body(body: {str: [{str: str}]}) -> [(np.ndarray, int)]:
    """
    Translate the request body to a Database understandable Object
    :param body: Request body of pallet to add or reidentify
    :return: Pallet Object with tuples of feet images as numpy ndarray with their according postion
    """
    logger.info("Processing request data... {}".format(body))
    data = body['pallet_feet_images']
    pfi = []
    for img_data in data:
        img_string = img_data['image_string']
        img_bytes = is_base64(img_string)
        if img_bytes is not None:
            img_arr = np.frombuffer(img_bytes, dtype=np.uint8)
            pallet_foot_image = cv2.imdecode(img_arr, flags=cv2.IMREAD_COLOR)
            if pallet_foot_image is not None:
                #logger.info("foot position: " + str(FootPosition[img_data['foot_position']].value))
                if str(img_data['foot_position']).isnumeric():
                        pfi.append((pallet_foot_image, FootPosition(int(img_data['foot_position'])).value))
                else:
                    if img_data['foot_position'] in FootPosition._member_names_:
                        pfi.append((pallet_foot_image, FootPosition[img_data['foot_position']].value))
                    else:
                        pfi.append((pallet_foot_image, -1))
    return pfi


def is_base64(img):
    """
    Decodes image string wit base64 encoding to bytes
    :param img: base64 string of image
    :return: decoded image as bytes
    """
    try:
        return base64.b64decode(img, validate=True)
    except binascii.Error:
        return None
