# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

from setuptools import setup

setup(
    name='natureident.signature-service',
    version='0.0.0',
    packages=['tests', 'flaskserver'],
    url='https://www.siliconeconomy.org',
    license='Apache License, Version 2.0',
    description='The signature service for the  natureident project.',
    # todo: update install_requires
    install_requires=['Flask',
                      'Flask-Cors',
                      'flask-restx',
                      'flask-pymongo'],
    entry_points={
        "console_scripts": [
            'server=flaskserver:server'
        ]
    }
)
