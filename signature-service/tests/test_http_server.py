# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

import unittest

from flaskserver.server import Server


class TestHTTPServer(unittest.TestCase):

    def setUp(self):
        self.server = Server()
        self.app = self.server.create_app(config='test')
        self.app.config['TESTING'] = True

    def test_app_creation(self):
        self.assertIsNotNone(self.app)

    def test_index_route(self):
        client = self.app.test_client()
        response = client.get('/')
        self.assertEqual(200, response.status_code)


if __name__ == '__main__':
    unittest.main()
