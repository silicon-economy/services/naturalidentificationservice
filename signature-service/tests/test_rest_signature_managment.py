# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Tests for signature Rest Api
"""
import base64
from unittest import mock

import cv2
import unittest

import numpy as np
import json

from pymilvus import connections
from torchreid.utils import FeatureExtractor

from flaskserver.server import Server
from flaskserver.signature_management.signature_management import SignatureManagement, SignaturManagementState
from flaskserver.utils.process_request import FootPosition


class MyTestCase(unittest.TestCase):

    def setUp(self):
        with mock.patch('pymilvus.connections.connect', return_value=connections.instance):
            self.server = Server()
            self.app = self.server.create_app(config='test')
            self.app.config['TESTING'] = True

            self.height = 384
            self.width = 128

            self.signature_management = SignatureManagement(self.app)

            model_name = self.app.config.get("ML_MODEL_NAME")
            with mock.patch.object(self.signature_management, "extractor") as extractor_mocked:
                extractor_mocked.return_value = FeatureExtractor(
                        model_name='pcb_p4',
                        model_path="../flaskserver/model/"+model_name,
                        device='cpu'
                    )
            self.client = self.app.test_client()

    def test_post_store_one_pallet(self):
        # prepare

        # create a pallet with 3 random pallet feet images
        nb = 3
        np.random.seed()
        pallet = [
            (np.random.uniform(low=0, high=255, size=(self.height, self.width, 3)).astype(np.uint8), i)
            for i in range(nb)
        ]

        pallet_feet_images = {
            "pallet_feet_images": []
        }
        for feet, position in pallet:
            _, buffer = cv2.imencode('.jpg', feet)
            image_string = base64.b64encode(buffer).decode()
            foot_position = position
            pallet_feet_images["pallet_feet_images"].append(
                {
                    "image_string": image_string,
                    "foot_position": str(foot_position)
                }
            )
        data = json.dumps(pallet_feet_images)
        invalid_data = json.dumps({"pallet_feet_images": []})

        # act
        with mock.patch(
            'flaskserver.signature_management.signature_management.SignatureManagement.create_signatures_in_db',
            return_value=(SignaturManagementState.pallet_id_created, 2004)
        ):
            response = self.client.post(
                "/signature/create_pallet", data=data, content_type="application/json"
            )
        invalid_response = self.client.post(
            "/signature/create_pallet", data=invalid_data, content_type="application/json"
        )

        # assert
        self.assertEqual(201, response.status_code)
        self.assertEqual(400, invalid_response.status_code)
        responseobj = response.json
        self.assertIsNotNone(responseobj)
        self.assertTrue(responseobj["pallet_id"].isdigit())

    def test_post_reidentify_pallet(self):
        # prepare

        # create a pallet with 3 random pallet feet images
        self.nb = 3
        np.random.seed()
        self.pallet = [
            (np.random.uniform(low=0, high=255, size=(self.height, self.width, 3)).astype(np.uint8), i)
            for i in range(self.nb)
        ]
        pallet_feet_images = {
            "pallet_feet_images": []
        }
        for foot, position in self.pallet:
            _, buffer = cv2.imencode('.jpg', foot)
            image_string = base64.b64encode(buffer).decode()
            foot_position = FootPosition(position).name
            pallet_feet_images["pallet_feet_images"].append(
                {
                    "image_string": image_string,
                    "foot_position": foot_position
                }
            )
        data = json.dumps(pallet_feet_images)

        # create invalid pallet
        self.invalid_pallet = [
            (np.zeros([self.height, self.width, 3]).astype(np.uint8), i)
            for i in range(self.nb)
        ]
        invalid_pallet_feet_images = {
            "pallet_feet_images": []
        }
        for foot, position in self.invalid_pallet:
            _, buffer = cv2.imencode('.jpg', foot)
            image_string = base64.b64encode(buffer).decode()
            foot_position = FootPosition(position).name
            invalid_pallet_feet_images["pallet_feet_images"].append(
                {
                    "image_string": image_string,
                    "foot_position": foot_position
                }
            )
        invalid_data = json.dumps(invalid_pallet_feet_images)

        no_pallet_data = json.dumps({"pallet_feet_images": []})

        with mock.patch(
            'flaskserver.signature_management.signature_management.SignatureManagement.create_signatures_in_db',
            return_value=(SignaturManagementState.pallet_id_created, 2004)
        ):
            create_response_obj = self.client.post(
                "/signature/create_pallet", data=data, content_type="application/json"
            ).json
        pallet_id = create_response_obj["pallet_id"]

        # act

        with mock.patch(
            'flaskserver.signature_management.signature_management.SignatureManagement.reidentify_signatures_in_db',
            return_value=(SignaturManagementState.pallet_id_created, 2004)
        ):
            response = self.client.post(
                "/signature/reid_pallet", data=data, content_type="application/json"
            )

        with mock.patch(
            'flaskserver.signature_management.signature_management.SignatureManagement.reidentify_signatures_in_db',
            return_value=(SignaturManagementState.pallet_not_found, None)
        ):
            invalid_response = self.client.post(
                "/signature/reid_pallet", data=invalid_data, content_type="application/json"
            )

        no_pallet_response_response = self.client.post(
            "/signature/reid_pallet", data=no_pallet_data, content_type="application/json"
        )

        # assert
        self.assertEqual(200, response.status_code)
        self.assertEqual(400, no_pallet_response_response.status_code)
        self.assertEqual(404, invalid_response.status_code)
        response_object = response.json
        self.assertIsNotNone(response_object)
        self.assertTrue(response_object["pallet_id"].isdigit())
        self.assertEqual(pallet_id, response_object["pallet_id"])


if __name__ == '__main__':
    unittest.main()
