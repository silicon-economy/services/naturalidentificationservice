# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.


import unittest.mock

import pytest
from pytest_mock import MockerFixture
from flask import Flask

from flaskserver.server import Server


@pytest.fixture(name="flask_mock")
def fixture_flask_mock(mocker: MockerFixture) -> unittest.mock.Mock:
    """A fixture to get a mocked version of class Flask inside Server.
    """
    return mocker.patch("flaskserver.server.Flask", autospec=True)


@pytest.fixture(name="api_bp_mock")
def fixture_api_bp_mock(mocker: MockerFixture) -> unittest.mock.Mock:
    """A fixture to get a mocked version of api_bp blueprint inside
    Server.
    """
    return mocker.patch("flaskserver.server.api_bp", autospec=True)


@pytest.fixture(name="cors_mock")
def fixture_cors_mock(mocker: MockerFixture) -> unittest.mock.Mock:
    """A fixture to get a mocked version of class CORS inside Server."""
    return mocker.patch("flaskserver.server.CORS", autospec=True)


@pytest.fixture(name="server")
def fixture_server() -> Server:
    """A fixture to get an instance of the Server class."""
    return Server()



def test_constructor(mocker: MockerFixture) -> None:
    """Tests that the constructor calls the init_loggers method."""
    mocker.patch.object(Server, "init_loggers")

    Server()

    Server.init_loggers.assert_called_once_with()


def test_start(server: Server, mocker: MockerFixture) -> None:
    """Tests that the start method creates an app instance and calls the
    run method with that app instance as parameter.
    """
    mocker.patch.object(Server, "create_app")

    server.start(config='test')

    Server.create_app.assert_called_once_with('test')
    Server.create_app.return_value.run.assert_called_once_with(host="0.0.0.0", port=5001)


def test_create_app(api_bp_mock: unittest.mock.Mock,
                    cors_mock: unittest.mock.Mock, mocker: MockerFixture) -> None:
    """Tests that create_app instantiates a Flask object, registers the
    coffee_page blueprint, applies CORS middleware and returns the Flask
    app.
    """
    mocker.patch.object(Server, "register_blueprints")

    app = Server.create_app('test')

    Server.register_blueprints.assert_called_once_with(app)
    cors_mock.assert_called_once_with(app)

    assert app.__class__ == Flask, 'create_app result is not as expected a Flask instance.'
