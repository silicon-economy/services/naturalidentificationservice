# Copyright 2022 Open Logistics Foundation
#
# Licensed under the Open Logistics License 1.0.
# For details on the licensing terms, see the LICENSE file.

"""
Tests for signature managment
"""

import unittest
from unittest import mock

import numpy as np
from pymilvus import (connections, Collection)
from torchreid.utils import FeatureExtractor

from flaskserver.server import Server
from flaskserver.signature_management.signature_management import SignatureManagement, SignaturManagementState


class TestSignatureManagement(unittest.TestCase):

    @mock.patch('pymilvus.connections.connect', return_value=connections.instance)
    def setUp(self, connection_mock):
        with mock.patch('pymilvus.connections.connect', return_value=connections.instance):
            self.server = Server()
            self.app = self.server.create_app(config='test')
            self.app.config['TESTING'] = True

            self.height = 384
            self.width = 128

            # create a pallet with 3 random pallet feet images
            self.nb = 3
            np.random.seed()
            self.pallet = [
                (np.random.uniform(low=0, high=255, size=(self.height, self.width, 3)).astype(np.uint8), i)
                for i in range(self.nb)
            ]

            # create a second pallet with 3 random pallet feet images
            np.random.seed()
            self.another_pallet = [
                (np.random.uniform(low=0, high=255, size=(self.height, self.width, 3)).astype(np.uint8), i)
                for i in range(self.nb)
            ]
            self.signature_management = SignatureManagement(self.app)

            model_name = self.app.config.get("ML_MODEL_NAME")
            with mock.patch.object(self.signature_management, "extractor") as extractor_mocked:
                extractor_mocked.return_value = FeatureExtractor(
                        model_name='pcb_p4',
                        model_path="../flaskserver/model/"+model_name,
                        device='cpu'
                    )

    def test_extraction_similarity(self):
        # prepare
        image_set, _ = map(list, zip(*self.pallet))

        # act
        signature_vector = SignatureManagement().extract_vectors([image_set[0]])

        # assert
        self.assertEqual(signature_vector, SignatureManagement().extract_vectors([image_set[0]]))
        self.assertNotEqual(signature_vector, SignatureManagement().extract_vectors([image_set[1]]))

    def test_extraction_batch(self):
        # prepare
        image_set, _ = map(list, zip(*self.pallet))

        # act
        signature_vectors = SignatureManagement().extract_vectors(image_set)

        # assert
        self.assertEqual(len(signature_vectors), self.nb)

    @mock.patch('pymilvus.Collection.load')
    @mock.patch('pymilvus.Collection.search')
    @mock.patch('pymilvus.Collection.release')
    @mock.patch('pymilvus.has_collection', return_value=False)
    @mock.patch('flaskserver.signature_management.signature_management.SignatureManagement.create_pallet_collection', return_value=Collection)
    @mock.patch('flaskserver.signature_management.signature_management.SignatureManagement.create_pallet_feet_collection', return_value=Collection)
    def test_reidentify_pallet(
        self,
        collection_load_mock,
        collection_search_mock,
        collection_release_mock,
        has_collection_mock,
        pallet_collection_mock,
        pallet_feet_collection_mock
    ):
        #prepare
        class MockObjectInsert:
            def __init__(self, primary_keys):
                self.primary_keys = primary_keys


        # act
        with mock.patch(
            'flaskserver.signature_management.signature_management.SignatureManagement.insert_in_collection',
            return_value=MockObjectInsert([2004])
        ):
            status_id, pallet_id = SignatureManagement().create_signatures_in_db(self.pallet)
        with mock.patch(
            'flaskserver.signature_management.signature_management.SignatureManagement.identify_pallet',
            return_value=(SignaturManagementState.pallet_id_found, 2004)
        ):
            status_reid, pallet_reid = SignatureManagement().reidentify_signatures_in_db(self.pallet)
        with mock.patch(
            'flaskserver.signature_management.signature_management.SignatureManagement.insert_in_collection',
            return_value=MockObjectInsert([2005])
        ):
            another_status_id, another_pallet_id = SignatureManagement().create_signatures_in_db(self.another_pallet)
        with mock.patch(
            'flaskserver.signature_management.signature_management.SignatureManagement.identify_pallet',
            return_value=(SignaturManagementState.pallet_id_found, 2005)
        ):
            another_status_reid, another_pallet_reid = SignatureManagement().reidentify_signatures_in_db(self.another_pallet)

        # assert valid pallet id
        self.assertEqual(status_id, SignaturManagementState.pallet_id_created)
        self.assertIsNotNone(pallet_id)
        self.assertIsInstance(pallet_id, int)
        self.assertEqual(another_status_id, SignaturManagementState.pallet_id_created)
        self.assertIsNotNone(another_status_id)
        self.assertIsInstance(another_pallet_id, int)
        self.assertNotEqual(pallet_id, another_pallet_id)

        # assert reidentification works
        self.assertEqual(status_reid, SignaturManagementState.pallet_id_found)
        self.assertIsNotNone(pallet_reid)
        self.assertIsInstance(pallet_reid, int)
        self.assertEqual(pallet_id, pallet_reid)
        self.assertEqual(another_status_reid, SignaturManagementState.pallet_id_found)
        self.assertIsNotNone(another_pallet_reid)
        self.assertIsInstance(another_pallet_reid, int)
        self.assertNotEqual(pallet_id, another_pallet_reid)


if __name__ == '__main__':
    unittest.main()
